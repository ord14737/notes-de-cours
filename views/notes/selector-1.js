export default (function() {
let code1 = 
`selecteur-css {
    propriété1: valeur1;
    propriété2: valeur2;
    propriété3: valeur3;
}`;

let code2 = 
`p {
    ...    
}

header {
    ...
}`;

let code3 = 
`<p id="nom-identifiant"> Lorem Ipsum </p>`;

let code4 = 
`#nom-identifiant {
    ...
}`;

let code5 = 
`<p class="en-gras"> Un paragraphe </p>
<a href="https://google.com" class="en-gras"> Un lien </a>`;

let code6 = 
`.en-gras {
    ...
}`;

let code7 = 
`<img src="./img/mon-image.jpg" alt="Belle image" class="petit-format bordure">`;

let code8 = 
`p.en-retrait {
    ...
}`;

let code9 = 
`ul#todo-list {
    ...
}`;

let code10 = 
`#principal ul li.important  {
    ...
}`;

let code11 = 
`<header>
    <a href="#"> Un lien </a>
    
    <nav>
        <ul>
            <li> <a href="#"> Un autre lien </a> </li>
        </ul>
    </nav>
</header>`;

let code12 = 
`header a {
    ...
}`;

let code13 = 
`footer, p.important, ul li a.en-gras {
    ...
}`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Sélecteurs CSS de base</h1>

                <section>
                    <h2>Format d'une feuille de style</h2>
                    <p>
                        Le code CSS est basé sur la programmation de feuille de style. Une feuille de style a le format suivant:
                    </p>
                    ${ addCode(code1, 'CSS') }
                    <p>
                        Les propriétés et leurs valeurs seront l'intérêt principal de ce cours pendant plusieurs semaines. Ce sont les règles qui définissent le style d'un élément. Par
                        exemple, vous pouvez spécifier la couleur du texte d'un élément avec la propriété ${ inline('color') } et lui donner une valeur comme ${ inline('red') }. Cela 
                        donne la règle ${ inline('color: red') }. Toutefois, dans cette section, nous ne verront pas comment styler un élément, mais plutôt comment identifier quel 
                        élément nous voulons styler. Nous mettrons donc l'emphase sur les sélecteurs CSS. 
                    </p>
                    <p>
                        En CSS, pour sélectionner un certain élément et lui appliquer un style, nous utilisons les sélecteurs CSS. Il existe de nombreux sélecteurs et de nombreuses 
                        règles pour les faire fonctionner ensemble. Dans cette section, nous verrons comment utiliser les sélecteurs CSS de base.
                    </p>
                </section>

                <section>
                    <h2>Sélecteur par nom de balise</h2>
                    <p>
                        Si vous désirez appliquer le même style à toutes les balises d'un certain type, c'est le sélecteur que vous utiliserez. Un sélecteur par nom de balise s'utilise
                        simplement en indiquant le nom de la balise que nous voulons affecter.
                    </p>
                    ${ addCode(code2, 'CSS') }
                </section>
                
                <section>
                    <h2>Sélecteurs par ID</h2>
                    <p>
                        Si vous désirer appliquer un style sur un élément très spécifique de votre page Web et que vous ne désirez pas réutiliser ce style pour d'autres éléments, vous 
                        voulez plutôt utiliser le sélecteur par ID. Ce sélecteur permet de sélectionner un élément de votre page HTML ayant un attribut ID spécifique.
                    </p>
                    ${ addCode(code3, 'HTML') }
                    <p>
                        Pour utiliser le sélecteur par ID, il suffit simplement de mettre le symbole ${ inline('#') } devant le nom du l'ID que vous voulez viser.
                    </p>
                    ${ addCode(code4, 'CSS') }
                </section>

                <section>
                    <h2>Sélecteurs par classe</h2>
                    <p>
                        Si vous désirez créer un style réutilisable que vous pourrez mettre sur plusieurs éléments, le sélecteur par classe sera votre ami. La réutilisation étant un 
                        des concepts de programmation les plus important, c'est probablement le sélecteur que vous utiliserez le plus souvent. Ce sélecteur fonctionne si nous donnons 
                        une "classe" à des éléments HTML. Pour ce faire, nous utiliserons l'attribut ${ inline('class') } qui peut être mis sur n'importe quelle balise HTML.
                    </p>
                    ${ addCode(code5, 'HTML') }
                    <p>
                        Par la suite, pour sélectionner tous les éléments possèdant une classe, nous pouvons simplement utiliser le point ${ inline('.') } suivi du nom de la classe.
                    </p>
                    ${ addCode(code6, 'CSS') }
                    <p>
                        Comme vous pouvez le constater, puisque nous pouvons mettre la même classe sur plusieurs élément, cela rend le style "réutilisable". Il est aussi possible de 
                        mettre plusieurs classe à un élément HTML simplement en les séparant par des espaces, ce qui nous permet beaucoup de flexibilités.
                    </p>
                    ${ addCode(code7, 'HTML') }
                </section>

                <section>
                    <h2>Combinaison de sélecteurs</h2>
                    <p>
                        Il est possible d'être très précis dans nos sélecteurs. Par exemple, si nous voulons sélectionner tous les paragraphes ayant la classe "en-retrait", nous 
                        pouvons utiliser la combinaison de sélecteurs suivante. Vous remarquerez qu'il n'y a aucune espace entre le nom de la balise et le nom de la classe.                        
                    </p>
                    ${ addCode(code8, 'CSS') }
                    <p>
                        Il est aussi possible de faire ce genre de combinaison avec les ID de la façon suivante:
                    </p>
                    ${ addCode(code9, 'CSS') }
                </section>

                <section>
                    <h2>Imbrication de sélecteurs</h2>
                    <p>
                        Il est possible de spécifier des sélecteurs à l'intérieur d'un autre sélecteur simplement en mettant un espace entre les sélecteurs de la façon suivante:
                    </p>
                    ${ addCode(code10, 'CSS') }
                    <p>
                        Dans cet exemple, nous sélectionnons tous les ${ inline('<li>') } ayant la classe ${ inline('important') } et se retrouvant dans un ${ inline('<ul>') } qui se
                        retrouve dans un élément ayant le id ${ inline('principal') }.
                    </p>
                    <p>
                        Il est important de noter que ce type de sélecteur capte les éléments même s'il ne sont pas des enfants direct du sélecteur précédant.
                    </p>
                    ${ addCode(code11, 'HTML') }
                    ${ addCode(code12, 'CSS') }
                    <p>
                        Dans l'exemple ci-dessus, puisque nous sélectionnons tous les ${ inline('<a>') } à l'intérieur des ${ inline('<header>') }, les 2 ${ inline('<a>') } seront visé.
                        En effet, même si le deuxième ${ inline('<a>') } n'est pas directement à l'intérieur du ${ inline('<header>') }, il est quand même dans celui-ci.
                    </p>
                </section>

                <section>
                    <h2>Multiples sélecteurs</h2>
                    <p>
                        Si vous désirez appliquer les mêmes règles CSS à plusieurs éléments, il est possible de le faire en utilisant la virgule ${ inline(',') }.
                    </p>
                    ${ addCode(code13, 'CSS') }
                    <p>
                        Dans l'exemple ci-dessus, les règles contenu entre les accolades sont donc appliqué aux balises ${ inline('<footer>') }, aux balises ${ inline('<p>') } ayant la 
                        classe ${ inline('important') } et aux ${ inline('<a>') } ayant la classe ${ inline('en-gras') } qui se retrouve dans des ${ inline('<li>') } à l'intérieur d'un 
                        ${ inline('<ul>') }.
                    </p>
                </section>
            </div>`
    }
})();