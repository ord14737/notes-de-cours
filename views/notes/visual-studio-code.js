export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Visual Studio Code</h1>

                <section>
                    <h2>Tout en un</h2>
                    <p>
                        Visual Studio Code est un éditeur de texte gratuit et open source de Microsoft. Il est sortie récemment et est devenu très populaire rapidement. En 2019, le site Web
                        Stack Overflow indiquait qu'il était présentement l'éditeur de texte le plus utilisé selon leurs sondages. Il doit principalement sont engouement à son 
                        extensibilité et à sa personnalisation. En effet, s'il ne fait pas quelque chose, il existe probablement une extension permettant de le faire! Les extensions sont 
                        aussi facile à créer et facile à trouver à partir de son interface graphique.
                    </p>
                    <p>
                        Même si cet éditeur est très léger, il possède des fonctionnalités très puissantes comme l'exécution et le débogage du code, le support du versionnement avec Git, 
                        l'utilisation d'une console interne et l'exécution de commande directement dans son interface.
                    </p>
                    <p>
                        Vous pouvez télécharger Visual Studio Code en cliquant sur le lien ci-dessous.
                    </p>
                    <p>
                        <a href="https://code.visualstudio.com/" target="_blank">Visual Studio Code</a>
                    </p>
                    <p>
                        Nous verrons comment utiliser ce logiciel majoritairement en classe. Il contient de nombreuses fonctionnalités que nous verrons au courant de la session. Si vous 
                        voulez en apprendre davantages, vous pouvez consulter la <a href="https://code.visualstudio.com/docs/" target="_blank">documentation</a> de Visual Studio Code.
                    </p>
                </section>

                <section>
                <h2>Alternatives</h2>
                <p>
                    Bien que j'utiliserai majoritairement Visual Studio Code durant la session, il existe de nombreux autres logiciels pour faire du développement Web. Voici quelques
                    alternatives qui pourrait vous intéresser:
                </p>
                <dl>
                    <dt><a href="https://notepad-plus-plus.org/" target="_blank">Notepad++</a></dt>
                    <dd>Éditeur de texte très simple qui supporte l'édition des fichiers de code. Il est gratuit et très léger.</dd>

                    <dt><a href="https://www.sublimetext.com/" target="_blank">Sublime Text</a></dt>
                    <dd>
                        Éditeur de texte très simple à utiliser, mais difficile à maîtriser. Cet éditeur offre des fonctionnalités complexes très intéressantes pour les programmeurs
                        avancés qui désire être efficace. De plus, il est très léger à exécuter. Il est théoriquement payant, mais on peut utiliser une version d'essai du logiciel sans 
                        aucune limite...
                    </dd>

                    <dt><a href="http://brackets.io/" target="_blank">Brackets</a></dt>
                    <dd>Éditeur de texte simple qui est bâtit pour le développement Web. Il est relativement léger et bien entendu gratuit.</dd>

                    <dt><a href="https://atom.io/" target="_blank">Atom</a></dt>
                    <dd>Éditeur de texte simple et léger, bâtit pour la coopération et la personnalisation. Il est lui aussi gratuit.</dd>

                    <dt><a href="https://visualstudio.microsoft.com/vs/" target="_blank">Visual Studio 2019</a></dt>
                    <dd>
                        Éditeur de texte complexe et lourd de Microsoft. Il possède de nombreuses fonctionnalités pour la coopération, le testing et la personnalisation. Il est payant,
                        mais il existe une version &quot;Community&quot; qui est gratuite. Il est toutefois uniquement disponible sous Windows. Vous avez probablement accès à la version 
                        Ultimate puisque vous êtes des étudiants. Bien que très performant et utile dans les gros projets, je ne vous le recommande pas vu sa lourdeur et sa complexité.
                    </dd>

                    <dt><a href="https://www.jetbrains.com/webstorm/" target="_blank">WebStorm</a></dt>
                    <dd>
                        Éditeur de texte complexe et lourd de JetBrains, basé sur IntelliJ IDEA, mais spécialisé pour le Web. Il possède de nombreuses fonctionnalités pour la coopération, 
                        le testing, la personnalisation et l'automatisation. Il possède généralement les dernières technologies et outils de développement pour les programmeur. Il est 
                        payant, mais vous y avez accès gratuitement puisque vous êtes des étudiants. Bien que très performant et utile dans les gros projets, je ne vous le recommande pas 
                        vu sa lourdeur et sa complexité.
                    </dd>
                </dl>
                <p>
                    Il y a encore beaucoup d'autres éditeurs de texte, mais ceux-ci sont tous des éditeurs avec lesquels j'ai eu au moins un peu d'expérience. N'hésitez pas à les essayer
                    et à voir lequel vous préférez.
                </p>
                </section>
            </div>`
    }
})();