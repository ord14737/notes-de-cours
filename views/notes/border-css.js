export default (function() {
let code1Css = 
`p.solide {
    border: 3px solid #000;
}

p.point {
    border: 3px dotted #111;
}

p.tiret {
    border: 3px dashed #222;
}

p.double {
    border: 3px double #333;
}

p.groove {
    border: 3px groove #444;
}

p.ridge {
    border: 3px ridge #555;
}

p.inset {
    border: 3px inset #666;
}

p.outset {
    border: 3px outset #777;
}`;

let code1Html = 
`<p class="solide"> Bordure solide </p>
<p class="point"> Bordure point </p>
<p class="tiret"> Bordure tiret </p>
<p class="double"> Bordure double </p>
<p class="groove"> Bordure 3D (1) </p>
<p class="ridge"> Bordure 3D (2) </p>
<p class="inset"> Bordure 3D (3) </p>
<p class="outset"> Bordure 3D (4) </p>`;

let code2Css = 
`.funky {
    border-top: 2px solid #F00;
    border-right: 4px groove #0F0;
    border-bottom: 6px dashed #FF0;
    border-left: 8px dotted #00F;
}`;

let code2Html = 
`<p class="funky"> Bordure funky </p>`;

let code3Css = 
`.souligner {
    border-bottom: 2px solid #000;
}`;

let code3Html = 
`<h1 class="souligner"> Beau titre </h1>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Bordure</h1>

                <section>
                    <h2>Propriétés d'une bordure</h2>
                    <p>
                        La bordure d'un élémemt HTML possède toujours 3 propriétés distinctes, soit son épaisseur, son style et sa couleur.
                    </p>
                    <dl>
                        <dt>Épaisseur</dt>
                        <dd>C'est essentiellement la taille de la bordure. On lui donnera une taille en pixel (${ inline('px') }) pour le moment.</dd>

                        <dt>Style</dt>
                        <dd>
                            C'est le format de la bordure. Voici une liste des styles de bordure disponible en CSS.
                            <ul>
                                <li><span>${ inline('none') } / ${ inline('hidden') } : Aucune bordure visible</span></li>
                                <li><span>${ inline('dotted') } : Bordure pointillé</span></li>
                                <li><span>${ inline('dashed') } : Bordure en tirets</span></li>
                                <li><span>${ inline('solid') } : Bordure normale</span></li>
                                <li><span>${ inline('double') } : Bordure double</span></li>
                                <li><span>${ inline('groove') } / ${ inline('ridge') } : Bordure 3D</span></li>
                                <li><span>${ inline('inset') } / ${ inline('outset') } : Autre bordure 3D</span></li>
                            </ul>
                        </dd>

                        <dt>Couleur</dt>
                        <dd>
                            La couleur de la bordure. Vous pouvez utiliser les formats des couleurs que nous avons vu précédement ici.
                        </dd>
                    </dl>
                </section>

                <section>
                    <h2>Propriété CSS de base</h2>
                    <p>
                        Pour spécifier une bordure à un élément, vous utiliserez généralement la propriété ${ inline('border') }. Avec cette propriété, nous pouvons spécifier l'épaisseur,
                        le style et la couleur de la bordure dans la même propriété.
                    </p>
                    ${ addCodeExample(code1Html, code1Css) }
                    <p>
                        Il existe d'autres façons de manipuler les bordures, comme avec les propriétés ${ inline('border-width') }, ${ inline('border-style') } et 
                        ${ inline('border-color') }. Il est effectivement possible de manipuler l'épaisseur, le style et la couleur des bordures séparément avec ces propriétés. Nous
                        utiliserons toutefois généralement la propriété ${ inline('border') } puisqu'elle est plus courte à écrire.
                    </p>

                </section>

                <section>
                    <h2>Bordures séparées</h2>
                    <p>
                        Il est possible d'utiliser les propriétés ${ inline('border-top') }, ${ inline('border-right') }, ${ inline('border-bottom') } et ${ inline('border-right') } pour
                        modifier la bordure d'un côté spécifique d'un élément. Bien que cela soit rare puisque les designers graphiques n'aime pas vraiment ça (et aussi parce que c'est
                        plutôt laid), il est possible de mettre une bordure différente à chaque côté de notre élément.
                    </p>
                    ${ addCodeExample(code2Html, code2Css) }
                    <p>
                        Il est aussi possible de mettre une seule bordure avec ces propriétés, ce qui est souvent désiré par les designers.
                    </p>
                    ${ addCodeExample(code3Html, code3Css) }
                </section>
            </div>`
        }
})();
