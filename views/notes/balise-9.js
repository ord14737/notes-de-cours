export default (function() {
let code1Css = 
`.rouge {
    color: #F00;
}

.bleu {
    color: #00F;
}`;

let code1Html = 
`<p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. <span class="rouge"> Praesent vitae nulla vel ante </span> 
    eleifend blandit ut in metus. In ut arcu nec enim placerat finibus. Vestibulum malesuada <span class="bleu"> augue 
    id gravida semper </span>. Curabitur mattis odio in eros pellentesque fermentum.
</p>`;

let code2Css = 
`.retrait {
    margin: 0 40px;
}

.couleur {
    background-color: #A33;
    color: #CCA;
    border: 3px outset #A00;
}`;

let code2Html = 
`<h1>Un titre</h1>
<p>Un petit paragraphe</p>

<div class="retrait couleur">
    <p>Une petite section en retrait avec une liste et du texte</p>
    <ul>
        <li>Élément 1</li>
        <li>Élément 2</li>
    </ul>
    <p>Encore du texte</p>
</div>

<p>Un autre petit paragraphe</p>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Balise non sémantique</h1>

                <section>
                    <h2>Regrouper des éléments</h2>
                    <p>
                        Il arrive souvent que l'on veuille regrouper des éléments ensemble dans le HTML pour pouvoir mettre du CSS sur ce groupe. Bien entendu, lorsque cela est possible,
                        on peut utiliser les balises sémantiques pour arriver à nos besoins. Par exemple, nous pourrions utiliser une balise ${ inline('<section>') } pour regrouper des
                        éléments et y appliquer du CSS. Toutefois, il arrive que les balises sémantiques n'ont pas leur place dans votre groupage d'éléments. Lorsque c'est le cas, nous 
                        utilisons plutôt les balises non sémantiques, soit le ${ inline('<div>') } et le ${ inline('<span>') }.
                    </p>
                </section>

                <section>
                    <h2>${ inline('<span>') }</h2>
                    <p>
                        Le ${ inline('<span>') } est une balise qui s'affiche avec le mode ${ inline('inline') } par défaut. Nous l'utilisons donc souvent pour regrouper des éléments 
                        ${ inline('inline') } ou textuel. Même s'il est possible de changer son mode d'affichage avec le CSS, nous l'utiliserons par convention uniquement en affichage 
                        ${ inline('inline') } ou ${ inline('inline-bloc') }.
                    </p>
                    ${ addCodeExample(code1Html, code1Css) }
                </section>

                <section>
                    <h2>${ inline('<div>') }</h2>
                    <p>
                        Le ${ inline('<div>') } est une balise qui s'affiche avec le mode ${ inline('block') } par défaut. On l'utilise donc généralement pour regrouper des éléments sous
                        forme de boîtes. Même s'il est possible de changer son mode d'affichage avec le CSS, nous l'utiliserons par convention uniquement en affichage 
                        ${ inline('block') }, ${ inline('inline-block') }, ${ inline('flex') } ou ${ inline('grid') }.
                    </p>
                    ${ addCodeExample(code2Html, code2Css) }
                </section>
            </div>`
    }
})();
