export default (function() {
let code1 = 
`<!-- Un lien qui ne fait rien -->
<a> Texte du lien </a>`;

let code2 = 
`<!-- Un lien qui pointe sur eCité -->
<p>
    <a href="https://ecite.lacitec.on.ca/"> Vers eCité! </a>
</p>

<!-- Un lien interne vers la page de la semaine 2 du site Web -->
<p>
    <a href="/#/week?id=1"> Semaine 2 </a>
</p>

<!-- Un lien qui ne fait rien, mais qui éventuellement pointera sur une page -->
<p>
    <a href="#"> Pas encore de liens </a>
</p>`;

let code2Example = 
`<p><a href="https://ecite.lacitec.on.ca/" target="_parent"> Vers eCité! </a></p>
<p><a href="/#/week?id=1" target="_parent"> Semaine 2 </a></p>
<p><a href="#" target="_parent"> Pas encore de liens </a></p>`;

let code3 = 
`<h1 id="ancre-exemple-3">Lien</h1>

...

<!-- Une ancre vers le titre de la page -->
<a href="#ancre-exemple-3"> Vers le haut! </a>`;

let code3Example = 
`<!-- Une ancre vers le titre de la page -->
<a href="#ancre-exemple-3" target="_parent"> Vers le haut! </a>`;

    return {
        template: `
            <div class="note" v-once>
                <h1 id="ancre-exemple-3">Lien</h1>

                <section>
                    <h2>Hyperliens</h2>
                    <p>
                        Les liens, aussi appelé "hyperliens", "ancres" ou "anchors", sont un moyen de rediriger l'utilisateur du site Web vers une autre page ou vers une autre partie de 
                        la page. Les liens ont été pendant longtemps le coeur du Web puisqu'ils nous permettaient de naviguer au travers des différentes pages. On les utilise toujours 
                        beaucoup aujourd'hui, mais en combinaison avec d'autres éléments. La balise utilisé pour créer un lien est ${ inline('<a></a>') }. Si nous utilisons simplement 
                        cette balise tel quel, elle ne fera rien.
                    </p>
                    ${ addCodeExample(code1) }
                </section>

                <section>
                    <h2>Destination</h2>
                    <p>
                        Il est possible de spécifier la destination d'un lien à l'aide de l'attribut ${ inline('href') }. Cet attribut s'attend à recevoir un URL vers une autre page 
                        Web. Il est aussi possible d'utiliser un chemin relatif pour pointer sur une autre page à l'intérieur de notre site Web. Finalement, si vous voulez simplement 
                        un lien qui ne pointe sur rien (un placeholder), vous pouvez utiliser l'URL ${ inline('#') } pour qu'il pointe sur la page courante.
                    </p>
                    ${ addCodeExample([code2, code2Example]) }
                    <p>
                        Dans l'exemple ci-dessus, j'utilise un attribut ${ inline('target') } caché. Si vous utilisez l'inspecteur de code, vous le verrez. Toutefois, à moins d'être
                        vraiement intéressé sur ce qu'il ce passe dans mon site Web, vous pouvez ignorez cet attribut ${ inline('target') }. Nous ne l'utiliserons pas pour le moment, 
                        mais il est nécessaire pour que l'exemple fonctionne dans ce site Web.
                    </p>
                </section>

                <section>
                    <h2>Ancres</h2>
                    <p>
                        Au lieu de faire un lien vers une autre page, il est possible de faire un lien vers un emplacement dans une page. C'est ce qu'on appèle un "ancre" ou "anchor".
                        Pour utiliser un ancre, il faut simplement spécifier un attribut ${ inline('id') } à un élément dans la page vers lequel on veut se rendre. Par la suite dans la
                        balise ${ inline('<a>') }, il suffit simplement de mettre le nom de l'attribut ${ inline('id') } dans l'attribut ${ inline('href') } après un symbole 
                        ${ inline('#') }.
                    </p>
                    ${ addCodeExample([code3, code3Example]) }
                    <p>
                        Vous pouvez inspecter le titre (le ${ inline('<h1>') }) de cette page pour voir qu'il possède effectivement un attribut ${ inline('id') }
                    </p>
                </section>
            </div>`
    }
})();