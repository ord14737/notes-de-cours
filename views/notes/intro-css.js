export default (function() {
let code1 = 
`...

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Titre de page</title>

    <!-- Fichier CSS -->
    <link rel="stylesheet" href="./css/nom-de-fichier.css">
</head>

...`

    return {
        template: `
            <div class="note" v-once>
                <h1>Introduction au CSS</h1>

                <section>
                    <h2>Cascading Style Sheets</h2>
                    <p>
                        Le CSS est un langage de style servant à décrire comment les éléments d'une page HTML devrait être affiché. C'est l'un des 3 langages principal du Web, avec le 
                        HTML et le Javascript. Ce langage nous permettra de créer des "feuilles de styles" que nous pourrons appliquer à notre HTML pour le rendre plus beau. Nous 
                        commencerons donc à avoir des pages Web beaucoup plus intéressantes et attrayantes.
                    </p>
                </section>

                <section>
                    <h2>Création d'un fichier CSS</h2>
                    <p>
                        Pour créer un fichier CSS, vous pouvez le faire comme pour un fichier HTML.
                    </p>
                    <ul>
                        <li><span>En créant un fichier texte dont vous renommez l'extension à ${ inline('.css') } dans votre explorateur de fichier</span></li>
                        <li><span>En créant un fichier CSS directement à partir de votre éditeur de code</span></li>
                    </ul>
                    <p>
                        Peu importe de quelle façon vous créez le fichier, vous devriez le mettre dans le dossier ${ inline('css') } de votre projet Web. Cela nous permettra de garder 
                        notre code de façon plus organisée.
                    </p>
                    <div class="schema">
                        <ul>
                            <li>
                                <i class="material-icons md-18">folder</i>
                                <span>nom-du-projet</span>
                                <ul>
                                    <li class="highlight">
                                        <i class="material-icons md-18">folder</i>
                                        <span>css</span>
                                    </li>
                                    <li>
                                        <i class="material-icons md-18">folder</i>
                                        <span>img</span>
                                    </li>
                                    <li>
                                        <i class="material-icons md-18">folder</i>
                                        <span>js</span>
                                    </li>
                                    <li>
                                        <i class="material-icons md-18">note</i>
                                        <span>index.html</span>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </section>
                <section>
                    <h2>Lier un fichier CSS</h2>
                    <p>
                        Une fois votre fichier CSS créé, nous ne pourrons pas immédiatement écrire du code dedans. En effet, il va falloir d'abords indiquer à notre page HTML 
                        d'utiliser ce nouveau fichier CSS. Pour ce faire, rendez vous dans le fichier HTML que vous voulez lier avec votre nouveau fichier CSS et ajouter la balise 
                        ${ inline('<link>') } dans la balise ${ inline('<head>') } de la page.
                    </p>
                    ${ addCode(code1, 'HTML') }
                    <dl>
                        <dt>${ inline('rel') }</dt>
                        <dd>Indique la relation entre le document lié et ce fichier HTML. Dans le cas du CSS, nous le mettrons toujours à ${ inline('stylesheet') }.</dd>

                        <dt>${ inline('href') }</dt>
                        <dd>
                            Indique le chemin vers le fichier CSS que nous désirons lier. Nous utiliserons généralement un chemin relatif qui pointe dans notre dossier 
                            ${ inline('css') }
                        </dd>
                    </dl>
                    <p>
                        Dans les sections suivantes, nous verrons comment écrire le code CSS pour permettre à notre navigateur d'afficher de plus belle page.
                    </p>
                </section>
            </div>`
    }
})();