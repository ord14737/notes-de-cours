export default (function() {
let code1 = 
`html {
    box-sizing: border-box;
}

*, *::before, *::after {
    box-sizing: inherit;
}`

let code1Css = 
`p.boite {
    width: 50%;
    height: 200px;
    background-color: #edb68c;
}`;

let code1Html = 
`<p class="boite">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt ex vitae leo gravida, ut semper nisi sagittis. Maecenas aliquam scelerisque ligula, efficitur bibendum 
    sapien tincidunt at. Nunc sit amet elementum orci. Aenean varius leo non leo commodo elementum. Cras rutrum pellentesque risus, vel rhoncus lectus ultricies in.
</p>`;

let code2Css = 
`.controle {
    width: 80%;
    max-width: 500px;
    min-width: 350px;
    background-color: #edb68c;
}`

let code2Html = 
`<p class="controle">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis condimentum massa mi, nec euismod est tempor 
    nec. Suspendisse finibus interdum luctus. Mauris scelerisque convallis massa, fermentum elementum urna 
    blandit quis.
</p>`;

let code3Css = 
`input.haut {
    padding-top: 30px;
}

input.droite {
    padding-right: 30px;
}

input.bas {
    padding-bottom: 30px;
}

input.gauche {
    padding-left: 30px;
}

input.partout {
    padding: 30px;
}`;

let code3Html = 
`<input type="button" class="haut" value="Padding en haut">
<input type="button" class="droite" value="Padding à droite">
<input type="button" class="bas" value="Padding en bas">
<input type="button" class="gauche" value="Padding à gauche">
<input type="button" class="partout" value="Padding partout">`;

let code4Css = 
`input.haut {
    margin: 30px 0 0 0;
}

input.droite {
    margin: 0 30px 0 0;
}

input.bas {
    margin: 0 0 30px;
}

input.gauche {
    margin: 0 0 0 30px;
}`;

let code4Html = 
`<p> Des boutons: </p>
<input type="button" class="gauche" value="Margin à gauche">
<input type="button" class="bas" value="Margin en bas">

<p> D'autres boutons: </p>
<input type="button" class="droite" value="Margin à droite">
<input type="button" class="haut" value="Margin en haut">`;

let code5Css = 
`.centrer {
    margin: 0 auto;
    max-width: 200px;
}`

let code5Html = 
`<header>
<h1 class="centrer">Titre de la page</h1>
<p class="centrer">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis condimentum massa mi, nec euismod est tempor 
    nec. Suspendisse finibus interdum luctus. Mauris scelerisque convallis massa, fermentum elementum urna 
    blandit quis.
</p>
<header>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Taille, marge et espacement</h1>

                <section>
                    <h2>Système de boîtes</h2>
                    <p>
                        Tous les éléments d'une page HTML sont essentiellement une boîte. À son plus simple, cette boîte à seulement une largeur et une hauteur qui sont très souvent 
                        définie par ce que contient la balise et par son type d'affichage. Toutefois, il est possible d'ajouter des marges, des bordures et de l'espacement dans cette 
                        boîte, ce qui peut compliquer les choses un peu. La terminologie ici est importante, je vous demande donc de bien comprendre le schémas suivant.
                    </p>
                    <div class="box-example">
                        <div class="box-example-tag top">Margin</div>
                        <div class="box-example-border">
                            <div class="box-example-tag top">Padding</div>
                            <div class="box-example-content">
                                Contenu de la boîte
                            </div>
                            <div class="box-example-tag bottom">Bordure</div>
                        </div>
                    </div>
                    <p>
                        Un élément HTML ressemble à l'exemple ci-dessus. Il est important de bien comprendre les différentes sections de la boîte.
                    </p>
                    <dl>
                        <dt>Bordure</dt>
                        <dd>C'est la bordure de l'élément HTML. Par défaut, le navigateur n'en mets tout simplement pas, mais on peut l'activer avec du CSS.</dd>

                        <dt>Padding</dt>
                        <dd>
                            C'est l'espacement entre le contenu de la boîte et sa bordure. La majorité des éléments en HTML n'ont pas de padding par défaut. Il est toutefois possible d'en 
                            ajouter avec du CSS.
                        </dd>

                        <dt>Margin</dt>
                        <dd>
                            C'est l'espacement entre la bordure de la boîte et les autres éléments de la page Web. Certains éléments HTML ont un margin par défaut, comme les 
                            ${ inline('<h?>') } ou la balise ${ inline('<body>') }. Il est toutefois possible d'en ajouter ou de le modifier avec du CSS.
                        </dd>
                    </dl>
                </section>

                <section>
                    <h2>Calcul des boîtes</h2>
                    <p>
                        Par défaut, le calcul de de la taille des boîtes n'est pas très pratique pour nous. En effet, lorsque nous allons vouloir un site Web qui fonctionne autant sur
                        ordinateur que sur mobile, le calcul par défaut rend les choses asses difficile. Nous pouvons toutefois changer la façon dont le calcul est fait avec une 
                        propriété CSS. Je vous recommande donc d'ajouter le code suivant dans le haut de votre fichier CSS pour modifier cette méthode de calcul.
                    </p>
                    ${ addCode(code1, 'CSS') }
                    <p>
                        Pour vous simplifier la vie, ajoutez toujours ce morceau de code à votre page HTML.
                    </p>
                </section>

                <section>
                    <h2>Taille</h2>
                    <p>
                        Pour modifier la taille d'un élément, vous pouvez utiliser les propriétés CSS ${ inline('width') } et ${ inline('height') } qui modifient respectivement la 
                        largeur et la hauteur du contenu de la boîte. 
                    </p>
                    ${ addCodeExample(code1Html, code1Css) }
                    <p>
                        Il est important de spécifier une unité de mesure quand nous donnons une largeur ou une hauteur. Dans l'exemple ci-dessus, nous utilisons le ${ inline('%') }
                        pour représenter un pourcentage de la largeur du parent et le ${ inline('px') } pour représenter un nombre de pixel. Nous verrons plus en détail les différents
                        types d'unités de mesure disponible en CSS.
                    </p>
                    <p>
                        Il faut aussi faire attention, le padding est aussi considéré dans la largeur et la hauteur, mais pas le margin.
                    </p>
                </section>

                <section>
                    <h2>Taille maximum et minimum</h2>
                    <p>
                        Si vous voulez avoir un meilleur contrôle sur la taille de vos éléments, spécialement lors de la redimension de votre navigateur, les propriétés 
                        ${ inline('min-width') }, ${ inline('max-width') }, ${ inline('min-height') } et ${ inline('max-height') } seront utile. Ces propriétés permettent d'indiquer une
                        valeur maximum ou minimum de taille pour des éléments de votre page Web.
                    </p>
                    ${ addCodeExample(code2Html, code2Css) }
                    <p>
                        Vous pouvez voir ce qui se passe dans l'exemple ci-dessus en changeant la dimension de votre navigateur. En effet, même si le paragraphe est supposé prendre 80%
                        de l'espace, il ne sera jamais plus grand que 500px et jamais plus petit que 350px.
                    </p>
                </section>

                <section>
                    <h2>Padding</h2>
                    <p>
                        Pour modifier le padding d'un élément HTML, nous utiliserons les propriétés CSS ${ inline('padding') }, ${ inline('padding-top') }, ${ inline('padding-right') },
                        ${ inline('padding-bottom') } et ${ inline('padding-left') }. La première propriété permet de modifier le padding de tous les côtés ou d'un seul côté spécifique
                        alors que les 4 autres propriétés vont uniquement modifier le padding d'un seul côté.
                    </p>
                    ${ addCodeExample(code3Html, code3Css) }
                </section>

                <section>
                    <h2>Margin</h2>
                    <p>
                        Pour modifier le margin d'un élément HTML, nous utiliserons les propriétés CSS ${ inline('margin') }, ${ inline('margin-top') }, ${ inline('margin-right') },
                        ${ inline('margin-bottom') } et ${ inline('margin-left') }. Les propriétés fonctionnent de la même façon que pour le padding.
                    </p>
                    ${ addCodeExample(code4Html, code4Css) }
                    <p>
                        Le margin est définitivement plus difficile à visualiser que le padding. Je vous suggère fortement d'utiliser l'inspecteur de code dans votre navigateur pour
                        avoir une représentation visuelle de celui-ci.
                    </p>
                </section>

                <section>
                    <h2>Centrer un élément horizontalement</h2>
                    <p>
                        Même si cela semble facile, il peut être assez difficile de centrer un élément dans votre page Web. Effectivement, dépendamment du type d'affichage d'un objet ou 
                        encore du positionnement ou de l'affichage de son élément parent, la façon de centrer un élément se fera différemment. La technique expliqué ci-dessous fonctionne
                        lorsqu'un élément utilisant un affichage en block n'ayant pas un ${ inline('width: 100%;') } est à l'intérieur d'un autre élément affiché en bloc.
                    </p>
                    ${ addCodeExample(code5Html, code5Css) }
                    <p>
                        La technique ci-dessus utilise la propriété de ${ inline('margin: auto;') } qui indique à une marge de prendre le plus d'espace possible. En utilisant cette 
                        propriété sur les marges à gauche et à droite, nous pouvons ainsi centrer un élément. Nous verrons d'autres façon de centrer les éléments durant d'autres cours
                        lorsque nous verrons d'autres propriétés CSS.
                    </p>
                </section>
            </div>`
    }
})();