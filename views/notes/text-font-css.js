export default (function() {
let code1Css = 
`.gauche {
    text-align: gauche;
}

.droite {
    text-align: right;
}

.centrer {
    text-align: center;
}

.justifier {
    text-align: justify;
}`;

let code1Html = 
`<p class="gauche">À gauche</p>
<p class="droite">À droite</p>
<p class="centrer">Au centre</p>
<p class="justifier">
    Du texte justifier ne fonctionne pas vraiment s'il ne contient pas assez de textes. Lorem ipsum dolor sit amet 
    consectetur, adipisicing elit. Rerum velit, eligendi officia sed quia vero itaque beatae quis, nulla eaque voluptas 
    soluta fuga doloribus laudantium maiores nemo quidem eius saepe!
</p>`;

let code2Css = 
`.decoration1 {
    text-decoration: underline
}

.decoration2 {
    text-decoration: line-through double #F00;
}

.decoration3 {
    text-decoration: underline overline wavy #2e7dd1;
}`;

let code2Html = 
`<p class="decoration1">Souslignement</p>
<p class="decoration2">Texte rayé</p>
<p class="decoration3">Décoration bizarre</p>`;

let code3Css = 
`.transform1 {
    text-transform: uppercase;
}

.transform2 {
    text-transform: lowercase;
}

.transform3 {
    text-transform: capitalize;
}

.transform4 {
    font-variant: small-caps;
}`;

let code3Html = 
`<p class="transform1">Je suis en majuscule</p>
<p class="transform2">Je suis en MINUSCULE</p>
<p class="transform3">Toutes les premières lettres sont en majuscules</p>
<p class="transform4">J'utilise les petites majuscules</p>`;

let code4Css = 
`.ombre {
    text-shadow: -.1em .1em 0 #999;
}

.eclat {
    text-shadow: 0 0 .2em #edaf1f;
}

.mixe {
    text-shadow: 0 .7em .2em #F00;
}`;

let code4Html = 
`<p>
    Lorem, ipsum dolor sit amet consectetur adipisicing elit. <span class="ombre"> Dolor aliquid </span> facere distinctio, 
    laboriosam <span class="eclat"> aspernatur quas </span> dolorum. Commodi nihil quidem, quae dolorum aspernatur et nesciunt, 
    necessitatibus asperiores perspiciatis <span class="mixe"> deleniti architecto </span> molestias?
</p>`;

let code5Css = 
`.interligne-double {
    line-height: 2em;
}

.etirer {
    letter-spacing: .3em;
}

.approcher {
    letter-spacing: -.1em;
}

.grand-espace {
    word-spacing: 2em;
}`;

let code5Html = 
`<p class="interligne-double">
    <span class="etirer"> Lorem, ipsum </span> dolor sit amet consectetur adipisicing elit. Dolor aliquid facere distinctio, 
    laboriosam <span class="approcher"> aspernatur quas </span> dolorum. Commodi nihil quidem, quae dolorum aspernatur et 
    nesciunt, <span class="grand-espace">necessitatibus asperiores perspiciatis  deleniti architecto molestias?</span>
</p>`;

let code6Css = 
`.petit {
    font-size: .7em;
}

.grand {
    font-size: 1.5em;
}

.flexible {
    font-size: 3vw;
}`;

let code6Html = 
`<p class="petit">Du texte écrit en tout petit</p>
<p class="grand">Du texte écrit plus grand</p>
<p class="flexible">Du texte qui change de taille avec la taille de la fenêtre</p>`;

let code7Css = 
`.italique {
    font-style: italic;
}

.gras {
    font-weight: 700;
}

.autre-gras {
    font-weight: bold;
}`;

let code7Html = 
`<p class="italique">Texte en italique</p>
<p class="gras">Texte en gras</p>
<p class="autre-gras">Texte aussi en gras</p>`;

let code8Css = 
`h1 {
    font-family: "Arial", sans-serif;
}

p {
    font-family: "Trebuchet MS", sans-serif;
}

.comic {
    font-family: "Comic Sans MS", cursive;
}`;

let code8Html = 
`<h1>Un titre important</h1>
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum voluptatibus commodi, animi ab  recusandae quia officiis 
    sed perferendis corporis, eveniet ratione, in nam sapiente eos voluptate facilis ipsa eaque vero.
</p>
<p class="comic">Ne JAMAIS utiliser comic sans pour du texte dans un site Web</p>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Typographie et polices</h1>

                <section>
                    <h2>Aligner le texte</h2>
                    <p>
                        Permet d'aligner le texte à gauche, à droite, au centre ou de le justifier. Vous devez mettre la propriété ${ inline('text-align') } sur le conteneur (parent) du
                        texte. Ce conteneur doit utiliser l'affichage de type ${ inline('block') } pour que tout fonctionne.
                    </p>
                    ${ addCodeExample(code1Html, code1Css) }
                </section>

                <section>
                    <h2>Décoration du texte</h2>
                    <p>
                        Permet de spécifier une ligne de soulignement, de surlignement ou de travers sur votre texte. La propriété ${ inline('text-decoration') } vous permet de spécifier
                        le type de ligne, son style ainsi que sa couleur. Voici un tableau contenant les valeurs possibles pour le type de la ligne ainsi que son style.
                    </p>
                    <table>
                        <thead>
                            <tr>
                                <th>Type de lignes</th><th>Style de lignes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr> <td>underline</td> <td>solid</td> </tr>
                            <tr> <td>overline</td> <td>double</td> </tr>
                            <tr> <td>line-through</td> <td>dotted</td> </tr>
                            <tr> <td></td> <td>dashed</td> </tr>
                            <tr> <td></td> <td>wavy</td> </tr>
                        </tbody>
                    </table>
                    ${ addCodeExample(code2Html, code2Css) }
                </section>

                <section>
                    <h2>Transformation du texte</h2>
                    <p>
                        Permet de forcer le texte en majuscule, en minuscule, en petite majuscule ou de mettre uniquement la première lettre de chaque mot majuscule. On utilisera les 
                        propriétés ${ inline('text-transform') } pour les majuscules, minuscules et premières lettres en majuscules ainsi que ${ inline('font-variant') } pour mettre en 
                        petite majuscule.
                    </p>
                    ${ addCodeExample(code3Html, code3Css) }
                </section>

                <section>
                    <h2>Ombre sur le texte</h2>
                    <p>
                        Permet de mettre une ombre ou une forme d'éblouissement sur le texte. La propriété ${ inline('text-shadow') } permet de spécifier de décalage de l'ombre, son 
                        rayon dans lequel nous mettrons un flou ainsi que la couleur de l'ombre.
                    </p>
                    ${ addCodeExample(code4Html, code4Css) }
                </section>

                <section>
                    <h2>Espacement</h2>
                    <p>
                        Il est possible de changer l'espacement entre les lettres à l'aide de la propriété ${ inline('letter-spacing') }, de changer l'espacement entre les mots à l'aide
                        de la propriété ${ inline('word-spacing') } et de changer l'espacement entre les lignes avec la propriété ${ inline('line-height') }
                    </p>
                    ${ addCodeExample(code5Html, code5Css) }
                </section>

                <section>
                    <h2>Taille de la police</h2>
                    <p>
                        Permet de changer la taille du texte. La propriété ${ inline('font-size') } permet de spécifier la taille du texte. Nous utiliserons généralement l'unité de 
                        mesure ${ inline('em') } ou ${ inline('rem') } pour définir la taille.
                    </p>
                    ${ addCodeExample(code6Html, code6Css) }
                </section>

                <section>
                    <h2>Style de la police</h2>
                    <p>
                        Permet de mettre le texte en gras ou en italique. Vous pouvez utiliser la propriété ${ inline('font-style') } pour mettre le texte en italique et la propriété
                        ${ inline('font-weight') } pour mettre le texte en gras.
                    </p>
                    ${ addCodeExample(code7Html, code7Css) }
                </section>

                <section>
                    <h2>Police de caractères</h2>
                    <p>
                        Permet de changer la police de caractère utilisée pour du texte. On utilise la propriété ${ inline('font-family') } pour définir une liste prioritaire de 
                        polices de caractères. Il est important de définir des familles de polices plus générique dans votre liste de priorités pour ainsi avoir une autre police pour
                        l'affichage si votre police la plus prioritaire ne fonctionne pas. Voici une liste des polices génériques disponible dans les navigateurs Web. Vous devez 
                        toujours en mettre une comme "fallback" dans votre liste de police de caractères lorsque vous utilisez la propriété ${ inline('font-family') }.
                    </p>
                    <table>
                        <thead>
                            <tr>
                                <th>Police générique</th><th>Définition</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr> 
                                <td>serif</td> 
                                <td>Police de caractères avec <a href="https://fr.wikipedia.org/wiki/Empattement_(typographie)" target="_blank">empattement</a></td> 
                            </tr>
                            <tr> 
                                <td>sans-serif</td> 
                                <td>Police de caractères sans empattement</td> 
                            </tr>
                            <tr> 
                                <td>monospace</td> 
                                <td>Police dont tous les caractères ont la même largeur</td> 
                            </tr>
                            <tr> 
                                <td>cursive</td> 
                                <td>Police simulant une écriture à la main</td> 
                            </tr>
                            <tr> 
                                <td>fantasy</td> 
                                <td>Police de caractères décorative</td> 
                            </tr>
                        </tbody>
                    </table>
                    ${ addCodeExample(code8Html, code8Css) }
                </section>
            </div>`
    }
})();
