let code = [];

code.push({});
code[0].js = 
`// Exemple 1: Tableau vide
let tab1 = [];

// Exemple 2: Tableau avec des éléments
let tab2 = [2, 6, 9, 1, 3, 8, 0, 7, 4, 5];

// Exemple 3: Tableau avec constructeur (même chose)
let tab3 = new Array(2, 6, 9, 1, 3, 8, 0, 7, 4, 5);

// Exemple 4: Tableau avec des types différents
let tab4 = ['Allo', 42, false, 'B', 2];`;

code.push({});
code[1].js = 
`let fruits = ['Pommes', 'Oranges', 'Poires', 'Bananes', 'Kiwis'];

// Exemple 1: Accéder à un élément
// Affiche dans la console "J\'aime les Poires"
let unFruitQueJAime = fruits[2];
console.log('J\\\'aime les ' + unFruitQueJAime);

// Exemple 2: Changer un élément
// Affiche dans la console "J\'aime les Fraises"
fruits[3] = 'Fraises';
console.log('J\\\'aime les ' + fruits[3];`;

code.push({});
code[2].html = 
`<div>red</div>
<div>blue</div>
<div>green</div>
<div>yellow</div>`;

code[2].js = 
`// Exemple 1: Boucles sur les éléments d'un tableau
// Le tableau contient tous les <div> de notre page et on modifie leur couleur de 
// fond avec le texte dans le <div> dans la boucle for
let elements = document.querySelectorAll('div');

for(let i = 0 ; i < elements.length ; i++){
    elements[i].style.backgroundColor = elements[i].innerText;
}`;

code.push({});
code[3].html = 
`<ul id="liste"></ul>`;

code[3].js = 
`let listeEpicerie = ['Lait', 'Oeuf', 'Steak', 'Brocoli', 'Pain'];
const listeHTML = document.getElementById('liste');

// Exemple 1: Ajouter des éléments à la fin du tableau
listeEpicerie.push('Fromage');

// Exemple 2: Ajouter des éléments au début du tableau
listeEpicerie.unshift('Céréales');

// Exemple 3: Ajouter des éléments à un index spécifique du tableau
// Le 1er paramètre indique l'index où ajouter
// Le 2ème paramètre doit être 0, sinon des valeurs seront supprimées
// Le 3ème paramètre indique l'élément à ajouter
listeEpicerie.splice(3, 0, 'Tomate');

for(let i = 0 ; i < listeEpicerie.length ; i++){
    listeHTML.innerHTML += ('<li>' + listeEpicerie[i] + '</li>');
}`;

code.push({});
code[4].html = 
`<ul id="liste"></ul>`;

code[4].js = 
`let listeEpicerie = ['Lait', 'Oeuf', 'Steak', 'Brocoli', 'Pain'];
const listeHTML = document.getElementById('liste');

// Exemple 1: Supprimer des éléments à la fin du tableau
listeEpicerie.pop();

// Exemple 2: Supprimer des éléments au début du tableau
listeEpicerie.shift();

// Exemple 3: Supprimer des éléments à un index spécifique du tableau
// Le 1er paramètre indique l'index à supprimer
// Le 2ème paramètre indique le nombre de valeurs à supprimer
listeEpicerie.splice(2, 1);

for(let i = 0 ; i < listeEpicerie.length ; i++){
    listeHTML.innerHTML += ('<li>' + listeEpicerie[i] + '</li>');
}`;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Tableau</h1>

            <section>
                <h2>Introduction</h2>
                <p>
                    Les tableaux sont l'une des plus importante structure de données en programmation. Ils nous permettent de rassembler plusieurs valeurs, objets ou éléments dans une
                    seule et même variable. Javascript permet la manipulation de tableaux, mais contrairement à d'autres langages de programmation, il est très flexible et permet de
                    nombreuses fonctionnalités qui sont généralement inhabituelle aux tableaux.
                </p>
            </section>

            <section>
                <h2>Création et utilisation de base</h2>
                <p>
                    Pour créer un tableau en Javascript, vous pouvez utiliser les instructions suivante:
                </p>
                <styled-code lang="Javascript" v-bind:code="code[0].js"></styled-code>
                <p>
                    Il est important de noter que l'exemple 1 et l'exemple 2 crée un tableau identique. Vous pouvez donc utiliser la méthode de création que vous préférez. De plus, 
                    puisque Javascript est un langage faiblement typé, il est possible de mettre des types différents à l'intérieur d'un tableau, contrairement à des langages comme Java
                    C# ou C++.
                </p>
                <p>
                    Pour accéder à un élément du tableau et le modifier, vous pouvez utiliser les instructions suivantes:
                </p>
                <styled-code lang="Javascript" v-bind:code="code[1].js"></styled-code>
                <p>
                    Nous utilisons ici l'opérateur d'indexation ${ inline('[]') } pour accèder à une variable. Le code accéder et modifier des éléments est le même que pour les 
                    langages Java, C# et C++. Vous êtes donc probablement en territoire connu.
                </p>
            </section>

            <section>
                <h2>Boucler sur un tableau</h2>
                <p>
                    Comme en Java, les tableaux ont une propriété ${ inline('.length') } qui nous permet d'avoir la taille du tableau. Nous pourrons utiliser cette valeur avec une 
                    boucle ${ inline('for') } pour itérer sur les différents éléments du tableau.
                </p>
                <code-example v-bind:code="code[2]"></code-example>
            </section>

            <section>
                <h2>Ajouter et supprimer des éléments</h2>
                <p>
                    Contrairement à Java, C# ou C++, Javascript nous permet d'ajouter et de supprimer des éléments dans un tableau. C'est parce qu'en fait, les tableaux en Javascript
                    sont plutôt des listes. Je ne vous explique pas la différence entre une liste et un tableau. Vous pouvez faire une recherche rapide sur le Web pour le savoir. Je 
                    montre toutefois comment ajouter et supprimer les éléments dans un tableau.
                </p>
                <code-example v-bind:code="code[3]"></code-example>
                <code-example v-bind:code="code[4]"></code-example>
            </section>

            <section>
                <h2>Ressources externes</h2>
                <p>
                    Voici quelques ressources externes qui vous permettront d'apprendre comment utiliser les tableaux en Javascript.
                </p>
                <ol>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array" target="_blank">
                            MDN
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://www.w3schools.com/js/js_arrays.asp" target="_blank">
                            w3school
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://www.w3schools.com/jsref/jsref_obj_array.asp" target="_blank">
                            Référence de toutes les fonctions de tableau sur w3school
                        </a>
                    </span></li>
                </ol>
            </section>

            <section>
                <h2>Capsule vidéo</h2>
                <div class="video">
                    <div>
                        <iframe
                            src="https://www.youtube.com/embed/1uKEGTnSGks" 
                            frameborder="0" 
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </section>
        </div>`
};