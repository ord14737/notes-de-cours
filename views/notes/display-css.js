export default (function() {
let code1Css = 
`nav a {
    display: inline-block;
    width: 150px;
    height: 30px;
    margin: 0 10px;
    border: 1px solid #555;
    background-color: #ccc;
}`;

let code1Html = 
`<nav>
    <a href="#">Accueil</a>
    <a href="#">Informations</a>
    <a href="#">Documentations</a>
    <a href="#">À propos</a>
    <a href="#">Contacts</a>
</nav>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Changement d'affichage</h1>

                <section>
                    <h2>Différents type d'affichage</h2>
                    <p>
                        Il existe différents types d'affichage. Nous en avons déjà couvert 2, soit l'affichage en ligne et l'affichage en bloc. Il existe toutefois de nombreux autres 
                        types d'affichage et CSS nous permettra de les manipuler facilement. Voici une courte liste des types d'affichage que vous utiliserez dans ce cours:
                    </p>
                    <dl>
                        <dt>${ inline('inline') }</dt>
                        <dd>L'affichage en ligne. Les éléments utilisant cet affichage suive le flux du texte dans la page Web.</dd>

                        <dt>${ inline('block') }</dt>
                        <dd>L'affichage en bloc. Les éléments utilisant cet affichage prennent par défaut tout l'espace en largeur et leur hauteur est facilement manipulable.</dd>

                        <dt>${ inline('inline-bloc') }</dt>
                        <dd>
                            Combinaison de l'affichage en ligne et en bloc. C'est essentiellement un affichage en ligne, mais dont nous pouvons y modifier facilement la hauteur, 
                            contrairement à l'affichage en ligne traditionnel.
                        </dd>

                        <dt>${ inline('none') }</dt>
                        <dd>
                            L'affichage vide. Ce type d'affiche indique que l'élément ne doit pas être visible. On l'utilise beaucoup avec de l'intéraction, comme pour cacher des menus
                            si nous cliquons sur un bouton.
                        </dd>

                        <dt>${ inline('flex') }</dt>
                        <dd>
                            L'affichage flexible en boîte. C'est un nouveau type d'affichage très pratique pour les sites Web qui s'affiche bien sur ordinateur ou sur mobile. Nous 
                            verrons comment utiliser ce type d'affichage plus tard.
                        </dd>

                        <dt>${ inline('grid') }</dt>
                        <dd>
                            L'affichage en grille. C'est un nouveau type d'affichage très pratique pour organiser un site Web sous la forme d'une grille. Nous verrons comment utiliser 
                            ce type d'affichage plus tard.
                        </dd>
                    </dl>
                    <p>
                        Vous trouverez l'ensemble des types d'affichage sur <a href="https://www.w3schools.com/cssref/pr_class_display.asp" target="_blank">w3school</a>.
                    </p>
                </section>

                <section>
                    <h2>Propriété CSS</h2>
                    <p>
                        Si vous voulez changer le type d'affichage d'un élément, vous utiliserez la propriété CSS ${ inline('display') }. Vous pouvez ensuite mettre l'un des type 
                        d'affichage ci-dessus comme valeur. Dans l'exemple suivant, nous stylons un menu de navigation en utilisant l'affichage ${ inline('inline-bloc') } pour mieux
                        contrôler la hauteur des éléments.
                    </p>
                    ${ addCodeExample(code1Html, code1Css) }
                </section>
            </div>`
    }
})();