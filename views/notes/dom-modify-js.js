export default {
    template: `
        <div class="note" v-once>
            <h1>Manipulation du DOM</h1>

            <section>
                <h2>Ressources sur le DOM en Javascript</h2>
                <p>
                    Voici quelques ressources externes qui vous indiqueront différente façon de manipuler le DOM en Javascript.
                </p>
                <ol>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/API/Document/createElement" target="_blank">
                            Créer des élément - MDN
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://www.w3schools.com/js/js_htmldom_nodes.asp" target="_blank">
                            Manipulation générale du DOM - w3school
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/HTML/Element/template" target="_blank">
                            Utilisation des templates - MDN
                        </a>
                    </span></li>
                </ol>
            </section>

            <section>
                <h2>Capsule vidéo</h2>
                <div class="video">
                    <div>
                        <iframe
                            src="https://www.youtube.com/embed/H2ybIJePXno" 
                            frameborder="0" 
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </section>
        </div>`
};