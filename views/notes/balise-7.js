export default (function() {
let code1 = 
`<p>
    Zone de texte: 
</p>

<!-- Zone de texte -->
<textarea rows="10" cols="80" maxlength="200" placeholder="Zone de texte vide">
Texte par défaut dans la zone de texte
</textarea>`;

let code2 = 
`<!-- Boîte de sélection -->
Boîte de sélection: 
<select size="6" multiple>
    <option>Chat</option>
    <option>Chien</option>
    <option selected>Lapin</option>
    <option>Hamster</option>
    <option selected>Serpent</option>
    <option>Tortue</option>
    <option>Oiseau</option>
    <option>Pinguin</option>
</select>`;

let code3 = 
`<!-- Boîte de sélection -->
Combobox / Listbox: 
<select size="1">
    <option>Oui</option>
    <option>Non</option>
    <option>Peut-être</option>
    <option>Je ne sais pas</option>
</select>`;

let code4 = 
`<select size=8>
    <optgroup label="Xbox">
        <option>Halo</option>
        <option>Minecraft</option>
        <option>Forza</option>
    </optgroup>
    <optgroup label="Playstation">
        <option>Ratchet & Clank</option>
        <option>Horizon</option>
        <option>God of War</option>
    </optgroup>
    <optgroup label="Nintendo">
        <option>Pokémon</option>
        <option>The Legend of Zelda</option>
        <option>Metroid</option>
    </optgroup>
</select>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Zone de textes et sélections</h1>

                <section>
                    <h2>Pas des inputs</h2>
                    <p>
                        Bien que la balise input constitue la majorité des contrôles affichable disponible dans une page Web, il existe quelques autres balises qui serviront aussi à
                        représenter des champs dans un formulaire. Dans ce document, nous passerons au travers de ces différentes balises.
                    </p>
                </section>

                <section>
                    <h2>Zone de texte long</h2>
                    <p>
                        Si vous voulez que votre utilisateur puisse entrer des valeurs de texte qui prennent plus d'une ligne, le input de type texte n'est pas très pratique. Vous 
                        voulez plutôt utiliser la balise ${ inline('<textarea></textarea>') } dans ce cas. Contrairement à la balise input, vous devez avoir une balise fermante pour
                        les ${ inline('<textarea>') }. Ses attrbuts sont sont essentiellement les mêmes que l'input de type texte, mais avec quelques suppléments. De plus, au lieu 
                        d'utiliser l'attribut value pour mettre une valeur par défaut, nous ajoutons simplement le contenu par défaut de la zone de texte dans sa balise.
                    </p>
                    <dl>
                        <dt>${ inline('maxlength') }</dt>
                        <dd>Longueur maximale du texte que l'utilisateur peut entrer.</dd>

                        <dt>${ inline('placeholder') }</dt>
                        <dd>Texte fantôme à afficher dans l'input lorsqu'il est vide.</dd>

                        <dt>${ inline('rows') }</dt>
                        <dd>Nombre de rangées (lignes) affiché dans la zone de texte.</dd>
                        
                        <dt>${ inline('cols') }</dt>
                        <dd>Nombre de colonnes (caractères) affiché dans la zone de texte.</dd>
                    </dl>
                    ${ addCodeExample(code1) }
                </section>

                <section>
                    <h2>Sélection</h2>
                    <p>
                        Si vous voulez plutôt que l'utilisateur du site Web puisse sélectionner un ou plusieurs éléments dans une boîte de sélection, c'est la balise 
                        ${ inline('<select>') } que vous voulez. Cette balise, avec l'aide de la balise ${ inline('<option>') }, vous permet de créer une boîte avec plusieurs choix que
                        l'utilisateur pourra sélectionner. Il possède aussi quelques attributs intéressant.
                    </p>
                    <dl>
                        <dt>${ inline('multiple') }</dt>
                        <dd>Indique que le select accepte plusieurs sélection avec la touche ${ inline('Shift') } ou ${ inline('Ctrl') }.</dd>

                        <dt>${ inline('size') }</dt>
                        <dd>Nombre de lignes affichées dans la boîte de sélection.</dd>

                        <dt>${ inline('selected') }</dt>
                        <dd>Attribut que l'on place dans la balise ${ inline('<option>') } pour indiquer que cette option est sélectionnée par défaut.</dd>
                    </dl>
                    ${ addCodeExample(code2) }
                    <p>
                        Si vous voulez afficher la boîte de sélection de façon différente, vous pouvez essayer de mettre le ${ inline('size') } à 1 et de ne pas mettre le 
                        ${ inline('multiple') } pour avoir un listbox / combobox.
                    </p>
                    ${ addCodeExample(code3) }
                    <p>
                        Il est possible de grouper les options dans un ${ inline('<select>') } à l'aide de la balise ${ inline('<optgroup>') } et de son attribut ${ inline('label') }.
                    </p>
                    ${ addCodeExample(code4) }
                </section>
            </div>`
    }
})();
