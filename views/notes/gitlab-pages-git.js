let code = [];

code.push({});
code[0].yml = 
`pages:
  stage: deploy
  script:
    # Copy whole project to the public path
    - mkdir .public
    - cp -r * .public
    - mv .public public
    
  artifacts:
    paths:
      - public
  only:
    - master`;

  code.push({});
  code[1].url = 
  `https://<nom-du-groupe-ou-du-créateur>.gitlab.io/<nom-du-projet>`;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Publier en ligne sur GitLab</h1>

            <section>
                <h2>Introduction</h2>
                <p>
                    Si vous voulez partager votre site Web avec votre famille et vos amis ou tout simplement démontrer vos talents de design et CSS au monde entier, vous avez besoin
                    d'héberger votre site Web sur un serveur. Habituellement, ceci coûte un certain montant d'argent, mais il y a certaines options gratuites. Dans notre cas, je vous 
                    suggère fortement d'utiliser GitLab puisque c'est déjà la plateforme que nous utilisons pour héberger notre code.
                </p>
            </section>

            <section>
                <h2>Configuration</h2>
                <p>
                    Pour héberger sur GitLab, vous devrez ajouter un fichier de configuration dans votre projet. Il est important de nommer ce fichier exactement 
                    ${ inline('.gitlab-ci.yml') }. Ce fichier de configuration du type YAML nous permettra d'indiquer à GitLab que ce projet est un projet Web et de l'héberger sur ses
                    serveurs. Ce fichier de configuration doit contenir exactement le code ci-dessous:
                </p>
                <styled-code lang="YAML" v-bind:code="code[0].yml"></styled-code>
            </section>

            <section>
                <h2>Résultat</h2>
                <p>
                    Après le commit du fichier de configuration, GitLab mettra automatiquement votre site Web en ligne. De plus, à chaque "push" fait sur le projet, le site Web sera 
                    mis à jour automatiquement. Attendez-vous quand même à 1 ou 2 minutes de délai puisque ce processus demande la réservation de ressources pour GitLab. Vous trouverez
                    votre site Web à l'adresse suivante:
                </p>
                <styled-code lang="plaintext" v-bind:code="code[1].url"></styled-code>
                <p>
                    Si le lien ne semble pas fonctionner, vous pouvez aussi le trouver dans les menus de configuration sous "Settings" > "Pages" dans GitLab.
                </p>
                <p>
                    Notez bien que si votre projet est marqué comme privé, uniquement les utiliseurs ayant accès au projet pourront accéder au site Web par défaut à moins que vous 
                    changiez la configuration de GitLab. Vous pouvez faire la modification dans "Settings" > "General" > "Visibility, project features, permissions" > "Pages".
                </p>
            </section>
        </div>`
};