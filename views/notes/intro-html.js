export default (function() {
// Code example 1
let code1 = 
`<nom-de-balise> contenu </nom-de-balise>`;

// Code example 2
let code2 = 
`<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Un titre</title>
</head>

<body>

</body>

</html>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Introduction au HTML</h1>

                <section>
                    <h2>Les balises</h2>
                    <p>
                        Au coeur de chaque application Web, il y a le HTML. Le HTML est un langage de balisage. Contrairement à un langage de programmation comme Java, les langages de 
                        balisage ne contiennent pas de logique. Ils ne servent qu'à définir et structurer de l'information. Le HTML sert donc à définir et à structurer nos pages Web. 
                        Comme vous pourrez le constater, le HTML sera beaucoup plus simple à apprendre qu'un langage de programmation.
                    </p>
                    <p>
                        Puisque le HTML est un langage de balise, il est essentiel de savoir ce qu'est une balise en HTML pour pouvoir l'utiliser. En effet, en HTML, tout est une balise. On
                        les retrouve généralement sous le format suivant:
                    </p> 
                    ${ addCode(code1, 'HTML') }
                    <p>
                        Chaque balise possède un nom. Elles vont aussi, en général, contenir du texte ou d'autres balises. Finalement, une balise doit se fermer à l'aide d'une balise de 
                        fermeture contenant une barre oblique ${ inline('/') }. Il arrive, dans certains cas, qu'une balise ne contienne rien. Dans ce cas particulier, nous omettrons 
                        généralement la balise de fermeture.
                    </p>
                    <p>
                        Il existe plus de 100 balises différentes en HTML et elles utilisent tous une syntaxe similaire à celle montré ci-dessus. Vous n'aurez pas à apprendre toutes les 
                        balises par coeur, mais il est important de connaître les balises de base. Pour les balises plus complexe ou plus rarement utilisé, vous pourrez utiliser les 
                        ressources disponibles sur ce site ou ailleurs sur Internet. Nous commencerons à voir plus en détail chaque balise un peu plus tard. Pour le moment, voyons comment 
                        construire une page Web.
                    </p>
                </section>
                <section>
                    <h2>Gabarit d'un fichier HTML</h2>
                    <p>
                        Chaque page Web que nous ferons utilisera le gabarit HTML de base ci-dessous. Pour créer un fichier HTML, vous pouvez simplement créer un fichier texte vide dans
                        votre explorateur de fichier. Vous n'aurez par la suite qu'à changer l'extension du fichier pour ".html" au lieu de ".txt". Assurez&#8209;vous de faire cette
                        modification pour que les navigateurs Web soient en mesure d'afficher votre page Web.
                    </p>
                    ${ addCode(code2, 'HTML') }
                    <p>
                        Chaque balise ici est importante. Voici donc une courte explication pour chaque balise:
                    </p>
                    <dl>
                        <dt>${ inline('<!doctype html>') }</dt>
                        <dd>
                            Commande indiquant au navigateur Web que le fichier contient du HTML. Contrairement à ce que l'on peut penser, cette commande n'est théoriquement pas une 
                            balise. Assurez-vous de toujours ajouter le Doctype au sommet de votre fichier HTML.
                        </dd>
                        <dt>${ inline('<html>') }</dt>
                        <dd>
                            La balise englobant la page Web. Toute la page Web au complet est inséré à l'intérieur de cette balise. Nous indiquons aussi la langue de base de la page Web
                            dans cette balise.
                        </dd>
                        <dt>${ inline('<head>') }</dt>
                        <dd>
                            La balise indiquant au navigateur quoi faire avant d'afficher la page. Toutes les balises à l'intérieur du ${ inline('<head>') } servent à configurer la page 
                            Web avant son affichage.
                        </dd>
                        <dt>${ inline('<meta>') }</dt>
                        <dd>
                            La balise permettant de donner de l'information sur la page. Nous l'utilisons généralement pour indiquer certaines informations importante au navigateur. Elle 
                            se retrouve uniquement à l'intérieur de la balise ${ inline('<head>') }. Dans notre gabarit, nous l'utilisons pour indiquer l'encodage de la page Web (utf-8) 
                            et la fenêtre d'affichage (pour les versions mobiles).
                        </dd>
                        <dt>${ inline('<title>') }</dt>
                        <dd>
                            La balise permettant d'indiquer le titre de la page. Le titre de la page sera affiché dans l'onglet de la page Web dans le navigateur. Comme pour la balise
                            ${ inline('<meta>') }, la balise ${ inline('<title>') } se retrouve uniquement à l'intérieur de la balise ${ inline('<head>') }. De plus, une page Web doit 
                            contenir un seul titre.
                        </dd>
                        <dt>${ inline('<body>') }</dt>
                        <dd>
                            La balise contenant le corps de la page Web. Essentiellement, toutes les balises contrôlant l'affichage du site Web se retrouve à l'intérieur de cette balise.
                            Nous mettrons donc la majorité de nos balises dans la balise ${ inline('<body>') }.
                        </dd>
                    </dl>
                    <p>
                        Si vous ouvrez le fichier HTML ci-dessus dans votre navigateur, vous aurez simplement une page blanche avec un titre. Nous verrons dans les prochaines pages comment 
                        ajouter des éléments à notre page Web à l'aides de différentes balises.
                    </p>
                </section>
            </div>
        </body>

        </html>`
    }
})();