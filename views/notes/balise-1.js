let code = [];

code.push({});
code[0].html =
`<!-- Ceci n'est pas affiché -->
Ceci est affiché`;

code.push({});
code[1].html =
`<pre>
    Une tabulation
Un retour de ligne
Des           espaces
</pre>`;

code.push({});
code[2].html =
`<p>
    Ce texte n'est pas très important, mais il contient toutefois 
    <strong> une partie importante </strong> 
    pour démontrer à quoi sert la balise <strong>.
</p>`;

code.push({});
code[3].html =
`<h1>Un supertitre H1</h1>
<h2>Un titre H2</h2>
<h3>Un titre H3</h3>
<h4>Un titre H4</h4>
<h5>Un titre H5</h5>
<h6>Un titre H6</h6>`;

code.push({});
code[4].html =
`<p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut augue in odio volutpat aliquet a at ipsum. Donec lobortis est in lorem ultricies eleifend. 
    Etiam molestie est sem, et vehicula quam malesuada quis.
</p>`;

code.push({});
code[5].html = 
`Étapes de création d'une page Web:
<ol>
    <li>Créer le fichier HTML</li>
    <li>Mettre des trucs dans le fichier HTML</li>
    <li>Tester le fichier HTML</li>
</ol>`;

code.push({});
code[6].html =
`Liste de trucs à faire:
<ul>
    <li>Laboratoire de Web</li>
    <li>Un travail pour un autre cours</li>
    <li>Étudier</li>
</ul>`;

code.push({});
code[7].html = 
`Liste d'épicerie:
<ul>
    <li>
        Fruits
        <ul>
            <li>Pomme</li>
            <li>Orange</li>
            <li>Kiwi</li>
        </ul>
    </li>
    <li>
        Légume
    </li>
    <li>
        Autre
        <ol>
            <li>Bière</li>
            <li>Gâteau</li>
        </ol>
    </li>
</ul>`;

code.push({});
code[8].html =
`Définitions:
<dl>
    <dt>h1</dt>
    <dd>Balise d'entête</dd>

    <dt>p</dt>
    <dd>Balise de paragraphe</dd>

    <dt>ul</dt>
    <dd>Balise de liste non ordonnée</dd>
</dl>`;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Balises et concepts de base</h1>
            
            <section>
                <h2>Concepts de base</h2>
                <p>
                    Dans la page d'introduction au HTML, nous avons brièvement étudié les balises de base permettant de créer une page Web sans aucun contenu. Dans ce document, nous 
                    commencerons à regarder les balises permettant d'ajouter du contenu à notre page Web. Toutes ces balises doivent être ajouté à l'intérieur de la balise 
                    ${ inline('<body>') }. En effet, la balise ${ inline('<body>') } contient le contenu d'une page Web. Donc, la majorité des balises que nous ajouterons seront 
                    toujours dans la balise ${ inline('<body>') }.
                </p>
                <p>
                    Il est important de comprendre que le HTML est un langage de balisage hiérarchique. Ceci veut dire que l'on peut généralement mettre des balises dans une autre balise.
                    Ce principe est éssentiel au bon fonctionnement du HTML et sera utilisé très fréquemment. Vous aurez donc très souvent à mettre des balises dans une autre balise et
                    cela est normal. Habituez&#8209;vous rapidement!
                </p>
                <p>
                    Aussi, le HTML sert uniquement à structurer nos pages Web. Il ne sert donc aucunement à faire une "belle" page Web. Vous ne devez pas utiliser le HTML pour styliser vos 
                    interfaces. Nous utiliserons le CSS pour cela dans quelques semaines. Attendez&#8209;vous donc à ce que vos premières pages Web ne soit pas très belle pour le moment.
                </p>
                <p>
                    Finalement, le HTML gère les caractères blanc (espaces, tabulations et retours de lignes) de façon particulière. En effet, si le navigateur voit plusieurs caractères
                    blancs de suite, il les combinera simplement en un espace. Cela peut parfois porter à confusion lorsque nous commençons à utiliser le HTML alors essayez de vous 
                    rappeler de ce concept.
                </p>
            </section>

            <section>
                <h2>Commentaires ${ inline('<!-- -->') }</h2>
                Les commentaires, comme dans les langages de programmation, servent à aider à organiser le code dans un fichier. Les commentaires ne sont pas affichés lorsque la
                page HTML est affiché dans un navigateur Web.
                <code-example v-bind:code="code[0]"></code-example>
            </section>

            <section>
                <h2>Texte préformatté ${ inline('<pre></pre>') }</h2>
                En HTML, s'il y a plusieurs espaces ou retours de lignes, ceux-ci sont simplement considéré comme un seul espace lorsque la page Web est affiché. La balise
                ${ inline('<pre>') } est un moyen de prévenir ce comportement. Elle permet en effet d'afficher ce qu'elle contient tout en préservant les espaces, retours de lignes 
                ou tabulation. On l'utilise généralement pour afficher du code source dans une page Web.
                <code-example v-bind:code="code[1]"></code-example>
            </section>

            <section>
                <h2>Texte important ${ inline('<strong></strong>') }</h2>
                Si vous désirez spécifier qu'une partie du texte de votre page Web est important, vous pouvez utiliser la balise ${ inline('<strong>') }. Par défaut, la plupart des 
                navigateur Web afficheront les éléments dans une balise ${ inline('<strong>') } en gras. Toutefois, n'utilisez pas cette balise pour mettre du contenu en gras. Cette 
                balise ne sert qu'à indiquer l'importance d'un morceau de texte. Comme mentionné ci-dessus, le CSS que nous apprendrons dans quelques semaines nous permettra de 
                mettre du contenu en gras pour le côté visuel de notre site Web.
                <code-example v-bind:code="code[2]"></code-example>
            </section>

            <section>
                <h2>Header ${ inline('<h1></h1>') } ... ${ inline('<h6></h6>') }</h2>
                Les balises d'entête (header), servent à mettre des titres dans les pages Web. Essentiellement, le texte ou les balises qui se trouve dans les balises d'entête 
                seront considéré comme un titre dans la page. Le numéro dans le ${ inline('<h?>') } est un nombre de 1 à 6 représentant l'importance du titre. Ainsi, un titre 
                ${ inline('<h1>') } est un titre plus important, ou plus global qu'un titre ${ inline('<h2>') }. Vous devriez toujours utiliser les titres dans le bon ordre. Ainsi, 
                un ${ inline('<h4>') } devrait toujours être après un ${ inline('<h3>') } qui devrait toujours être après un ${ inline('<h2>') }, ainsi de suite. Vous ne devez donc 
                pas passer directement d'un ${ inline('<h1>') } à un ${ inline('<h5>') }.
                <code-example v-bind:code="code[3]"></code-example>
            </section>

            <section>
                <h2>Paragraphe ${ inline('<p></p>') }</h2>
                Les paragraphes servent à mettre un bloque de texte dans le HTML. Si vous n'avez que quelques mots à écrire, il n'est généralement pas nécessaire d'utiliser un 
                paragraphe. Toutefois, si vous avez au moins une phrase de texte, il est recommandé de la mettre dans un paragraphe.
                <code-example v-bind:code="code[4]"></code-example>
            </section>

            <section>
                <h2>Liste ordonnée ${ inline('<ol></ol>') }</h2>
                Les listes ordonnées servent principalement à afficher une liste de texte avec un numéro avant chacun d'eux. On l'utilise conjointement avec la balise
                ${ inline('<li>') } qui représente un élément de la liste.
                <code-example v-bind:code="code[5]"></code-example>
            </section>

            <section>
                <h2>Liste non ordonnée ${ inline('<ul></ul>') }</h2>
                Les listes non ordonnées servent principalement à afficher une liste de texte simplement avec des points ou barres. On l'utilise lui-aussi conjointement avec la 
                balise ${ inline('<li>') } qui représente un élément de la liste.
                <code-example v-bind:code="code[6]"></code-example>
            </section>

            <section>
                <h2>Liste imbriquée</h2>
                Les listes, ordonnées ou non peuvent être mises l'une dans l'autre pour créer plusieurs niveau dans la liste. Il est même possible de mélanger les listes ordonnée
                et non ordonnée ensemble. Pour ce faire, nous mettrons simplement des listes à l'intérieur des ${ inline('<li>') }.
                <code-example v-bind:code="code[7]"></code-example>
            </section>

            <section>
                <h2>Liste de description ${ inline('<dl></dl>') }</h2>
                Les listes de description sont utilisé pour afficher un terme ou une courte phrase, suivi de sa description. On l'utilisera conjointement avec les balises 
                ${ inline('<dt>') } pour donner le terme et ${ inline('<dd>') } pour donner la description.
                <code-example v-bind:code="code[8]"></code-example>
            </section>
        </div>`
};
