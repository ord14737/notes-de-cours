let code = [];

code.push({});
code[0].css =
`.selecteur {
    transition: [propriété] [durée] [fonction-de-timing] [délais]
}`;

code.push({});
code[1].exHtml =
`<input type="button" value="Cliquer et garder">
<div class="rushhour">
    <div class="rouge"></div>
    <div class="vert"></div>
    <div class="orange"></div>
    <div class="bleu"></div>
    <div class="jaune"></div>
</div>`;

code[1].exCss =
`.rushhour {
    position: relative;
    background-color: #DDD;
    height: 20em;
    overflow: hidden;
}

.rouge, .vert, .orange, .bleu, .jaune {
    position: absolute;
}

input:active + .rushhour > .rouge {
    left: 100%;
    transition: all 1s ease-in 2s;
}

input:active + .rushhour > .vert {
    top: 10em;
    transition: all .5s ease 0s;
}

input:active + .rushhour > .orange {
    top: 10em;
    transition: all .5s ease .5s;
}

input:active + .rushhour > .bleu {
    top: 10em;
    transition: all .5s ease 1s;
}

input:active + .rushhour > .jaune {
    top: 10em;
    transition: all .5s ease 1.5s;
}

.rouge {
    background-color: #A00;
    width: 10em;
    height: 5em;
    top: 5em;
    left: 0;
    transition: all 1s ease-out 0s;
}

.vert {
    background-color: #02631f;
    width: 5em;
    height: 15em;
    top: 5em;
    left: 15em;
    transition: all .5s ease 2.5s;
}

.orange {
    background-color: #ed6700;
    width: 5em;
    height: 10em;
    top: 0;
    left: 20em;
    transition: all .5s ease 2s;
}

.bleu {
    background-color: #0e11a1;
    width: 5em;
    height: 10em;
    top: 5em;
    left: 25em;
    transition: all .5s ease 1.5s;
}

.jaune {
    background-color: #ebd549;
    width: 5em;
    height: 15em;
    top: 0;
    left: 30em;
    transition: all .5s ease 1s;
}`;

code.push({});
code[2].exHtml =
`linear
<div class="slider">
    Je vais toujours à la même vitesse.
    <div class="box linear"></div>
</div>
ease
<div class="slider">
    Je commence lentement, je vais vite au milieu et je fini lentement.
    <div class="box ease"></div>
</div>
ease-in
<div class="slider">
    Je commence lentement et je fini vite.
    <div class="box ease-in"></div>
</div>
ease-in-out
<div class="slider">
    Je commence vraiment lentement, je vais vraiment vite au milieu et je fini vraiment lentement.
    <div class="box ease-in-out"></div>
</div>
ease-out
<div class="slider">
    Je commence vite et je fini lentement.
    <div class="box ease-out"></div>
</div>`;

code[2].css = 
`.linear {
    transition: all 1s linear;
}

.ease {
    transition: all 1s ease;
}

.ease-in {
    transition: all 1s ease-in;
}

.ease-in-out {
    transition: all 1s ease-in-out;
}

.ease-out {
    transition: all 1s ease-out;
}`;

code[2].exCss =
`.slider {
    border: 1px solid #CCC;
    position: relative;
    height: 3em;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
}

.box {
    position: absolute;
    width: 3em;
    height: 3em;
    background-color: #333;
    top: 0;
    left: 0;
}

.slider:hover .box {
    left: 100%;
    transform: translateX(-100%)
}` + 
code[2].css;

code.push({});
code[3].html =
`Même si la transition sur la couleur du texte est valide, il n'y a aucune transition puisqu'il y a un changement de type d'affichage.
<div class="container">
    <div>:_(</div>
</div>`;

code[3].css =
`.container {
    display: flex;
    justify-content: center;
    width: 4em;
    height: 4em;
    background-color: #CCC;
}

.container:hover div {
    display: block;
    color: #F78F20;
}

.container div {
    display: none;
    font-size: 3em;
    transform: rotate(90deg);
    transition: all 1s ease;
}`;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Transition</h1>

            <section>
                <h2>Introduction</h2>
                <p>
                    Les transitions sont un moyen simple d'animer les éléments de nos pages Web lorsque nous activons des règles CSS. Les transitions nous permettent essentiellement 
                    d'animer n'importe quelle propriété CSS lorsque sa valeur change, ce qui nous permet de rendre notre site Web plus beau et de plaire aux designers graphiques. Elles
                    sont donc un supplément intéressant à vos pages Web.
                </p>
            </section>

            <section>
                <h2>Syntaxe</h2>
                <p>
                    La syntaxe de base d'une transition est la suivante:
                </p>
                <styled-code v-bind:code="code[0].css" lang="CSS"></styled-code>
                <dl>
                    <dt>${ inline('[propriété]') }</dt>
                    <dd>
                        Indique sur quelle propriété vous voulez appliquer la transition. Cela vous permet ainsi d'avoir une transition sur certaines propriétés, mais pas sur d'autres.
                        Il est aussi possible d'utiliser le mot-clé ${ inline('all') } pour avoir une transition sur toutes les propriétés en même temps. Dans le même style, vous pouvez 
                        mettre ${ inline('none') } pour indiquer de ne plus faire de transition.
                    </dd>

                    <dt>${ inline('[durée]') }</dt>
                    <dd>
                        Indique la durée totale de la transition, donc essentiellement, la longueur de l'animation. Celle-ci doit être spécifié avec une unité de mesure soit en secondes 
                        (${ inline('s') }) ou en millisecondes (${ inline('ms') }).
                    </dd>

                    <dt>${ inline('[fonction-de-timing]') }</dt>
                    <dd>
                        Fonction indiquant à quelle vitesse exécuter notre animation en fonction du temps. Il est possible de définir ses propres fonctions ici, mais nous utiliserons 
                        généralement celle de base qui nous sont fournie par le navigateur.
                    </dd>

                    <dt>${ inline('[délais]') }</dt>
                    <dd>
                        C'est le délais avant que la transition démarre. Comme pour la durée, nous avons besoin d'une valeur en secondes (${ inline('s') }) ou en millisecondes 
                        (${ inline('ms') }).
                    </dd>
                </dl>
            </section>

            <section>
                <h2>Exemple concret</h2>
                <code-example v-bind:code="code[1]"></code-example>
            </section>

            <section>
                <h2>Fonction de timing</h2>
                <p>
                    Les navigateurs Web nous offrent des fonctions de timing pour nous simplifier la tâche dans nos transitions. Pour utiliser une de ces fonctions, nous avons uniquement
                    besoin de spécifier son nom. Voici donc un exemple de chacune des fonctions de timing prédéfinies par les navigateurs:
                </p>
                <code-example v-bind:code="code[2]"></code-example>
            </section>

            <section>
                <h2>Limites</h2>
                <p>
                    Bien que les transitions sont très pratiques, elles ont tout de même quelques limites. Une des plus connue est que les transitions ne fonctionnent pas avec la 
                    propriété ${ inline('display') }. Le pire dans tout ça: si vous avez un changement d'affichage, aucune transition ne fonctionnera sur l'élément, même si cette 
                    transition serait valide. Bref, si nous voulons afficher ou cacher un élément, vous ne pouvez donc pas mettre de transtion, à moins d'utiliser quelques petits
                    trucs que vous pourrez trouver sur le Web si vous en avez besoin.
                </p>
                <code-example v-bind:code="code[3]"></code-example>
            </section>
        </div>`
}