let code = [];

code.push({});
code[0].js = 
``;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Objets</h1>

            <section>
                <h2>Introduction</h2>
                <p>
                    Les objets sont un moyen de regrouper plusieurs valeurs dans une seule et même variable. On les utilise principalement pour mieux organiser notre code. Contrairement
                    à beaucoup de langages de programmation, les objets en Javascript ne sont pas nécessairement utilisé avec des classes. En effet, la majorité du temps, on déclare les
                    objets directements, sans passer par une classe. De plus, le concept de contenu privé ou publique n'existe pas en Javascript. Tout cela simplifie énormément 
                    l'utilisation de objets, mais peut favoriser un peu le code spaghetti si on ne fait pas attention.
                </p>
            </section>

            <section>
                <h2>Ressources externes</h2>
                <p>
                    Voici quelques ressources externes qui vous permettront d'apprendre comment utiliser les objets en Javascript.
                </p>
                <ol>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Utiliser_les_objets" target="_blank">
                            MDN
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://www.w3schools.com/js/js_objects.asp" target="_blank">
                            w3school
                        </a>
                    </span></li>
                </ol>
            </section>

            <section>
                <h2>Capsule vidéo</h2>
                <div class="video">
                    <div>
                        <iframe
                            src="https://www.youtube.com/embed/V7-EROb0Rjg" 
                            frameborder="0" 
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </section>
        </div>`
};