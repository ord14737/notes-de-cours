let code = [];

code.push({});
code[0].html =
`<div class="box"></div>`;

code[0].css = 
`.box {
    background-color: #CCC;
    height: 2em;
}

@media (min-width: 1000px){
    .box {
        background-color: #333;
    }
}`;

code.push({});
code[1].html =
`<div class="mobile"> Je suis affiché sur mobile </div>
<div class="ordinateur"> Je suis affiché sur un ordinateur </div>`;

code[1].css = 
`.ordinateur {
    display: none;
    background-color: #CCC
}

.mobile {
    background-color: #333;
    color: #CCC;
}

@media (min-width: 1000px){
    .mobile {
        display: none;
    }

    .ordinateur {
        display: block;
    }
}`;

code.push({});
code[2].css = 
`/*
 Règle CSS de base et pour mobile
*/
html {
    box-sizing: border-box;
}

*, *::before, *::after {
    box-sizing: inherit;
}



/*
 Règle à ajouter pour tablette électronique
*/
@media (min-width: 640px){
    
}

/*
 Règle à ajouter pour ordinateur
*/
@media (min-width: 1200px){
    
}
`;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Approche "mobile-first" et media queries</h1>

            <section>
                <h2>Développement pour appareil mobile</h2>
                <p>
                    Selon de nombreuses sources en ligne, nos sites Web sont maintenant vu autant, sinon même plus sur appareil mobile que sur ordinateur de bureau. Ce changement majeur 
                    de navigation sur l'Internet nous oblige, nous les développeurs, à nous adapter à nos consommateurs ainsi qu'à nos clients. Même si nous sommes habitué à travailler 
                    et à naviguer l'Internet sur un ordinateur (puisquer c'est notre travail), cela n'est généralement pas le cas de nos clients.
                </p>
                <p>
                    Ce changement de l'utilisation du Web nous force à faire un changement majeur: développer nos sites Web pour un appareil mobile en premier lieu, puis graduellement 
                    le convertir pour tablettes éléctroniques, puis ordinateurs. Cette approche est nommée "mobile-first" puisque l'on pense à notre design et à notre développement en
                    commençant par les appareils mobiles. À partir de maintenant, nous allons toujours commencer le développement d'un site Web à partir de sa version mobile. 
                    Redimensionnez donc votre navigateur pour qu'il est une petite largeur pour facilement tester votre design.
                </p>
            </section>

            <section>
                <h2>Design réactif (Responsive design)</h2>
                <p>
                    Développer en "mobile-first" ne veut toutefois pas dire d'oublier les tablettes électroniques ou les ordinateurs. Nous utiliserons donc ce qu'on appelle le design
                    réactif. Ce type de design signifie simplement que dépendant de la taille du navigateur Web, nous déplacerons et changerons les éléments de notre page HTML pour que
                    ceux-ci soient affichés correctement. Les Flexbox et les éléments ${ inline('inline-block') } sont très pratique pour ce genre de design. Toutefois, si on veut 
                    afficher des éléments, cacher des éléments, changer la taille du texte ou encore changer le type d'affichage de nos menus en fonction de la taille du navigateur, 
                    nous allons avoir besoin d'autres outils.
                </p>
            </section>

            <section>
                <h2>Briser notre page en fonction de sa taille</h2>
                <p>
                    Dans notre CSS, il est possible d'ajouter des règles CSS seulement sous certaines conditions avec la règle ${ inline('@media') }. Ces conditions, que nous appelerons 
                    fréquemment "media queries" peuvent être le type de media (par exemple, ajouter des règles si on imprime la page ou si elle est visité par un lecteur de page Web), 
                    la résolution de l'écran, la quantité de mémoire utilisé pour les couleurs, l'orientation de l'écran ou encore la largeur de la page Web. C'est en général cette 
                    dernière que nous utiliserons pour nos designs réactifs.
                </p>
                <code-example v-bind:code="code[0]"></code-example>
                <p>
                    Dans l'exemple ci-dessus, nous appliquons toutes les règles CSS qui sont dans le bloc ${ inline('@media') } si la taille de la page est au moins 1000px. Ainsi, si vous
                    redimensionnez votre navigateur, vous verrez la couleur de la boîte changer si la taille de la page est plus petite ou plus grande que 1000px. Voici un autre exemple
                    très pratique:
                </p>
                <code-example v-bind:code="code[1]"></code-example>
                <p>
                    L'exemple ci-dessus démontre bien comment l'on peut afficher ou cacher des éléments en fonction de la taille de la page, ce qui est assez fréquent dans un design 
                    réactif.
                </p>
                <p>
                    Vous pensez pêut-être que vous aurez à modifier beaucoup d'élément HTML pour les changements entre mobile, tablette et ordinateur. Vous verrez toutefois que la 
                    majorité du temps, ce n'est pas le cas. Pour voir un exemple concret, inspectez ce site Web et regarder le fichier CSS. vous verrez que très peu de propriétés CSS
                    se retrouve dans les règles ${ inline('@media') }.
                </p>
            </section>

            <section>
                <h2>Gabarit de base</h2>
                <p>
                    Puisque nous utiliserons maintenant toujours le design réactif, je vous suggère fortement d'utiliser le gabarit suivant pour vos fichiers CSS. Si vous avez besoin
                    d'ajouter d'autres "breakpoints", n'hésitez pas à ajouter d'autres règles ${ inline('@media') }. Il en est de même si vous en avez trop, n'hésitez pas à en retirer.
                </p>
                <styled-code v-bind:code="code[2].css" lang="CSS"></styled-code>
            </section>
        </div>`
};
