export default (function() {
let code1Html = 
`<input type="number">
<input type="text" maxlength="10">
<input type="radio">
<input type="checkbox">`;

let code1Css = 
`input[type=text], input[type=checkbox] {
    box-shadow: inset 0 0 1em #000;
}

input[maxlength] {
    border: 2px solid #F00;
}`;

let code2Html = 
`<img src="./img/weedle.webp">

<p class="test"> Lorem Ipsum 1 </p>
<p class="test"> Lorem Ipsum 2 </p>
<p> Lorem Ipsum 3 </p>`;

let code2Css = 
`/* Sélectionne toutes les images qui n'ont pas d'attribut "alt" */
img:not([alt]) {
    width: 5em;
    border: 3px solid #A22;
    box-shadow: inset 0 0 1em #F00;
}

/* Sélectionne tous les paragraphes qui n'ont pas la classe "test" */
p:not(.test){
    font-weight: bold;
    color: #22A;
}`;

let code3Css = 
`main .wrapper ul li {
    ...
}`;

let code4Html = 
`<div class="principal">
    <p> Paragraphe directement dans la division principale. </p>
    <div>
        <p> Paragraphe dans une autre division. </p>
    </div>
</div>`;

let code4Css = 
`.principal p {
    font-weight: bold;
}

.principal > p {
    font-size: 1.5em;
}`;

let code5Html = 
`<div class="carre"> Click me! </div>
<div class="cercle"> Cercle </div>`;

let code5Css = 
`.carre, .cercle {
    width: 5em;
    height: 5em;
    text-align: center;
}

.carre {
    background-color: #F77;
}

.cercle {
    background-color: #77F;
    border-radius: 50%;
    display: none;
}

.carre:active {
    display: none;
}

.carre:active + .cercle {
    display: block;
}`;

let code6Html = 
`<p>Paragraphe avant</p>

<div class="carre"> Carré </div>
<div class="cercle"> Cercle </div>

<p>Paragraphe après</p>`;

let code6Css = 
`.carre, .rectangle, .cercle {
    display: inline-block;
    width: 5em;
    height: 5em;
    text-align: center;
    vertical-align: middle;
}

.carre {
    background-color: #F77;
}

.cercle {
    background-color: #77F;
    border-radius: 50%;
}

.carre ~ p {
    font-weight: bold; 
}

.cercle ~ p {
    font-size: 1.5em; 
}`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Sélecteur CSS d'attributs et de voisins</h1>

                <section>
                    <h2>Attribut HTML</h2>
                    <p>
                        Il est possible de sélectionner des éléments HTML à partir de leur attribut et potentiellement de la valeur de ces attributs. Par exemple, ceci peut être 
                        pratique pour sélectionner les lien qui n'ont pas de ${ inline('href') } ou encore pour sélectionner les inputs de type ${ inline('text') }. Pour sélectionner
                        à partir des attributs, nous utiliserons les accolades carrées ${ inline('[ ]') }
                    </p>
                    ${ addCodeExample(code1Html, code1Css) }
                    <p>
                        Pour plus d'information sur les sélecteurs par attribut, vous pouvez visiter 
                        <a href="https://www.w3schools.com/css/css_attribute_selectors.asp" target="_blank">w3school</a>
                    </p>
                </section>

                <section>
                    <h2>Inversion d'un sélecteur</h2>
                    <p>
                        Il est possible d'inverser un sélecteur, ce qui nous permet de sélectionner tout ce que notre sélecteur ne sélectionne pas. Pour ce faire, nous utiliserons la 
                        pseudo-classe ${ inline(':not') }.
                    </p>
                    ${ addCodeExample(code2Html, code2Css) }
                </section>

                <section>
                    <h2>Sélectionner un enfant direct</h2>
                    <p>
                        Si vous désirez sélectionner un élément à l'intérieur d'un autre élément, nous utilisions jusqu'à présent l'imbrication de sélecteurs comme ceci:
                    </p>
                    ${ addCode(code3Css, 'CSS') }
                    <p>
                        Ce genre de sélecteur fonctionne bien, mais si l'on veut sélectionner un enfant direct d'un autre élément, vous ne pourrez pas le faire. En effet, le sélecteur
                        d'imbrication permet de sélectionner un élément à l'intérieur d'un autre peu importe à quelle profondeur il se trouve. Si vous voulez un enfant direct, vous 
                        utiliserez plutôt le sélecteur ${ inline('>') }.
                    </p>
                    ${ addCodeExample(code4Html, code4Css) }
                </section>

                <section>
                    <h2>Sélectionner un voisin</h2>
                    <p>
                        Les sélecteurs de voisin sont un peu plus complexe. Ils permettent de sélectionner un élément à partir d'un de ses voisins, donc d'un élément au même niveau.
                        Il existe 2 sélecteurs de ce type, soit le ${ inline('+') } et le ${ inline('~') }.
                    </p>
                    <p>
                        Le premier, le ${ inline('+') }, permet de sélectionner un élément qui suit directement un autre élément.
                    </p>
                    ${ addCodeExample(code5Html, code5Css) }
                    <p>
                        Le deuxième sélecteur, le ${ inline('~') }, permet de sélectionner un élément qui est précédé, directement ou non d'un autre élément.
                    </p>
                    ${ addCodeExample(code6Html, code6Css) }
                </section>
            </div>`
    }
})();