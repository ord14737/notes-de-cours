export default (function() {
let code1Html =
`<p> 
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, aperiam. Quibusdam eius quod sit fuga nulla ipsam
    officia accusantium minus nam, repellat nostrum quisquam a animi ea. Sunt, cum incidunt.
</p>
<span class="position"> Bonjour! </span>`;

let code1Css =
`.position {
    position: static;
    color: #B22;

    /* Ne fait rien */
    top: 10em;
}`;

let code2Html =
`<p> 
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, aperiam. Quibusdam eius quod sit fuga nulla ipsam
    officia accusantium minus nam, repellat nostrum quisquam a animi ea. Sunt, cum incidunt.
</p>
<span class="position"> Bonjour! </span>`;

let code2Css =
`.position {
    position: relative;
    color: #B22;

    left: 3em;
    top: -1em;
}`;

let code3Html =
`<p> 
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, aperiam. Quibusdam eius quod sit fuga nulla ipsam
    officia accusantium minus nam, repellat nostrum quisquam a animi ea. Sunt, cum incidunt.
</p>

<div class="container">
    <div class="box red"></div>
</div>

<div class="box blue"></div>`;

let code3Css =
`.container {
    position: relative;
    height: 8em;
    border: 4px solid #666;
}

.box {
    position: absolute;
    width: 4em;
    height: 4em;
}

.box.red {
    bottom: 0;
    right: 0;
    background-color: #F00;
}

.box.blue {
    top: 0;
    left: 0;
    background-color: #00F;
}`;

let code4Html =
`<div class="container" v-once>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus quisquam exercitationem eum, harum corrupti 
    quo libero provident dolore at voluptatum nisi ad ipsam molestiae atque eius cupiditate porro non eos. Lorem 
    ipsum dolor sit amet consectetur adipisicing elit. Accusamus quisquam exercitationem eum, harum corrupti quo 
    libero provident dolore at voluptatum nisi ad ipsam molestiae atque eius cupiditate porro non eos. Lorem ipsum 
    dolor sit amet consectetur adipisicing elit. Accusamus quisquam exercitationem eum, harum corrupti quo libero 
    provident dolore at voluptatum nisi ad ipsam molestiae atque eius cupiditate porro non eos. Lorem ipsum dolor 
    sit amet consectetur adipisicing elit. Accusamus quisquam exercitationem eum, harum corrupti quo libero 
    provident dolore at voluptatum nisi ad ipsam molestiae atque eius cupiditate porro non eos. Lorem ipsum dolor 
    sit amet consectetur adipisicing elit. Accusamus quisquam exercitationem eum, harum corrupti quo libero 
    provident dolore at voluptatum nisi ad ipsam molestiae atque eius cupiditate porro non eos. Lorem ipsum dolor 
    sit amet consectetur adipisicing elit. Accusamus quisquam exercitationem eum, harum corrupti quo libero 
    provident dolore at voluptatum nisi ad ipsam molestiae atque eius cupiditate porro non eos.

    <div class="fixed-box"></div>
</div>`;

let code4Css =
`.container {
    height: 5em;

    /* Permet de faire afficher la barre de défilement verticale */
    overflow-y: scroll;
}

.fixed-box {
    position: fixed;
    top: 0;
    left: 50%;

    width: 4em;
    height: 4em;
    background-color: #0F0;
}`;

let code5Html =
`<div class="container">
    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, amet voluptatem nisi corporis officia 
        mollitia quae delectus, ut quibusdam cumque laborum veniam? Laborum iste facilis molestias odit, quis aliquam 
        exercitationem?
    </p>
    <div class="sticky-box">Sticky</div>
    <p> Lorem ipsum 1 </p> <p> Lorem ipsum 2 </p> <p> Lorem ipsum 3 </p>
    <p> Lorem ipsum 4 </p> <p> Lorem ipsum 5 </p> <p> Lorem ipsum 6 </p>
    <p> Lorem ipsum 7 </p> <p> Lorem ipsum 8 </p> <p> Lorem ipsum 9 </p>
</div>`;

let code5Css =
`.container {
    height: 8em;

    /* Permet de faire afficher la barre de défilement verticale */
    overflow-y: scroll;
}

.sticky-box {
    position: -webkit-sticky; /* Safari */
    position: sticky;
    top: 0;

    background-color: #AAA;
    border: 3px solid #333;
}`;

let code6Html =
`<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae labore esse consequuntur repellendus dolore amet 
    adipisci sapiente fugit? Libero, ex quod. Voluptatibus beatae quod magni debitis illum exercitationem sit vel. Lorem 
    ipsum dolor sit amet consectetur adipisicing elit. Repudiandae labore esse consequuntur repellendus dolore amet adipisci 
    sapiente fugit? Libero, ex quod. Voluptatibus beatae quod magni debitis illum exercitationem sit vel. Lorem ipsum dolor 
    sit amet consectetur adipisicing elit. Repudiandae labore esse consequuntur repellendus dolore amet adipisci sapiente 
    fugit? Libero, ex quod. Voluptatibus beatae quod magni debitis illum exercitationem sit vel.
</p>
<div class="box over"> Dessus </div>
<div class="box middle"> Milieu </div>
<div class="box under"> Dessous </div>`;

let code6Css =
`.box {
    position: absolute;
    text-align: center;
    font-weight: bold;
    height: 6em;
}

.box.under {
    top: 1em;
    left: 40%;
    right: 40%;
    background-color: #777;

    z-index: -1000;
}

.box.middle {
    top: 3em;
    left: 42%;
    right: 42%;
    background-color: #999;

    z-index: 0;
}

.box.over {
    top: 5em;
    left: 44%;
    right: 44%;
    background-color: #BBB;
    
    z-index: 500;
}`;

    return {
        template: `
            <div class="note">
                <h1>Positionnement</h1>

                <section>
                    <h2>Propriété et valeurs</h2>
                    <p>
                        La propriété CSS ${ inline('position') } permet de définir une méthode de positionnement d'un élément dans une page Web. Il ne faut pas confondre cette propriété
                        avec la propriété ${ inline('display') }, qui elle change la méthode d'affichage d'un élément. Il existe 5 méthodes de positionnement.
                    </p>
                    <ul>
                        <li><span>${ inline('static') }</span></li>
                        <li><span>${ inline('relative') }</span></li>
                        <li><span>${ inline('fixed') }</span></li>
                        <li><span>${ inline('sticky') }</span></li>
                        <li><span>${ inline('absolute') }</span></li>
                    </ul>
                    <p>
                        La plupart des méthodes de positionnement utilise les propriétés ${ inline('top') }, ${ inline('right') }, ${ inline('bottom') } et ${ inline('left') } pour 
                        déplacer l'élément dans la page Web en fonction du côté spécifié. Ces propriétés accepte une valeur ayant n'importe quel unité de mesure de distance. On 
                        favorisera toutefois les ${ inline('em') }, ${ inline('rem') } et même parfois ${ inline('px') }.
                    </p>
                </section>

                <section>
                    <h2>Position ${ inline('static') }</h2>
                    <p>
                        C'est le mode de positionnement par défaut. Ce positionnement ne fait rien de spécial. Il assure simplement que les éléments suivent le flux normal de la page.
                        Puisque c'est la valeur par défaut, vous aurez très rarement besoin de spécifier cette valeur. Dans ce mode de positionnement, les propriétés ${ inline('top') }, 
                        ${ inline('right') }, ${ inline('bottom') } et ${ inline('left') } ne font rien.
                    </p>
                    ${ addCodeExample(code1Html, code1Css) }
                </section>

                <section>
                    <h2>Position ${ inline('relative') }</h2>
                    <p>
                        Le mode de positionnement ${ inline('relative') } permet de déplacer un élément par rapport à sa position originale dans le flux de la page Web à l'aide des 
                        propriétés ${ inline('top') }, ${ inline('right') }, ${ inline('bottom') } et ${ inline('left') }.
                    </p>
                    ${ addCodeExample(code2Html, code2Css) }
                    <p>
                        Nous utiliserons aussi fréquement le positionnement ${ inline('relative') } pour contrôler le positionnement ${ inline('absolute') } que nous verrons ci-dessous.
                    </p>
                </section>

                <section>
                    <h2>Position ${ inline('absolute') }</h2>
                    <p>
                        Ce mode de positionnement permet de déplacer un élément dans la page Web par rapport à son premier parent le plus proche qui a un positionnement autre que 
                        ${ inline('static') }. On l'utilisera donc beaucoup conjointement avec le positionnement ${ inline('relative') }. Si aucun des parents d'un élément ayant un 
                        positionnement ${ inline('absolute') } n'a de position autre que ${ inline('static') }, cet élément sera positionné par rapport au ${ inline('<body>') } de la 
                        page.
                    </p>
                    ${ addCodeExample(code3Html, code3Css) }
                </section>

                <section>
                    <h2>Position ${ inline('fixed') }</h2>
                    <p>
                        Si vous désirez positionner un élément par rapport à la fenêtre de votre navigateur, vous utiliserez le mode de positionnement ${ inline('fixed') }. La 
                        particularité de ce mode de positionnement est que l'élément affecté reste à la même place dans l'écran même quand on fait défiler la page.
                    </p>
                    ${ addCodeExample(code4Html, code4Css) }
                </section>

                <section>
                    <h2>Position ${ inline('sticky') }</h2>
                    <p>
                        Le mode de positionnement ${ inline('sticky') } est un mixe entre le positionnement ${ inline('relative') } et ${ inline('fixed') }. Il permet à l'élément de 
                        rester à sa place dans la page jusqu'à ce que le défilement de la page atteigne une certaine valeur. Après ça, il utilise le positionnement ${ inline('fixed') }.
                        Si vous désirez utiliser ce genre de propriété, notez que pour fonctionne aussi sur Safari, vous devrez ajouter un préfix.
                    </p>
                    ${ addCodeExample(code5Html, code5Css) }
                </section>

                <section>
                    <h2>Chevauchements</h2>
                    <p>
                        Si vous jouez un peu avec les positionnements autre que ${ inline('static') }, vous rencontrerez probablement des chevauchements entre les éléments HTML. Il 
                        arrive parfois que le chevauchement ne se fasse pas de la façon que vous voulez. Par exemple, un élément pourrait apparaître en dessous d'un autre alors que vous
                        voulez qu'il apparaîsse au-dessus. Dans ce cas, la propriété ${ inline('z-index') } vous permet de définir un ordre de priorité pour l'affichage. Plus le 
                        ${ inline('z-index') } est grand, plus il va apparaître au-dessus des autres éléments. Le contraire est aussi valide. Un ${ inline('z-index') } petit affichera 
                        les éléments en dessous des autres.
                    </p>
                    ${ addCodeExample(code6Html, code6Css) }
                    <p>
                        L'index du contenu normal de votre page Web est zéro. Par conséquant, si vous utilisez un ${ inline('z-index') } négatif, vous vous retrouverez probablement en 
                        dessous du contenu principal de la page. Si 2 éléments ont le même ${ inline('z-index') }, alors le dernier affiché sera au-dessus.
                    </p>
                </section>
            </div>`
    }
})();