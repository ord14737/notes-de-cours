export default {
    template: `
        <div class="note" v-once>
            <h1>Introduction au DOM</h1>

            <section>
                <h2>Ressources sur le Javascript</h2>
                <p>
                    Voici quelques ressources externes qui vous permettront d'apprendre comment utiliser les fonctions en Javascript.
                </p>
                <ol>
                    <li><span>
                        <a href="https://www.w3schools.com/js/js_htmldom.asp" target="_blank">
                            Tutoriel w3school sur le DOM et son utilisation
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/API/Document_Object_Model/Introduction" target="_blank">
                            Référence complète sur tous les éléments du DOM
                        </a>
                    </span></li>
                </ol>
            </section>

            <section>
                <h2>Capsule vidéo</h2>
                <div class="video">
                    <div>
                        <iframe
                            src="https://www.youtube.com/embed/ta9ixnE8uKc" 
                            frameborder="0" 
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </section>
        </div>`
};