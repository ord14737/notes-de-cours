export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Validation du HTML</h1>

                <section>
                    <h2>SEO et compatibilité</h2>
                    <p>
                        Parfois, bien qu'un site Web semble s'afficher correctement dans un navigateur Web, son HTML n'est pas valide. Cela peut arriver de plusieurs façon, comme une 
                        balise mal fermée, une mauvaise utilisation de certains attributs ou encore parce que vous avez oublié de mettre le ${ inline('<!doctype html>') }. Bien que 
                        cela ne semble pas être vraiment utile à corriger puisque les navigateurs sont très permissif, il y a de très bonnes raison pour lesquelles on devrait toujours
                        avoir un HTML valide:
                    </p>
                    <dl>
                        <dt>SEO</dt>
                        <dd>
                            Cet acronyme en anglais veut dire "Search Engine Optimization", donc optimization des engins de recherche. Essentiellement, un site Web avec un HTML valide 
                            sera mieux classé lorsque les engins de recherche les indexeront. Bref, un HTML valide veut dire que vous avez plus de chance d'être mieux classé dans les
                            recherches sur Google.
                        </dd>

                        <dt>Compatibilité</dt>
                        <dd>
                            Certains navigateur sont beaucoup moins permissif que d'autres. C'est entres autres le cas des navigateurs qui ont accès à moins de ressources, comme les 
                            navigateur sur des téléphones cellulaires ou sur des consoles de jeux. Si vous voulez que votre site Web fonctionne bien partout, il est important que son
                            HTML soit valide.
                        </dd>
                    </dl>
                </section>

                <section>
                    <h2>Outils de validation</h2>
                    <p>
                        Très souvent, vos éditeurs de code Web ne vous afficheront pas par défaut si votre HTML n'est pas valide. Il existe toutefois plusieurs services pour vous 
                        aider. On utilisera souvent des outils qui nous permettent de faire du "linting". Ce terme signifie "Analyser le code pour en ressortir les erreurs". Voici donc
                        quelques outils qui peuvent vous aider:
                    </p>
                    <dl>
                        <dt><a href="https://validator.w3.org/#validate_by_input" target="_blank">Service de validation du W3C</a></dt>
                        <dd>
                            Le W3C (World Wide Web Consortium) est l'organisme qui est derrière la standardisation du Web. Sans ce consortium, le Web serait encore un total chaos.
                            Derrières ce consortium, il y a de nombreuses compagnies, comme Google, Microsoft, Apple, Amazon. Toutefois, on y retrouve aussi un nombre phénoménal de
                            développeur indépendant venant de partout dans le monde. Bref, ils ont fait un validateur de HTML que vous pouvez retrouver sur leur site Web.
                        </dd>

                        <dt><a href="https://marketplace.visualstudio.com/items?itemName=Umoxfo.vscode-w3cvalidation" target="_blank">Extension Visual Studio Code</a></dt>
                        <dd>
                            Une extension de Visual Studio Code se nommant "W3C Validation" vous permettra de valider votre HTML. Assurez-vous d'installer Java avant d'utiliser cette
                            extension parce que sinon, elle ne fonctionnera pas.
                        </dd>
                    </dl>
                </section>
            </div>`
    }
})();