export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Balises sémantiques</h1>

                <section>
                    <h2>Décrire son sens</h2>
                    <p>
                        Une balise sémantique est une balise qui ne fait, à proprement parler, rien de spécial, mais qui a un sens. En général, ces balises indiquent par leur nom ce 
                        qu'elles contiennent. À première vu, ces balises ne semblent pas être utile, mais vous les utiliserez souvent. En effet, elles sont très pratique pour les 
                        lecteurs de site Web pour les personnes ayant des handicaps de la vue ou encore pour l'analyse des sites Web par d'autres programmes. Dans cette page, nous 
                        verrons quelques balises sémantiques que vous devrez utiliser dans vos site Web.
                    </p>
                </section>

                <section>
                    <h2>Entête ${ inline('<header></header>') }</h2>
                    <p>
                        Il ne faut pas confondre cette balise avec la balise ${ inline('<head>') }. Cette balise contient l'entête d'une page Web ou parfois d'une section d'une page 
                        Web. Elle contient donc généralement le logo du site Web et son menu. Si vous inspectez ce site Web, vous remarquerez que la barre avec le logo et le menu sont 
                        contenu dans une balise ${ inline('<header>') }.
                    </p>
                </section>

                <section>
                    <h2>Pied de page ${ inline('<footer></footer>') }</h2>
                    <p>
                        Cette balise contient le pied d'une page Web ou parfois d'une section d'une page Web. Elle contient donc généralement l'auteur du site Web, les informations de 
                        contact, des liens vers des documents connexes et souvent le &copy;copyright. Si vous inspectez ce site Web, vous remarquerez que la barre en bas du site Web 
                        avec le copyright et l'auteur du site Web est contenu dans une balise ${ inline('<footer>') }.
                    </p>
                </section>

                <section>
                    <h2>Contenu principal ${ inline('<main></main>') }</h2>
                    <p>
                        Cette balise contient le contenu principal d'une page Web. Cette balise ne devrait pas contenir de contenus qui se répète sur plusieurs pages, comme les barres 
                        de navigation, les entêtes ou les pieds de page. Cette balise contient le contenu principal de la page, peu importe son contenu. Si vous inspectez ce site Web, 
                        vous remarquerez que ce texte se retrouve éventuellement dans une balise ${ inline('<main>') }.
                    </p>
                </section>

                <section>
                    <h2>Contenu à côté ${ inline('<aside></aside>') }</h2>
                    <p>
                        Cette balise contient des éléments qui se retrouve en bordure de votre site Web. On y met souvent les menus de navigation de la page. Si vous inspectez ce site 
                        Web, vous remarquerez que le menu de gauche du site Web se retrouve dans une balise ${ inline('<aside>') }.
                    </p>
                </section>

                <section>
                    <h2>Navigation ${ inline('<nav></nav>') }</h2>
                    <p>
                        Cette balise contient généralement un menu de navigation d'un site Web. Cette balise contient généralement une liste de liens vers différentes pages de votre 
                        site Web et se retrouve souvent dans une balise ${ inline('<header>') }, ${ inline('<footer>') } ou ${ inline('<aside>') }. Si vous inspectez ce site Web, vous 
                        remarquerez que le menu dans l'entête et le menu dans la partie sur le côté du site Web sont tous 2 dans une balise ${ inline('<nav>') }.
                    </p>
                </section>

                <section>
                    <h2>Section ${ inline('<section></section>') }</h2>
                    <p>
                        Cette balise contient une section du contenu de votre site Web. On retrouve souvent cette balise à l'intérieur de la balise ${ inline('<main>') } et elle 
                        contiendra généralement une balise ${ inline('<h?>') } au début de son contenu. Si vous inspectez ce site Web, vous remarquerez que chaque titre, suivi d'un 
                        paragraphe d'explication se retrouve dans une balise ${ inline('<section>') }.
                    </p>
                </section>

                <section>
                    <h2>Autres balises sémantiques</h2>
                    <p>
                        Il existe de nombreuses autres balises sémantiques, comme ${ inline('<article>') }, ${ inline('<details>') } ou ${ inline('<summary>') }. Je ne vous les 
                        présenterai pas dans cette page puisqu'elles sont un peu moins souvent utilisé. Je vous suggère donc fortement d'aller lire la page Web sur les balises 
                        sémantiques sur <a href="https://www.w3schools.com/html/html5_semantic_elements.asp">w3school</a> pour avoir une idée des autres balises sémantiques qui existe 
                        ainsi que leur utilité.
                    </p>
                </section>
            </div>`
    }
})();