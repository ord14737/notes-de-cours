let code1 = 
`# Pour cloner un projet Git
git clone <adresse-du-projet-git>

# Pour ajouter les modifications à notre prochain commit
git add --all

# Commit des modifications
git commit -m "Commentaire décrivant le commit"

# Push des modifications sur le serveur Git
git push origin master`;

export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Introduction à Git</h1>
                
                <section>
                    <h2>Système de versioning</h2>
                    <p>
                        Un système de versioning est un système permettant de gérer les différentes versions du code d'une application. En général, ce système garde les données du code source
                        dans le cloud ou sur un serveur privé. En gros, nous l'utilisons comme un endroit pour sauvegarder notre code source, un peu comme si nous le sauvegardions dans 
                        OneDrive ou Google Drive, mais avec le bénéfice de garder l'information des différentes versions. Cela nous évite d'avoir des répertoires de fichiers comme celui-ci:
                    </p>
                    <div class="schema">
                        <ul>
                            <li>
                                <i class="material-icons md-18">folder</i>
                                <span>projet-important</span>
                            </li>
                            <li>
                                <i class="material-icons md-18">folder</i>
                                <span>projet-important-1</span>
                            </li>
                            <li>
                                <i class="material-icons md-18">folder</i>
                                <span>projet-important-2</span>
                            </li>
                            <li>
                                <i class="material-icons md-18">folder</i>
                                <span>projet-important-final</span>
                            </li>
                            <li>
                                <i class="material-icons md-18">folder</i>
                                <span>projet-important-vraiment-final</span>
                            </li>
                            <li>
                                <i class="material-icons md-18">folder</i>
                                <span>projet-important-le-plus-final-du-final</span>
                            </li>
                        </ul>
                    </div>
                    <p>
                        Il est très utile de garder plusieurs version d'un code source. En effet, il arrive fréquement d'écraser du code en pensant qu'il nous servait plus pour finalement se
                        rendre compte qu'on en a encore besoin. Parfois, quelques Ctrl-Z (Undo) peuvent régler le problème, mais si le changement est plus ancien, cela peut causer des 
                        problèmes. Un autre cas typique est que certains logiciels supportes plusieurs version fonctionnelles d'un logiciel en même temps. Par exemple, dans plusieurs cas, des
                        logiciels ont une version stable et une version ayant les dernières fonctionnalitées, mais contenant potentiellement des bogues.
                    </p>
                    <p>
                        Un autre avantage des systèmes de versioning est la possibilité de coopération. Sur le marché du travail, la coopération pour le développement d'un projet est 
                        nécessaire, surtout si le projet tend à grossir. Pour résoudre les conflits que cela peut occasionner, les gestionnaires de version permette entres autres aux
                        développeurs de travailler sur des versions parallèles du même logiciel qui pourront être combinées quand les programmeurs seront prêt. Vous n'avez probablement pas
                        encore eu besoin de travailler en équipe sur des projets plus gros jusqu'à présent, mais sachez que sans système de versioning, cela peut devenir un vrai casse-tête.
                    </p>
                    <img src="./img/branches.png" alt="Schémas de version parallèle du code">
                </section>

                <section>
                    <h2>Versioning avec Git</h2>
                    <p>
                        Git est présentement le système de versioning le plus utilisé par les développeurs. Bien que très complexe, il bénificie de plusieurs outils pour simplifier son 
                        utilisation. Initialement, Git a été développé par Linus Torvalds, le créateur de Linux, pour pouvoir gérer les différentes versions de linux facilement. Une lecture 
                        de <a href="https://en.wikipedia.org/wiki/Git" target="_blank">l'article Wikipedia en anglais</a> est une lecture intéressante et amusante si vous en avez le temps.
                    </p>
                    <p>
                        L'installation d'un serveur Git peut être assez complexe. De plus, même si cela serait très intéressant, nous n'avons pas de serveurs Git privé au collège. Nous devons
                        donc nous tourner vers d'autres solutions pour l'utiliser dans nos cours. Heureusement, aujourd'hui, il existe de nombreuses interfaces Web nous permettant de mettre 
                        notre code dans des serveurs Git préinstallés dans le cloud. Le plus connu est probablement GitHub. Cette plate-forme en ligne était initialement faite pour permettre 
                        aux développeur qui faisait du code open source de gérer les versions de leur code sur le cloud, tout en favorisant le partage de ce code dans la communauté. Cette 
                        plate-forme appartient aujourd'hui à Microsoft, mais reste gratuite pour les projets de communauté publique. Cette plate-forme ne permet donc pas de faire des projets 
                        privée gratuitement. Cela nous limite donc, puisque dans un contexte scolaire, un répertoire Git publique invite seulement le plagiat, ce que nous voulons éviter.
                    </p>
                    <p>
                        Nous avons toutefois une solution à tous nos problèmes: <a href="https://gitlab.com/" target="_blank">GitLab</a>. GitLab est une plate-forme en ligne similaire à GitHub, 
                        sauf qu'elle nous autorise à faire des projets privées. Je vous recommande fortement de vous faire un compte sur GitLab à l'aide de votre adresse courriel du collège.
                        Vous pourrez facilement créer des projets, les maintenir et collaborer dans les travaux d'équipe.
                    </p>
                </section>

                <section>
                    <h2>Opérations typiques de Git</h2>
                    <p>
                        En général, nous utilisons Git à partir d'une plate-forme Web de la façon suivante:
                    </p>
                    <ol>
                        <li><span>Créer un projet sur la plate-forme Web</span></li>
                        <li><span>Cloner (Clone) le projet sur un ordinateur de développement (télécharger le projet)</span></li>
                        <li><span>Écrire ou modifier du code sur l'ordinateur de développement</span></li>
                        <li><span>Commettre (Commit) les modifications du projet sur l'ordinateur de développement (créer une nouvelle version)</span></li>
                        <li><span>Publier (Push) les modifications du projet sur le serveur Git (téléverser les nouvelles versions du projet)</span></li>
                        <li><span>Recommencer à partir de l'étape 3</span></li>
                    </ol>
                    <img src="./img/git-operations.png" alt="Schémas des opérations typiques de Git">
                    <p>
                        Il existe plusieurs façon de faire ces opérations. Une des façon est de le faire en ligne de commande. Il existe essentiellement 4 commandes qui vont nous permettre
                        de faire les opérations Git ci-dessus.
                    </p>
                    ${ addCode(code1, 'Bash') }
                    <p>
                        Si vous ne voulez pas utiliser les commandes, il est aussi possible d'utiliser un interface graphique. Vous pouvez trouver une liste de différents clients Git avec 
                        interface graphique sur <a href="https://git-scm.com/download/gui/windows" target="_blank">le site Web officiel de Git</a>. Je vous recommande fortement d'utiliser
                        le client Git venant avec votre éditeur de texte (Visual Studio Code, Atom, Sublime Text, etc). Autrement, si vous préférez, vous pouvez aussi télécharger GitHub 
                        Desktop qui est un bon client Git qui fonctionne même avec GitLab.
                    </p>
                </section>

                <section>
                    <h2>Alternatives</h2>
                    <p>
                        Il existe quelques alternatives à Git. Toutefois, Git est aujourd'hui le gestionnaire de version le plus utilisé. De plus, il existe beaucoup plus d'outil pour nous 
                        aider dans l'utilisation de Git que dans celle de ses alternatives. Je vous recommande donc fortement de l'utiliser pour vous habituer. Voici tout de même quelques 
                        alternatives qui sont encore parfois utilisé dans l'industrie:
                    </p>
                    <dl>
                        <dt><a href="https://subversion.apache.org/" target="_blank">Apache Subversion (SVN)</a></dt>
                        <dd>
                            Système de versioning gratuit développé par la fondation Apache. Il possède de nombreux logiciels pour faciliter sa gestion et est encore utilisé à plusieurs 
                            endroits.
                        </dd>

                        <dt><a href="https://www.mercurial-scm.org/" target="_blank">Mercurial</a></dt>
                        <dd>
                            Système de versioning gratuit et open source. Il possède de très bonne performance pour la gestion des gros fichiers, ce qui le rends pratique pour les artistes 
                            numériques et designeurs.
                        </dd>
                    </dl>
                </section>
            </div>`
    }
})();
