export default (function() {
let code1Html = 
`<a href="http://un-site-web-qui-n-existe-pas.com/beau-domaine">
    Un lien pas encore visité 
</a>`;

let code1HtmlReal = 
`<a href="http://un-site-web-qui-n-existe-pas.com/beau-domaine" target="_parent"> 
    Un lien pas encore visité 
</a>`;

let code1Css = 
`a:link {
    color: #F00;
}`;

let code2Html = 
`<a href="http://google.com">
    Un lien déjà visité
</a>`;

let code2HtmlReal = 
`<a href="http://www.google.com" target="_parent">
    Un lien déjà visité
</a>`;

let code2Css = 
`a:visited {
    color: #F0F;
}`;

let code3Html = 
`<input type="button" value="Survole moi!" class="hoverable">
<ul class="hoverable">
    <li>Élément 1</li>
    <li>Élément 2</li>
    <li>Élément 3</li>
    <li>Élément 4</li>
</ul>`;

let code3Css = 
`input.hoverable:hover {
    background-color: #FF0;
}

ul.hoverable li {
    border: 1px solid rgba(0, 0, 0, 0);
}

ul.hoverable li:hover {
    border-color: #AAA;
}`;

let code4Html = 
`<input type="button" value="Clique moi!" class="clickable">

<a href="#" class="clickable">Un lien</a>

<p class="clickable">Un paragraphe cliquable</p>`;

let code4Css = 
`input.clickable:active, a.clickable:active, p.clickable:active {
    background-color: #F00;
}`;

let code5Html = 
`<input type="text">

<input type="button" value="Clique moi!">

<a href="#">Un lien</a>`;

let code5Css = 
`input:focus, textarea:focus, select:focus, a:focus {
    border: 3px double #00F;
}`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Sélecteurs CSS de pseudo-classe</h1>

                <section>
                    <h2>Pseudo-classes</h2>
                    <p>
                        Les pseudo-classes sont des sélecteurs CSS permettant de spécifier l'état spécifique dans lequel doit être un élément HTML pour pouvoir être sélectionné par ce 
                        sélecteur. Dans ce document, nous verrons principalement les pseudo-classes ${ inline(':link') }, ${ inline(':visited') }, ${ inline(':hover') }, 
                        ${ inline(':active') } et ${ inline(':focus') }.
                    </p>
                    <p>
                        Faites attention à l'ordre dans lequel vous mettez les pseudo-classes ci-dessus dans votre fichier CSS. Bien que pour la majorité des pseudo-classes il n'y ait 
                        pas de problèmes, les pseudo-classes ${ inline(':link') }, ${ inline(':visited') }, ${ inline(':hover') } et ${ inline(':active') } devrait toujours se retrouver 
                        dans cet ordre si vous les utilisez tous. Cela vous évitera d'avoir des conflits de règles.
                    </p>
                </section>

                <section>
                    <h2>Liens non visités</h2>
                    <p>
                        On utilise la pseudo-classe ${ inline(':link') } pour styler les liens non visités. Cette pseudo-classe s'applique donc uniquement aux balises ${ inline('<a>') }.
                    </p>
                    ${ addCodeExample([code1Html, code1HtmlReal], code1Css) }
                </section>

                <section>
                    <h2>Liens visités</h2>
                    <p>
                        Si vous désirez modifier le style des liens qui ont été visités, vous utiliserez le sélecteur de pseudo-classes ${ inline(':visited') }. Comme pour 
                        ${ inline(':link') }, cette pseudo-classe s'applique uniquement aux balises ${ inline('<a>') }.
                    </p>
                    ${ addCodeExample([code2Html, code2HtmlReal], code2Css) }
                </section>

                <section>
                    <h2>Survol du curseur</h2>
                    <p>
                        La pseudo-classe ${ inline(':hover') } nous permet de mettre un style sur des éléments quand nous les survolont avec la souris. Nous utiliserons souvent ce 
                        sélecteur sur les boutons ou pour indiquer que l'on peut cliquer sur un élément.
                    </p>
                    ${ addCodeExample(code3Html, code3Css) }
                </section>

                <section>
                    <h2>Clic de souris</h2>
                    <p>
                        Si vous voulez mettre un style lorsque vous êtes en train de cliquer sur un élément, c'est la pseudo-classe ${ inline(':active') } que nous utiliserons. Cette
                        pseudo-classe est très utile pour des éléments cliquables comme les boutons ou les liens.
                    </p>
                    ${ addCodeExample(code4Html, code4Css) }
                </section>

                <section>
                    <h2>Réception du focus</h2>
                    <p>
                        Le sélecteur ${ inline(':focus') } nous permet de mettre des règles CSS sur des éléments lorsqu'ils ont le focus. Nous pouvons par conséquent uniquement utiliser 
                        cette pseudo-classe avec les éléments qui peuvent recevoir le focus (focusable).
                    </p>
                    ${ addCodeExample(code5Html, code5Css) }
                </section>

                <section>
                    <h2>Autre pseudo-classes</h2>
                    <p>
                        Il existe de nombreuses pseudo-classes en CSS. Les 5 sélecteurs ci-dessus sont probablement les plus utilisés, mais avec quelques recherches, vous en trouverez
                        beaucoup d'autres. Je vous suggère fortement d'aller jeter un coup d'oeil à la page des pseudo-classes sur le site de 
                        <a href="https://developer.mozilla.org/fr/docs/Web/CSS/Pseudo-classes" target="_blank">développement de Mozilla</a> pour avoir une idée des autres pseudo-classes 
                        qui sont présentement disponibles.
                    </p>
                </section>
            </div>`
    }
})();