let code = [];

code.push({});
code[0].bash = 
`# Pour indiquer à votre ordinateur que le dossier du projet est un répertoire Git
git init

# Pour indiquer à votre ordinateur sur quel serveur mettre le projet Git
# Changer "votre_nom_ici" par votre nom d'utilisateur au 2 endroits
# Changer "nom-projet" par le nom de votre projet
# Vous pouvez trouver cette commande sur la page principale de votre projet
git remote add origin https://votre-nom-ici@gitlab.com/votre-nom-ici/nom-projet.git

# Ajoutes les fichiers et modifications au prochain commit
git add .

# Crée un commit avec le message "Commit initial"
git commit -m "Commit initial"

# Push des modifications sur le serveur Git et sauvegarde du serveur en mémoire
git push -u origin master`;

code.push({});
code[1].bash = 
`# Pour indiquer à votre ordinateur de télécharger le répertoire Git sur le serveur
# Changer "votre_nom_ici" par votre nom d'utilisateur
# Changer "nom-du-partenaire" par le nom d'utilisateur de votre partenaire
# Changer "nom-projet" par le nom de votre projet
# Vous pouvez aussi trouver le lien du clone sur la page principal du projet
git clone https://votre-nom-ici@gitlab.com/nom-du-partenaire/nom-projet.git`;

code.push({});
code[2].bash = 
`# (Non obligatoire) Affiche les modifications
git status

# Ajoutes les fichiers et modifications au prochain commit
git add .

# Crée un commit avec le message que vous voulez mettre
# Toujours ajouter un message à un commit
git commit -m "Message à mettre ici"

# Push des modifications sur le serveur Git
git push origin master`;

code.push({});
code[3].bash = 
`# Va chercher la dernière version du code sur le serveur à la fusionne avec votre code
git pull origin master`;

code.push({});
code[4].bash = 
`# (Non obligatoire) Affiche les modifications
git status

# Ajoutes les fichiers et modifications au prochain commit
git add .

# Crée un commit avec le message que vous voulez mettre
git commit -m "Update from master, Fix merge conflict"

# Push des modifications sur le serveur Git
git push origin master`;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Travailler en équipe avec Git</h1>

            <section>
                <h2>Introduction</h2>
                <p>
                    Git est présentement l'outil le plus utilisé pour gérer les versions du code source et pour travailler en équipe sur un projet. Il est donc important d'être à l'aise
                    avec cet outil si nous voulons travailler en programmation. Bien que l'apprentissage de Git ne soit pas facile pour tout le monde, vous verrez à force de l'utiliser
                    qu'il est très pratique.
                </p>
                <p>
                    Cette page suppose que vous avez déjà fait les étapes suivantes:
                </p>
                <ol>
                    <li><span> Installer <a href="https://git-scm.com/downloads" target="_blank">Git</a> sur votre ordinateur </span></li>
                    <li><span> Créer un compte sur <a href="https://gitlab.com/users/sign_up" target="_blank">GitLab</a> </span></li>
                </ol>
            </section>

            <section>
                <h2>Créer un projet</h2>
                <p>Voici les étapes à compléter pour créer un projet: </p>
                <ol>
                    <li><span> Créer le projet sur GitLab </span></li>
                    <li><span> Créer un dossier dans lequel vous voulez mettre votre projet sur votre ordinateur </span></li>
                    <li><span> Ajouter les fichiers de bases et configurer votre projet à son état initial </span></li>
                    <li><span> Ouvrir un terminal (CMD, Powershell, Git Bash, Bash, etc.) dans le dossier de votre projet </span></li>
                    <li><span> 
                        Exécuter les commandes suivantes dans la console: 
                        <styled-code lang="Bash" v-bind:code="code[0].bash"></styled-code>
                    </span></li>
                </ol>
                
            </section>

            <section>
                <h2>Rejoindre un projet</h2>
                <p>Voici les étapes pour rejoindre un projet existant: </p>
                <ol>
                    <li><span> Demander à votre partenaire de vous ajouter comme membre du projet sur GitLab </span></li>
                    <li><span> Naviguer sur votre ordinateur jusqu'au dossier oû vous voulez mettre votre projet </span></li>
                    <li><span> Ouvrir un terminal (CMD, Powershell, Git Bash, Bash, etc.) dans ce dossier </span></li>
                    <li><span> 
                        Exécuter les commandes suivantes dans la console: 
                        <styled-code lang="Bash" v-bind:code="code[1].bash"></styled-code>
                    </span></li>
                </ol>
            </section>

            <section>
                <h2>Pousser des modifications sur le serveur</h2>
                <p> Voici les étapes pour envoyer vos modifications sur le serveur: </p>
                <ol>
                    <li><span> Ouvrir un terminal (CMD, Powershell, Git Bash, Bash, etc.) dans le dossier de votre projet </span></li>
                    <li><span> 
                        Exécuter les commandes suivantes dans la console: 
                        <styled-code lang="Bash" v-bind:code="code[2].bash"></styled-code>
                    </span></li>
                </ol>
            </section>

            <section>
                <h2>Résoudre des conflits</h2>
                <p> Voici les étapes mettre votre code à jour avec le serveur et régler les conflits: </p>
                <ol>
                    <li><span> Ouvrir un terminal (CMD, Powershell, Git Bash, Bash, etc.) dans le dossier de votre projet </span></li>
                    <li><span> 
                        Exécuter les commandes suivantes dans la console: 
                        <styled-code lang="Bash" v-bind:code="code[3].bash"></styled-code>
                    </span></li>
                    <li><span> 
                        S'il y a un conflit: 
                        <ol>
                            <li><span> Ouvrir tous les fichiers causant des conflits indiqué dans la console </span></li>
                            <li><span> 
                                Partout dans le code où il y a des ${ inline('<<<<<<<') } et ${ inline('>>>>>>>') }, choisir la version du code que vous voulez pour corriger le conflit
                            </span></li>
                            <li><span> Sauvegarder vos fichiers </span></li>
                        </ol>
                    </span></li>
                    <li><span> 
                        Exécuter les commandes suivantes dans la console: 
                        <styled-code lang="Bash" v-bind:code="code[4].bash"></styled-code>
                    </span></li>
                </ol>
            </section>

            <section>
                <h2>Capsule vidéo</h2>
                <div class="video">
                    <div>
                        <iframe
                            src="https://www.youtube.com/embed/qGaXiWKQ2j4" 
                            frameborder="0" 
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </section>
        </div>`
};