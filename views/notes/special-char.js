export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Caractères spéciaux</h1>

                <section>
                    <h2>Séquences d'échappement</h2>
                    <p>
                        Certains caractères en HTML ne peuvent pas être affiché facilement. On peut catégoriser ces caractères en 2 groupes différents:
                    </p>
                    <ul>
                        <li><span>Les caractères réservé par le HTML</span></li>
                        <li><span>Les caractères ne se trouvant pas sur un clavier</span></li>
                    </ul>
                    <p>
                        Un peu comme le langage Java, le HTML possède donc des séquences d'échappement pour corriger ce problème. Toutefois, contrairement à la plupart des langages de programmation, nous
                        n'utiliserons pas le ${ inline('\\') } pour échapper nos caractères. Nous utiliserons le ${ inline('&') }.
                    </p>
                </section>
            
                <section>
                    <h2>Caractères réservés</h2>
                    <p>
                        Le langage HTML possède 5 caractères réservés. Pour chacun d'entre eux, nous pouvons utiliser les séquences d'échappements suivantes:
                    </p>
                    <table>
                        <thead>
                            <tr>
                                <th>Caractère</th>
                                <th>Échappement avec numéro</th>
                                <th>Échappement avec texte</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td>&quot;</td><td>&amp;#34;</td><td>&amp;quot;</td></tr>
                            <tr><td>&apos;</td><td>&amp;#39;</td><td>&amp;apos;</td></tr>
                            <tr><td>&amp;</td><td>&amp;#38;</td><td>&amp;amp;</td></tr>
                            <tr><td>&lt;</td><td>&amp;#60;</td><td>&amp;lt;</td></tr>
                            <tr><td>&gt;</td><td>&amp;#62;</td><td>&amp;gt;</td></tr>
                        </tbody>
                    </table>
                </section>
            
                <section>
                    <h2>Caractères n'étant pas sur un clavier</h2>
                    <p>
                        Les caractères n'étant pas sur un clavier sont nombreux. Il y a en effet une table Unicode complète de caractère qui, pour la plupart, ne sont pas accessible sur votre 
                        clavier. Pour ces caractères, il est possible de trouver leur séquence d'échappement facilement sur le Web. Ils commencent généralement par les symboles 
                        ${ inline('&#') }, mais certains d'entre eux sont utilisé assez fréquement pour avoir une version textuelle commençant uniquement par ${ inline('&') }. Voici tout de 
                        même quelques caractères intéressant à connaître:
                    </p>
                    <table>
                        <thead>
                            <tr>
                                <th>Caractère</th>
                                <th>Échappement</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td>&nbsp;</td><td>&amp;nbsp;</td><td>Un espace ne pouvant pas séparer les mots sur plusieurs lignes</td></tr>
                            <tr><td>&#8209;</td><td>&amp;#8209;</td><td>Un tiret ne pouvant pas séparer les mots sur plusieurs lignes</td></tr>
                            <tr><td>&copy;</td><td>&amp;copy;</td><td>Le symbole de copyright</td></tr>
                            <tr><td>&cent;</td><td>&amp;cent;</td><td>Le symbole des cents</td></tr>
                        </tbody>
                    </table>
                </section>
            </div>`
    }
})();