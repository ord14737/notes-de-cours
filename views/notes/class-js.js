let code = [];

code.push({});
code[0].js = 
``;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Classes</h1>

            <section>
                <h2>Introduction</h2>
                <p>
                    Si les classes des langages orientés objets comme Java ou C# vous manque, sachez que depuis quelques années, Javascript les supporte aussi. Ce qu'il faut savoir 
                    toutefois, c'est que ces classes sont en fait des fonctions spéciales. On les utilise comme des classes, mais en arrière-plan, le navigateur utilise des fonctions.
                    De plus, puisque cet ajout est relativement récent, plusieurs fonctionnalités sont encore expérimentales, dont la déclaration d'attribut privée.
                </p>
                <p>
                    Pour faire un résumé rapide des classes, on peut dire que c'est un patron / modèle (un blueprint) que l'on peut utiliser pour créer des objets. Essentiellement, une
                    classe nous permet de créer rapidement des objets ayant des propriétés et des fonctions prédéfinies.
                </p>
            </section>

            <section>
                <h2>Ressources externes</h2>
                <p>
                    Voici quelques ressources externes qui vous permettront d'apprendre comment utiliser les classes en Javascript.
                </p>
                <ol>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Classes" target="_blank">
                            MDN
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://www.w3schools.com/js/js_classes.asp" target="_blank">
                            w3school
                        </a>
                    </span></li>
                </ol>
            </section>
        </div>`
};