export default (function() {
let code1Css = 
`ul {
    list-style-type: square;
}`;

let code1Html = 
`<ul>
    <li>Bonjour</li>
    <li>Salut</li>
    <li>Allo</li>
</ul>`;

let code2Css = 
`ul {
    list-style-type: circle;
    list-style-image: url('../img/pokeball.png');
}`;

let code2Html = 
`<ul>
    <li>Bulbasaur</li>
    <li>Ivysaur</li>
    <li>Venosaur</li>
</ul>`;

let code3Css = 
`ul {
    list-style-position: inside;
    padding: 0;
}`;

let code3Html = 
`<ul>
    <li>Sit quam assumenda voluptatibus sed porro nam illum sunt nihil ipsam officia blanditiis aperiam quia eum, pariatur esse doloremque obcaecati ipsa omnis.</li>
    <li>Vero ullam similique culpa unde iste, soluta magni magnam aut tempora saepe corrupti repudiandae esse porro, eligendi enim assumenda, eum temporibus quo.</li>
    <li>Quod, cupiditate asperiores amet consequuntur delectus fuga voluptatum assumenda eius cum quae accusantium velit minus modi corrupti quos vel recusandae nam blanditiis!</li>
</ul>`;

let code4Css = 
`table, td, th {
    border: 1px solid #000;
}

table{
    border-collapse: collapse;
}`;

let code4Html = 
`<table>
    <tr> 
        <td> 1 x 1 </td> <td> 1 x 2 </td> 
    </tr>
    <tr> 
        <td> 2 x 1 </td> <td> 2 x 2 </td> 
    </tr>
</table>`;

let code5Css = 
`table, td, th {
    border: 1px solid #000;
}

table{
    border-collapse: seperate;
    border-spacing: 10px 5px;
}`;

let code6Css = 
`table, td, th {
    border: 1px solid #000;
}

table {
    caption-side: bottom;
}`;

let code5_6Html = 
`<table>
    <caption>Tableau 1.1 - Exemple</caption>
    <tr> 
        <td> 1 x 1 </td> <td> 1 x 2 </td> <td> 1 x 3 </td> <td> 1 x 4 </td> 
    </tr>
</table>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Liste et tableaux</h1>

                <section>
                    <h2>Listes</h2>
                    <p>
                        Les listes sont des éléments important du HTML. Ces éléments ont aussi plusieurs propriétés CSS propres à eux-mêmes. Voici donc une liste des propriétés CSS que 
                        vous pouvez utiliser avec les listes:
                    </p>
                    <dl>
                        <dt>${ inline('list-style-type') }</dt>
                        <dd>
                            Permet de définir le type de marqueur utilisé dans la liste. Les listes ${ inline('<ul>') } peuvent utiliser des des valeurs comme ${ inline('disc') },
                            ${ inline('circle') } ou encore ${ inline('square') }. Les listes ${ inline('<ol>') } quant à elle peuvent utiliser des valeurs comme ${ inline('decimal') },
                            ${ inline('lower-alpha') } ou ${ inline('upper-roman') }. Si vous voulez tout simplement cacher le marqueur, vous pouvez utiliser la valeur 
                            ${ inline('none') }. Vous pouvez trouver l'ensemble des valeurs disponible sur 
                            <a href="https://www.w3schools.com/cssref/pr_list-style-type.asp" target="_blank">w3school</a>.
                            ${ addCodeExample(code1Html, code1Css) }
                        </dd>

                        <dt>${ inline('list-style-image') }</dt>
                        <dd>
                            Permet de définir une image qui sera utilisé à la place du marqueur dans la liste. Vous devriez toujours spécifier un marqueur de base au cas où la navigateur 
                            n'est pas en mesure de charger l'image. Pour spécifier l'image, vous pouvez utiliser la fonction CSS ${ inline('url(\'chemin\')') } en lui passant le chemin 
                            de l'image.
                            ${ addCodeExample(code2Html, code2Css) }
                        </dd>

                        <dt>${ inline('list-style-position') }</dt>
                        <dd>
                            Permet de spécifier où les marqueurs seront placé lors de l'affichage. Les 2 valeurs acceptées sont ${ inline('outside') } (valeur par défaut) ou 
                            ${ inline('inside') }. La valeur ${ inline('inside') } rend le marqueur plus facile à déplacer, mais complique l'alignement du texte si celui-ci s'étire sur
                            plusieurs lignes.
                            ${ addCodeExample(code3Html, code3Css) }
                        </dd>

                        <dt>${ inline('list-style') }</dt>
                        <dd>
                            Une propriété raccourci des 3 propriétés précédantes en une seule. Vous pouvez trouver plus d'information sur comment utiliser cette propriété sur 
                            <a href="https://www.w3schools.com/cssref/pr_list-style.asp" target="_blank">w3school</a>.
                        </dd>
                    </dl>
                </section>

                <section>
                    <h2>Tableaux</h2>
                    <p>
                        Les tableaux possèdes eux-aussi quelques propriétés CSS qui sont unique à eux-mêmes. Voici une liste des propriétés CSS que vous pouvez utiliser avec les 
                        tableaux:
                    </p>
                    <dl>
                        <dt>${ inline('border-collapse') }</dt>
                        <dd>
                            Permet d'indiquer si l'on veut fusionner les bordures des cellules voisines du tableau. Les 2 valeurs possibles sont ${ inline('collapse') } ou
                            ${ inline('separate') }.
                            ${ addCodeExample(code4Html, code4Css) }
                        </dd>

                        <dt>${ inline('border-spacing') }</dt>
                        <dd>
                            Permet de spécifier l'espace entre les cellules. Cette propriété fonctionne uniquement si la propriété ${ inline('border-collapse') } a la valeur
                            ${ inline('separate') }. Si vous spécifiez 1 valeur, celle-ci sera utilisé pour tous les côtés des cellules. Si vous spécifiez 2 valeurs, la première 
                            indiquera l'espacement horizontal et la deuxième donnera l'espacement vertical.
                            ${ addCodeExample(code5_6Html, code5Css) }
                        </dd>

                        <dt>${ inline('caption-side') }</dt>
                        <dd>
                            Permet d'indiquer où mettre le ${ inline('<caption>') } s'il y en a un. Par défaut, sa valeur est ${ inline('top') }, mais on peut aussi lui mettre la valeur
                            ${ inline('bottom') }
                            ${ addCodeExample(code5_6Html, code6Css) }
                        </dd>
                    </dl>
                </section>
            </div>`
    }
})();