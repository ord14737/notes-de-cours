export default (function() {
let code1 = 
`<input type="type_de_données" value="contenu_par_défaut">`;

let code2 = 
`<!-- Input de type texte -->
Votre nom: 
<input type="text">`;

let code3 = 
`<!-- 
Input de type texte avec les attributs
 - longueur maximale de caractères
 - placeholder à afficher lorsque l'input est vide
-->
Votre code postal: 
<input type="text" maxlength="6" placeholder="Code postal">`;

let code4 = 
`<!-- Input de type nombre -->
Votre âge: 
<input type="number">`;

let code5 = 
`<!-- 
Input de type nombre avec les attributs
 - valeur minimum
 - valeur maximum
 - la taille d'une marche lors de l'incrémentation ou décrémentation
 - placeholder à afficher lorsque l'input est vide
-->
Votre âge: 
<input type="number" min="0" max="99" step="3" placeholder="Âge">`;

let code6 = 
`<!-- Input de type range -->
Niveau de satisfaction: 
<input type="range">`;

let code7 = 
`<!-- 
Input de type range avec les attributs
 - valeur minimum
 - valeur maximum
 - la taille d'une marche lors de l'incrémentation ou décrémentation
-->
Niveau de satisfaction: 
<input type="range" min="0" max="10" step="2" placeholder="Âge">`;

let code8 = 
`<!-- Input de type checkbox -->
Échalote française: 
<input type="checkbox">
Oeuf: 
<input type="checkbox">`;

let code9 = 
`<!-- 
Input de type checkbox avec les attributs
 - valeur indiquant que la case est cocher par défaut
-->
Échalote française: 
<input type="checkbox" checked>
Oeuf: 
<input type="checkbox" checked>`;

let code10 = 
`<!-- Input de type radiobutton -->
Bouillon: 
poulet <input type="radio">
boeuf <input type="radio">
légume <input type="radio">`

let code11 = 
`<!-- 
Input de type radiobutton avec les attributs
 - valeur indiquant que la case est cocher par défaut
-->
Bouillon: 
poulet <input type="radio" checked name="bouillon">
boeuf <input type="radio" name="bouillon">
légume <input type="radio" name="bouillon">`

let code12 = 
`<!-- Autres types d'inputs -->
<p> Date: <input type="date"> <p>
<p> Temps: <input type="time"> <p>
<p> Couleur: <input type="color"> <p>
<p> Mot de passe: <input type="password"> <p>
<p> Recherche: <input type="search"> <p>
<p> Bouton de soumission: <input type="submit"> <p>
<p> Bouton générique: <input type="button" value="CLIQUE MOI!"> <p>`

    return {
        template: `
            <div class="note" v-once>
                <h1>Balises d'entrée</h1>

                <section>
                    <h2>La balise ${ inline('<input>') }</h2>
                    <p>
                        Bien qu'il existe de nombreuses balises pour demander des informations à un utilisateur de notre site Web, nous utiliserons le plus souvent la balise 
                        ${ inline('<input>') }. Cette balise offre une multitude d'options pour demander des informations de types différents à l'utilisateur. Il est important de noter 
                        que cette balise n'a pas de balise fermante et que par défaut, elle utilise l'affichage en ligne. Cette balise s'utilise de la façon suivante:
                    </p>
                    ${ addCode(code1, 'HTML') }
                    <p>
                        Où la valeur de l'attribut ${ inline('type') } est le format ou le type de données que nous voulons demander à l'utilisateur et l'attribut ${ inline('value') }
                        est la valeur par défaut contenu dans l'input.
                    </p>
                    <p>
                        Ce document résumera l'utilisation des principaux types d'input que nous utiliserons dans une page Web. Vous pouvez aussi consulter la page de 
                        <a href="https://www.w3schools.com/tags/tag_input.asp" target="_blank">w3schools</a> pour d'autres information sur cette balise.
                    </p>
                </section>

                <section>
                    <h2>Texte</h2>
                    <p>
                        L'input de base est celui qui accepte le texte. C'est essentiellement une boîte de texte dans laquelle l'utilisateur peut entrer des données.
                    </p>
                    ${ addCodeExample(code2) }
                    <p>
                        Bien que cela semble assez simple, nous pouvons toutefois ajouter des attributs pour changer l'affichage ou l'utilisation de cette balise.
                    </p>
                    <dl>
                        <dt>${ inline('maxlength') }</dt>
                        <dd>Longueur maximale du texte que l'utilisateur peut entrer.</dd>

                        <dt>${ inline('placeholder') }</dt>
                        <dd>Texte fantôme à afficher dans l'input lorsqu'il est vide.</dd>
                    </dl>
                    ${ addCodeExample(code3) }
                </section>

                <section>
                    <h2>Nombre</h2>
                    <p>
                        L'input qui accepte des nombres. C'est essentiellement une boîte de texte qui n'accepte que des nombres et qui permet l'incrémentation et la décrémentation par 
                        des petits boutons dans l'input.
                    </p>
                    ${ addCodeExample(code4) }
                    <p>
                        Voici les attributs valides permettant de changer le fonctionnement de cet input.
                    </p>
                    <dl>
                        <dt>${ inline('min') }</dt>
                        <dd>La valeur minimale possible de l'input.</dd>

                        <dt>${ inline('max') }</dt>
                        <dd>La valeur maximale possible de l'input.</dd>

                        <dt>${ inline('step') }</dt>
                        <dd>Taille de l'incrémentation lorsque l'utilisateur clique sur les boutons.</dd>

                        <dt>${ inline('placeholder') }</dt>
                        <dd>Texte fantôme à afficher dans l'input lorsqu'il est vide.</dd>
                    </dl>
                    ${ addCodeExample(code5) }
                </section>

                <section>
                    <h2>Range</h2>
                    <p>
                        L'input qui accepte un intervalle. C'est essentiellement une barre sur laquelle nous pouvons sélectionner un nombre. C'est donc une autre façon de faire entrer
                        un nombre à notre utilisateur.
                    </p>
                    ${ addCodeExample(code6) }
                    <p>
                        Voici les attributs valides permettant de changer le fonctionnement de cet input.
                    </p>
                    <dl>
                        <dt>${ inline('min') }</dt>
                        <dd>La valeur minimale possible de l'input.</dd>

                        <dt>${ inline('max') }</dt>
                        <dd>La valeur maximale possible de l'input.</dd>

                        <dt>${ inline('step') }</dt>
                        <dd>Taille de l'incrémentation lorsque l'utilisateur déplace le curseur sur la ligne.</dd>
                    </dl>
                    ${ addCodeExample(code7) }
                </section>

                <section>
                    <h2>Checkbox</h2>
                    <p>
                        L'input qui est une case à cocher. Elle permet à l'utilisateur de sélectionner une option.
                    </p>
                    ${ addCodeExample(code8) }
                    <p>
                        Voici les attributs valides permettant de changer le fonctionnement de cet input.
                    </p>
                    <dl>
                        <dt>${ inline('checked') }</dt>
                        <dd>Indique si le checkbox doit être cocher par défaut.</dd>
                    </dl>
                    ${ addCodeExample(code9) }
                </section>

                <section>
                    <h2>Radiobutton</h2>
                    <p>
                        L'input qui donne plusieurs options à un utilisateur sous forme de case à cocher, mais qui n'accepte qu'une seule option à la fois.
                    </p>
                    ${ addCodeExample(code10) }
                    <p>
                        Voici les attributs valides permettant de changer le fonctionnement de cet input.
                    </p>
                    <dl>
                        <dt>${ inline('checked') }</dt>
                        <dd>Indique si le checkbox doit être cocher par défaut.</dd>

                        <dt>${ inline('name') }</dt>
                        <dd>Indique à quel groupe de case à cocher appartient cette case à cocher.</dd>
                    </dl>
                    ${ addCodeExample(code11) }
                </section>

                <section>
                    <h2>Autres inputs</h2>
                    <p>
                        Il existe plus de 20 sortes d'inputs différents. Je ne les passerai pas tous en revue, mais voici un exemple contenant plusieurs des autres inputs non vu jusqu'à 
                        présent. Pour en apprendre d'avantages sur ces types d'inputs, vous pouvez vous rendre sur w3schools.
                    </p>
                    ${ addCodeExample(code12) }
                </section>
            </div>`
    }
})();
