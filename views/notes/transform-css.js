let code = [];

code.push({});
code[0].css =
`.selecteur {
    transform: [fonction-transformation]
}`;

code.push({});
code[1].exHtml =
`none
<div class="conteneur"> <div class="box none"></div> </div>

translate(x, y)
<div class="conteneur"> <div class="box translate"></div> </div>

scale(x, y)
<div class="conteneur"> <div class="box scale"></div> </div>

skew(angleX, angleY)
<div class="conteneur"> <div class="box skew"></div> </div>

rotate(angle)
<div class="conteneur"> <div class="box rotate"></div> </div>

multiples transformations
<div class="conteneur"> <div class="box multiple"></div> </div>`;

code[1].css =
`.none {
    transform: none;
}

.translate {
    transform: translate(25px, 10px);
}

.scale {
    transform: scale(1, .5);
}

.skew {
    transform: skew(-30deg);
}

.rotate {
    transform: rotate(45deg);
}

.multiple {
    transform: scale(1, 0.5) rotate(30deg) translate(35px, 20px);
}`;

code[1].exCss = 
`.conteneur {
    border: 1px solid #CCC;
    height: 5em;
}

.box {
    background-color: #A00;
    width: 3em;
    height: 3em;
}` + 
code[1].css;

code.push({});
code[2].exHtml =
`<div class="container">
    <div class="box left">
        <div></div>
    </div>
    <div class="box right">
        <div></div>
    </div>
</div>`;

code[2].exCss =
`.container {
    position: relative;
    height: 15em;
}

.box {
    position: absolute;
    width: 5em;
    height: 5em;
    border: 2px dashed #A00;
    top: 5em;
}

.left {
    left: 10%;
}

.right {
    right: 10%;
}

.right > div {
    transform-origin: left top;
}

.box > div {
    height: 100%;
    background-color: #F44;
    transition: transform 1s ease;
}

.box:hover > div {
    transform: rotate(135deg)
}`;

code.push({});
code[3].html =
`<div class="container">
    <h1>Pingouin</h1>
</div>`;

code[3].css =
`.container {
    position: relative;
    height: 15em;
}

h1{
    /* Nécessaire pour les <h?> si on a pas le reset.css */
    margin: 0;

    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}`;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Transformation</h1>

            <section>
                <h2>Introduction</h2>
                <p>
                    Les transformations sont un moyen de changer la forme et la position des éléments HTML sans modifier le flux normal de la page Web. Elles sont utiles, surtout si 
                    utilisé conjointement avec les transitions ou les animations pour déplacer ou modifier des éléments de façon agréable à l'écran. Dans ce document, nous survolerons
                    les transformations de base que CSS nous permet de faire.
                </p>
                <p>
                    Les transformations utilisent des calculs matriciels pour générer les modifications. Par conséquant, ces transformations sont généralement calculé par le processeur 
                    graphique de votre ordinateur (GPU) pour de meilleurs performance.
                </p>
            </section>

            <section>
                <h2>Syntaxe</h2>
                <p>
                    La syntaxe de base d'une transformation est la suivante:
                </p>
                <styled-code v-bind:code="code[0].css" lang="CSS"></styled-code>
                <p>
                    Il existe plusieurs fonctions de transformations déjà intégré dans les navigateurs. Nous utiliserons majoritairement ces fonctions déjà établies, donc voici une liste
                    des fonctions que vous utiliserez le plus souvent.
                </p>
                <code-example v-bind:code="code[1]"></code-example>
                <dl>
                    <dt>${ inline('none') }</dt>
                    <dd>
                        Spécifie aucune transformation. Pratique lorsque vous voulez retirer des transformations déjà mise sur des éléments.
                    </dd>

                    <dt>${ inline('translate(x, y)') }</dt>
                    <dd>
                        Indique une translation en X et Y. Les unités de mesure accepté pour cette transformation sont les même que les unités de distance habituelle, comme le 
                        ${ inline('px') }, ${ inline('em') } ou autre.
                    </dd>

                    <dt>${ inline('scale(x, y)') }</dt>
                    <dd>
                        Indique une homothétie (agrandissement ou rapetissement) en X et/ou Y. Il n'y a pas d'unité de mesure pour cette fonction. La valeur 1 indique que l'on garde la 
                        même taille et la valeur 0 indique une taille de zéro, donc inexistante. Ainsi, si la valeur est entre 0 et 1, on a un rapetissement et si la valeur est plus 
                        grande que 1, on a un agrandissement.
                    </dd>

                    <dt>${ inline('rotate(angle)') }</dt>
                    <dd>
                        Indique une rotation de l'élément d'un certain angle. L'angle peut être défini en degrés (${ inline('deg') }), en radians (${ inline('rad') }) ou encore en tour 
                        (${ inline('turn') }).
                    </dd>

                    <dt>${ inline('skew(angle)') }</dt>
                    <dd>
                        Indique une distorsion oblique de l'élément d'un certain angle. L'angle spécifié doit utiliser les mêmes unités de mesure que dans la fonction de rotation.
                    </dd>
                </dl>
            </section>

            <section>
                <h2>Origine de la transformation</h2>
                <p>
                    Par défaut, les transformations se font à partir du centre de l'élément sur lequel nous appliquons la transformation. Il est toutefois possible d'appliquer la 
                    tranformation à partir d'un autre point dans l'élément à l'aide de de la propriété ${ inline('transform-origin') }. Vous pouvez en apprendre davantage en allant sur
                    <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/transform-origin" target="_blank">MDN</a>
                </p>
                <code-example v-bind:code="code[2]"></code-example>
            </section>

            <section>
                <h2>Centrer un élément absolue</h2>
                <p>
                    Un truc utile avec les transformations est de centrer les éléments en position absolue à l'intérieur de leur parent. Historiquement, si nous n'avions pas la largeur
                    de l'élément absolue, ceci nécessitait absolument du Javascript. Voici un exemple illustrant comment centrer vos éléments:
                </p>
                <code-example v-bind:code="code[3]"></code-example>
            </section>

            <section>
                <h2>Autres transformations et ressources</h2>
                <p>
                    Il existe de nombreuses autres fonctions de transformations. De plus, l'intégration du 3D dans une page Web peut maintenant se faire dans la majorité des navigateurs,
                    ce qui rajoute encore plus de fonctions de transformations. Bien que les transformations 2D de base sont généralement suffisante, si vous voulez vous essayer à 
                    utiliser les autres transformations, vous pouvez en apprendre davantage sur 
                    <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Transforms/Using_CSS_transforms" target="_blank">MDN</a>, 
                    <a href="https://css-tricks.com/things-watch-working-css-3d/" target="_blank">CSS-TRICKS</a> ou 
                    <a href="https://www.w3schools.com/css/css3_3dtransforms.asp" target="_blank">w3school</a>.
                </p>
            </section>
        </div>`
}