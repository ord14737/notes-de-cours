export default {
    template: `
        <div class="note" v-once>
            <h1>Fonctions</h1>

            <section>
                <h2>Ressources sur le Javascript</h2>
                <p>
                    Voici quelques ressources externes qui vous permettront d'apprendre comment utiliser les fonctions en Javascript.
                </p>
                <ol>
                    <li><span>
                        <a href="https://www.w3schools.com/js/js_functions.asp" target="_blank">
                            Utilisation des fonctions de base
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Fonctions" target="_blank">
                            Documentation complète sur les fonctions
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://codeburst.io/javascript-functions-understanding-the-basics-207dbf42ed99" target="_blank">
                            Résumé court sur les fonctions
                        </a>
                    </span></li>
                </ol>
            </section>

            <section>
                <h2>Capsule vidéo</h2>
                <div class="video">
                    <div>
                        <iframe
                            src="https://www.youtube.com/embed/ddEi7TV2Kkg" 
                            frameborder="0" 
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </section>
        </div>`
};