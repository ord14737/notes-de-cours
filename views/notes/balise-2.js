export default (function() {
let code1 =
`<table>
    <tr>
        <td> 1x1 </td>
        <td> 1x2 </td>
        <td> 1x3 </td>
        <td> 1x4 </td>
    </tr>
    <tr>
        <td> 2x1 </td>
        <td> 2x2 </td>
        <td> 2x3 </td>
        <td> 2x4 </td>
    </tr>
    <tr>
        <td> 3x1 </td>
        <td> 3x2 </td>
        <td> 3x3 </td>
        <td> 3x4 </td>
    </tr>
</table>`;

let code2 =
`<table>
    <thead>
        <tr>
            <th> Prix </th>
            <th> Quantité </th>
            <th> Taxe </th>
            <th> Sous-total </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td> 10 </td>
            <td> 2 </td>
            <td> 2.60 </td>
            <td> 22.60 </td>
        </tr>
        <tr>
            <td> 5 </td>
            <td> 3 </td>
            <td> 1.95 </td>
            <td> 16.95 </td>
        </tr>
        <tr>
            <td> 8 </td>
            <td> 2 </td>
            <td> 2.08 </td>
            <td> 18.08 </td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td> 57.63 </td>
        </tr>
    </tfoot>
</table>`;

let code3 =
`<table>
    <caption>Tic&#8209;Tac&#8209;Toe</caption>
    <tr>
        <td> X </td>
        <td>   </td>
        <td> O </td>
    </tr>
    <tr>
        <td> X </td>
        <td> X </td>
        <td>   </td>
    </tr>
    <tr>
        <td> X </td>
        <td> O </td>
        <td> O </td>
    </tr>
</table>`;

let code4 =
`<table>
    <tr>
        <td colspan="4"> Je prends toute la rangée </td>
    </tr>
    <tr>
        <td rowspan="3"> Je prends 3 colonnes </td>
        <td> 2x2 </td>
        <td> 2x3 </td>
        <td> 2x4 </td>
    </tr>
    <tr>
        <td> 3x2 </td>
        <td colspan="2" rowspan="2"> Je combine les fusions </td>
    </tr>
    <tr>
        <td> 3x2 </td>
    </tr>
</table>`;

let code4extension = `<style>table, td { border: 1px solid #000; }</style>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Tableau</h1>

                <section>
                    <h2>Tableaux simples</h2>
                    <p>
                        Les tableaux sont une façon d'afficher de l'information dans une page Web sous la forme... de tableau. Les tableaux servent principalement à afficher des données, 
                        comme des budgets, des rapports financiers, des définitions de termes ou encore des notes d'examens. On peut aussi l'utiliser tout simplement pour organiser des
                        données sous différentes catégories. En HTML, les tableaux sont à la base créé à l'aide de 3 balises différentes:
                    </p>
                    <dl>
                        <dt>${ inline('<table></table>') }</dt>
                        <dd>C'est la balise englobant le tableau. L'ensemble d'un tableau devrait se retrouver à l'intérieur de cette balise.</dd>

                        <dt>${ inline('<tr></tr>') }</dt>
                        <dd>Balise représentant une rangée.</dd>

                        <dt>${ inline('<td></td>') }</dt>
                        <dd>Balise représentant une cellule du tableau. Les cellules doivent se retrouver absolument dans une balise de rangée <span class=inline-code>&lt;tr&gt;</span></dd>
                    </dl>
                    <p>
                        Voici comment créer un simple tableau avec ces 3 balises:
                    </p>
                    ${ addCodeExample(code1) }
                    <p>
                        Le tableau est un peu difficile à remarquer. C'est parce que par défaut, les bordures du tableau ne sont pas affiché. Malheureusement, nous verrons uniquement comment
                        ajouter les bordures avec du CSS dans les prochaines semaines.
                    </p>
                </section>

                <section>
                    <h2>Entête, corps et pied de tableaux</h2>
                    <p>
                        Il est possible de diviser les tableaux en 3 sections: l'entête, le corps et le pied. Nous utiliserons 3 balises pour séparer ces sections. Ces balises seront mises
                        directement à l'intérieur de la balise ${ inline('<table>') }.
                    </p>
                    <dl>
                        <dt>${ inline('<thead></thead>') }</dt>
                        <dd>Balise englobant l'entête du tableau. On y mettra en général les différents titres des colonnes.</dd>

                        <dt>${ inline('<tbody></tbody>') }</dt>
                        <dd>Balise englobant le corps du tableau. On y mettra généralement les données du tableau.</dd>

                        <dt>${ inline('<tfoot></tfoot>') }</dt>
                        <dd>Balise englobant le pied du tableau. On y mettra généralement les résultats ou les totaux des données du tableau.</dd>
                    </dl>
                    <p>
                        Voici à quoi ressemble un tableau avec ses différentes sections:
                    </p>
                    ${ addCodeExample(code2) }
                    <p>
                        Il y a quelques petits détails à mentionner sur le code ci-dessus:
                    </p>
                    <ul>
                        <li><span>On doit mettre les ${ inline('<tr>') } à l'intérieur des balises de section</span></li>
                        <li><span>Il est possible de ne pas mettre de valeur dans une cellule</span></li>
                        <li><span>Dans un ${ inline('<thead>') }, on doit mettre des ${ inline('<th>') } au lieu de des ${ inline('<td>') }</span></li>
                        <li><span>Les balise ${ inline('<tbody>') } et ${ inline('<tfoot>') } ne servent pas vraiment pour le moment, mais vont être utile avec le CSS</span></li>
                    </ul>
                </section>

                <section>
                    <h2>Légende ou titre du tableau</h2>
                    <p>
                        Il est possible d'afficher un titre ou une légende à un tableau à l'aide de la balise ${ inline('<caption>') }. Si vous voulez utiliser cette fonctionnalité, 
                        vous devez obligatoirement mettre cette balise en premier à l'intérieur de la balise ${ inline('<table>') }.
                    </p>
                    ${ addCodeExample(code3) }
                </section>

                <section>
                    <h2>Fusionner des cellules</h2>
                    <p>
                        Il est possible de fusionner des cellules à l'aide des attributs ${ inline('colspan') } et ${ inline('rowspan') }. Les attributs sont un moyen de configurer 
                        certaines balise. Dans le cas de ces 2 attributs, ils servent à configurer les cellules, donc les balises ${ inline('<th>') } ou ${ inline('<td>') }. Ces 
                        attributs doivent indiquer de combien de cellules nous voulons faire la fusion à partir de la cellule spécifiée.
                    </p>
                    <p>
                        L'exemple suivant est démontre bien la puissance de la fusion des cellules. J'ai aussi affiché les bordures des cellules pour que vous puissiez mieux voir l'effet de
                        ${ inline('colspan') } et ${ inline('rowspan') }.
                    </p>
                    ${ addCodeExample(code4) }
                    <p>
                        Il est important de noter que la fusion se fait uniquement dans 2 directions: vers la droite (pour ${ inline('colspan') }) ou vers le bas (pour 
                        ${ inline('rowspan') }). Il est aussi important de noter que nous devons retirer les cellules qui sont fusionné à la cellule spécifié si nous voulons un affichage 
                        correct.
                    </p>
                </section>

                <section>
                    <h2>Disposition de la page</h2>
                    <p>
                        Par le passé, il était fréquent d'utiliser les tableaux pour gérer la disposition (layout) de la page Web. Aujourd'hui, les tableaux servent uniquement à faire la
                        présentation de données. Vous ne devriez jamais utiliser les tableaux pour les layouts de page. Ceci est une mauvaise pratique. Ce sera le travail du CSS de
                        s'occuper de la disposition de notre site Web.
                    </p>
                </section>
            </div>`
    }
})();