export default (function() {
let code1 = `#F78F20`;
let code2 = `#000`;
let code3 = `rgb(255, 90, 130)`;
let code4 = `rgba(123, 25, 242, .7)`;
let code5 = `crimson`;

let code1Css = 
`p {
    color: #00F;
}`;

let code1Html = 
`<p>
    Lorem ipsum accumsan nibh. Fusce condimentum tincidunt aliquet. Nam aliquam pulvinar purus, in porta est. Proin rhoncus dolor et velit placerat fermentum. Interdum et malesuada 
    fames ac ante ipsum primis in faucibus. Etiam mollis varius gravida. Nunc eleifend at velit sit amet laoreet. Etiam vel lacus in elit faucibus ornare dictum eu nisl. Vivamus nec 
    est ut velit vehicula gravida gravida ut ante.
</p>`;

let code2Css = 
`li.surligner {
    background-color: rgb(128, 13, 13);
    color: #AAAAAA;
}`;

let code2Html = 
`<ul>
    <li class="surligner">Élément 1</li>
    <li>Élément 2</li>
    <li class="surligner">Élément 3</li>
</ul>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Couleurs</h1>

                <section>
                    <h2>Définir une couleur</h2>
                    <p>
                        En informatique, les couleurs fonctionne en mélangeant une valeur de rouge, de vert et de bleu. Ces valeurs, que nous appelerons "canaux", peuvent être 
                        uniquement entre 0 et 255. En donnant des valeurs à nos 3 canaux, nous sommes en mesure d'utiliser plus de 16 millions de couleurs. Par exemple, si nous 
                        spécifions une couleur ayant 0 dans tous ces canaux, la couleur résultante sera noir. Si la valeur dans tous les canaux est de 255, alors nous avons la couleur 
                        blanche. Si nous mettons 255 dans le canal rouge, mais 0 dans le canal vert et bleu, nous aurons une couleur rouge.
                    </p>
                    <p>
                        Il existe de nombreux standard pour spécifier les 3 canaux d'une couleur. En CSS, nous avons l'embarras du choix. Voici une liste non exhaustive de façons de 
                        définir une couleur en CSS:
                    </p>
                    <dl>
                        <dt>Hexadécimal</dt>
                        <dd>
                            Vous avez probablement déjà vu ce genre de code en ligne. Chaque canal de couleur est représenté un nombre hexadécimal à 2 chiffres. Puisqu'il y a 3 canaux,
                            nous avons donc 6 chiffres hexadécimal.
                            ${ addCode(code1, 'CSS') }                            
                        </dd>

                        <dt>Hexadécimal court</dt>
                        <dd>
                            C'est une façon plus courte de définir une couleur hexdécimal. Essentiellement, nous utilisons uniquement des valeurs de 0 à 16 pour chaque canal. Bien que 
                            cela nous permette de raccourcir notre code, cela diminue de beaucoup les possibilités de couleurs. En effet, en utilisant cette méthode, nous avons accès à 
                            seulement 4096 couleurs différentes.
                            ${ addCode(code2, 'CSS') }                            
                        </dd>

                        <dt>RGB</dt>
                        <dd>
                            C'est une façon facile de définir la valeur des canaux rouge, vert et bleu respectivement avec une valeur entre 0 et 255.
                            ${ addCode(code3, 'CSS') }                            
                        </dd>

                        <dt>RGBA</dt>
                        <dd>
                            C'est le même principe que le RGB, mais en ajoutant un quatrième canal que nous appelerons le canal "alpha". Ce canal représente la transparence de la 
                            couleur. La valeur est un nombre à virgule entre 0 et 1. À 0, la couleur est complètement transparente et à 1 la couleur est complètement opaque.
                            ${ addCode(code4, 'CSS') }                            
                        </dd>

                        <dt>Nom</dt>
                        <dd>
                            Certaines couleurs ont un nom. Pour ces couleurs, il est possible de spécifier le nom de la couleur directement. Vous pouvez trouver la liste des couleurs
                            ayant un nom sur <a href="https://www.w3schools.com/colors/colors_names.asp" target="_blank">w3school</a>
                            ${ addCode(code5, 'CSS') }                            
                        </dd>
                    </dl>
                    <p>
                        Parmis tous ces choix, lequel est le meilleur? En général, pour des raisons de performance, le meilleur choix est généralement le plus petit. Nous allons donc 
                        favoriser le format hexadécimal et hexadécimal court. Toutefois, si vous devez utiliser de la transparence, n'hésitez pas à utiliser le RGBA.
                    </p>
                </section>

                <section>
                    <h2>Couleur de texte</h2>
                    <p>
                        Pour modifier la couleur du texte, nous devons changer la valeur de la propriété ${ inline('color') } d'un élément pour y mettre la couleur de notre choix. Pour
                        changer la couleur du texte de tous les paragraphes d'une page Web en bleu, nous utiliserons une feuille de style similaire à celle-ci. 
                    </p>
                    ${ addCodeExample(code1Html, code1Css) }
                </section>

                <section>
                    <h2>Couleur de fond</h2>
                    <p>
                        Pour modifier la couleur du fond d'un élément HTML, nous utiliserons la propriété ${ inline('background-color') }. Cette propriété s'utilise de la même façon que
                        pour la couleur du texte.
                    </p>
                    ${ addCodeExample(code2Html, code2Css) }
                    <p>
                        Comme vous pouvez le remarquer, il est aussi possible de combiner plusieurs proprités à l'intérieur de la même feuille de style. Dans l'exemple ci-dessus, nous 
                        utilisons ${ inline('color') } et ${ inline('background-color') } en même temps.
                    </p>
                </section>
            </div>`
    }
})();