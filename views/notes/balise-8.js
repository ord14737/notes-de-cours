export default (function() {
let code1 = 
`<form>
    <p> Prénom: </p> 
    <input type="text" placeholder="prénom"> 
    <p> Nom: </p> 
    <input type="text" placeholder="nom"> 
    <p> Courriel: </p> 
    <input type="email" placeholder="courriel@domaine.com"> 
    <p> Commentaires: </p> 
    <textarea cols="40" rows="8" placeholder="Vos commentaires" maxlength="500"></textarea> 

    <p> <input type="submit" value="Soumettre"> </p>
</form>`;

let code2 = 
`<form>
    <p>
        <label> 
            Prénom: 
            <input type="text" placeholder="prénom"> 
        </label> 
    </p>

    <p>
        <label> 
            Nom: 
            <input type="text" placeholder="nom"> 
        </label> 
    </p>

    <label for="homme">Homme</label>
    <input type="radio" name="sexe" id="homme">

    <label for="femme">Femme</label>
    <input type="radio" name="sexe" id="femme">

    <label for="autre">Autre</label>
    <input type="radio" name="sexe" id="autre">

    <p> <input type="submit" value="Soumettre"> </p>
</form>`;

let code3 = 
`<form>
    <fieldset>
        <legend>Données personnelles</legend>
        <p> Prénom: </p> 
        <input type="text" placeholder="prénom"> 
        <p> Nom: </p> 
        <input type="text" placeholder="nom"> 
    </fieldset>
    
    <fieldset>
        <legend>Contact</legend>
        <p> Courriel: </p> 
        <input type="email" placeholder="courriel@domaine.com"> 
        <p> Code postal: </p> 
        <input type="text" placeholder="Q1Q1Q1" maxlength="6"> 
        <p> Adresse: </p> 
        <input type="text" placeholder="42 rue de la réponse à la vie" maxlength="6"> 
    </fieldset>

    <p> <input type="submit" value="Soumettre"> </p>
</form>`;

let code4 = 
`<!-- Readonly -->
<p> <input type="text" value="Un champ en lecture seulement" readonly> </p>

<!-- Disabled -->
<p> <input type="text" value="Un champ désactivé" disabled> </p>

<!-- Autofocus (ne fonctionne pas vraiment dans cette page Web) -->
<p> <input type="text" value="Un champ qui a le focus par défaut" autofocus> </p>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Formulaire, nom de champs et attributs</h1>

                <section>
                    <h2>Formulaires</h2>
                    <p>
                        Les formulaires sont un moyen de regrouper plusieurs inputs ou contrôles dans le but d'éventuellement envoyer les données sur le Web. En général, vous verrez
                        une balise ${ inline('<form></form>') } délimitant un formulaire. Nous ajouterons généralement ces balises lorsque nous avons besoin d'envoyer le formulaire à un serveur,
                        ce qui n'est pas notre cas pour le moment, mais c'est une bonne pratique de le faire.
                    </p>
                    <p>
                        Pour envoyer un formulaire, il est généralement nécessaire d'avoir un bouton pour le soumettre. Dans ce cas, nous utiliserons généralement le input de type
                        ${ 'submit' }.
                    </p>
                    ${ addCodeExample(code1) }
                </section>

                <section>
                    <h2>Nom de champ</h2>
                    <p>
                        Jusqu'à maintenant, pour spécifier le nom d'un champ, nous utilisions la balise ${ inline('<p>') } ou simplement du texte. Il existe toutefois une meilleur balise pour ce 
                        genre de travail. En effet, la balise ${ inline('<label></label>') } vous permet non seulement de spécifier un nom de champ, mais offre aussi de mettre le focus sur le 
                        bon input ou contrôle si l'on clique dessus.
                    </p>
                    <p>
                        Pour spécifier quel contrôle est pointé par quel nom, il vous suffit simplement de mettre le contrôle ou l'input dans le ${ inline('<label>') } auquel vous voulez
                        qu'il corresponde. Une autre façon de faire est d'utiliser l'attribut ${ inline('for') } dans le ${ inline('<label>') } et d'y ajouter le ${ inline('id') } de 
                        l'input que vous voulez décrire.
                    </p>
                    ${ addCodeExample(code2) }
                </section>

                <section>
                    <h2>Groupe d'inputs</h2>
                    <p>
                        Il est possible de regrouper des inputs ou des contrôles dans des sections du formulaire. Nous utiliserons la balise ${ inline('<fieldset></fieldset>') } à 
                        l'intérieur du formulaire pour spécifier un groupe et la balise ${ inline('<legend></legend>') } pour donner un nom au groupe à l'intérieur de la balise 
                        ${ inline('<fieldset>') }. La balise ${ inline('<legend>') } doit impérativement être la première balise à l'intérieur de la balise ${ inline('<fieldset>') }
                        pour que votre HTML soit valide.
                    </p>
                    ${ addCodeExample(code3) }
                </section>

                <section>
                    <h2>Attributs supplémentaires</h2>
                    <p>
                        Il existes de nombreux attributs supplémentaires qui peuvent être mis sur les éléments d'un formulaire. Nous en avons vu quelques-uns avec les inputs et les
                        contrôles de formulaire. En voici d'autres, mais plus générique cette-fois, qui peuvent être mis sur n'importe quel input ou contrôle.
                        <dl>
                            <dt>${ inline('readonly') }</dt>
                            <dd>Attribut qui indique que le champ ne peut pas être modifié. Vous pouvez tout de même copier le texte à l'intérieur.</dd>
                        </dl>

                        <dl>
                            <dt>${ inline('disabled') }</dt>
                            <dd>Attribut qui indique que le champ est désactivé. Un champ désactivé n'est pas utilisable.</dd>
                        </dl>

                        <dl>
                            <dt>${ inline('autofocus') }</dt>
                            <dd>
                                Attribut indiquant qu'au chargement de la page, le focus devrait être sur ce champ. Assurez-vous de n'avoir qu'un seul attribut ${ inline('autofocus') }
                                pour ne pas créer de conflits.
                            </dd>
                        </dl>
                    </p>
                    ${ addCodeExample(code4) }
                    <p>
                        Il existe de nombreux autres attributs, certains plus spécifique que d'autres. Vous pouvez les retrouver sur 
                        <a href="https://www.w3schools.com/html/html_form_attributes.asp" target="_blank">w3school</a>.
                    </p>
                </section>
            </div>`
    }
})();
