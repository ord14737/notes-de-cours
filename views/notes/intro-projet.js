export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Création d'un projet</h1>

                <section>
                    <h2>Types de fichiers</h2>
                    <p>
                        Les projets Web sont, comme pour la plupart des projets de programmation, simplement une hierarchie de dossiers et de fichiers. Toutefois, contrairement à un projet en
                        Java qui contient majoritairement des fichiers de code ".java", les projets de programmation en Web contiendront plusieurs fichiers différents. Voici quelques exemples
                        de fichiers généralement vu dans un projet Web:
                    </p>
                    <dl>
                        <dt>Fichier HTML (.html)</dt>
                        <dd>Les fichiers permettant de structurer une page Web. Nous verrons comment utiliser le HTML dans les premières semaines de la session.</dd>
                        <dt>Fichier CSS (.css)</dt>
                        <dd>
                            Les fichiers permettant de styliser nos pages Web. Nous utiliserons ces fichier pour rendre nos sites Web plus beau et mieux organisé. Nous verrons comment 
                            utiliser le CSS après avoir vu le HTML plus en profondeur.
                        </dd>
                        <dt>Fichier Javascript (.js)</dt>
                        <dd>
                            Les fichiers permettant de donner des fonctionnalités plus complexe à notre projet Web. Nous utiliserons ces fichier pour rendre nos sites Web plus dynamique. Nous 
                            verrons comment utiliser le Javascript après avoir vu le CSS, donc probablement un peu après la mi-session.
                        </dd>
                        <dt>Fichier de média (Image, Vidéo ou Musique)</dt>
                        <dd>
                            Les fichiers permettant de donner plus d'information ou une meilleure expérience à ceux utilisant nos pages Web. Nous verrons comment intégrer ces fichiers dans 
                            le HTML durant les prochaines semaines.
                        </dd>
                    </dl>
                </section>

                <section>
                    <h2>Structure</h2>
                    <p>
                        Puisque nous aurons à gérer de nombreux fichiers, notre structure de projet devra être assez flexible et organisée. Contrairement à des projets fait dans un autre 
                        langage de programmation, il est très fréquent dans un projet Web de définir la structure du projet vous-même. Vous allez donc devoir créer cette structure à la
                        main, par vous-même. Pour ce cours, nous utiliserons la structure de projet suivante:
                    </p>
                    <div class="schema">
                        <ul>
                            <li>
                                <i class="material-icons md-18">folder</i>
                                <span>nom-du-projet</span>
                                <ul>
                                    <li>
                                        <i class="material-icons md-18">folder</i>
                                        <span>css</span>
                                        <ul>
                                            <li>
                                                <i class="material-icons md-18">note</i>
                                                <span>style.css</span>
                                            </li>
                                            <li>
                                                <i class="material-icons md-18">note</i>
                                                <span>(autres fichiers CSS)</span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <i class="material-icons md-18">folder</i>
                                        <span>img</span>
                                        <ul>
                                            <li>
                                                <i class="material-icons md-18">image</i>
                                                <span>(fichiers d'image)</span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <i class="material-icons md-18">folder</i>
                                        <span>js</span>
                                        <ul>
                                            <li>
                                                <i class="material-icons md-18">note</i>
                                                <span>main.js</span>
                                            </li>
                                            <li>
                                                <i class="material-icons md-18">note</i>
                                                <span>(autres fichiers Javascript)</span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <i class="material-icons md-18">note</i>
                                        <span>index.html</span>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <p>
                        À chaque nouveau projet que vous démarrerez dans ce cours, à moins d'indication contraire, vous devez créer cette structure de projet de base par vous-même.
                    </p>
                </section>
                
                <section>
                    <h2>Fichiers nécessaires</h2>
                    <p>
                        Le fichier index.html est le plus important pour le moment. En effet, ce fichier est toujours la première page de notre site Web. Dans chacun de vos projets Web, 
                        vous devez avoir un fichier index.html. Si éventuellement vous créez d'autres pages Web avec d'autres fichiers HTML, vous pourrez les mettre dans le dossier principal
                        du projet avec un nom significatif.
                    </p>
                    <p>
                        Les fichiers style.css et main.js ne sont pas nécessaire pour le moment. Nous les ajouterons plus tard durant la session lorsque nous étudierons le CSS et le 
                        Javascript. Je vous demande quand même de créer les dossiers /css, /img et /js.
                    </p>
                </section>
            </div>`
    }
})();