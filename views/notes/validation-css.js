export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Validation du CSS</h1>

                <section>
                    <h2>Pourquoi</h2>
                    <p>
                        Comme pour le HTML, il est important de valider son CSS. En effet, ceci peut prévenir des bogues d'affichage sur différents navigateurs et aussi favoriser de 
                        meilleurs classements sur les moteurs de recherche comme Google. Le CSS est en changement continuel. Il est donc possible que votre CSS ne valide pas si vous
                        utilisez des propriétés CSS plus récentes. C'est effectivement un risque si vous utilisez des fonctionnalités qui ne sont pas encore dans les standards.
                    </p>
                </section>

                <section>
                    <h2>Outils de validation</h2>
                    <p>
                        Bien que la plupart des éditeurs de codes permettent de valider le CSS directement, ils sont souvent très permissif. En effet, plusieurs fonctionnalités non 
                        standard peuvent être utilisé sans problème dans un éditeur de code, mais être problématique (ou non) dans un site Web si la fonctionnalité change. Je vous 
                        recommande donc d'utiliser l'outil suivant:
                        <dl>
                            <dt><a href="https://jigsaw.w3.org/css-validator/#validate_by_input" target="_blank">Service de validation du W3C</a></dt>
                            <dd>
                                Comme pour le HTML, le consortium du Web nous offre un validateur de CSS. Je vous recommande donc de toujours passer votre code CSS final dans ce 
                                validateur en ligne. Il s'utilise de la même façon que le validateur de HTML.
                            </dd>
                        </dl>
                    </p>
                </section>
            </div>`
    }
})();