let code = [];

code.push({});
code[0].html =
`<div class="conteneur-flexbox">
    <div>Élément Flexbox</div>
    <div>Élément Flexbox</div>
    <div>Élément Flexbox</div>
    <div>Élément Flexbox</div>
</div>`;

code[0].css = 
`.conteneur-flexbox {
    display: flex;
    background-color: #CCC;
}

.conteneur-flexbox > div {
    margin: 1em;
    padding: 1em;
    color: #EEE;
    background-color: #333;
}`;

code.push({});
code[1].exHtml =
`<div class="conteneur-flexbox">
    <span>1</span>
    <span>2</span>
    <span>3</span>
    <span>4</span>
    <div class="texte">axe</div>
    <div class="axe"></div>
    <div class="fleche"></div>
</div>`;

code[1].exCss = 
`.conteneur-flexbox {
    display: flex;
    position: relative;
    background-color: #CCC;
}

.conteneur-flexbox > span {
    margin: 1em;
    padding: 1em;
    color: #EEE;
    background-color: #333;
}

.axe {
    position: absolute;
    border-bottom: 2px solid #900;
    left: 0;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
}

.fleche {
    position: absolute;
    height: 0;
    border-left: .8em solid #900;
    border-top: .8em solid transparent;
    border-bottom: .8em solid transparent;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
}

.texte {
    position: absolute;
    color: #900;
    right: 1.5em;
    bottom: 50%;
}`;

code.push({});
code[2].html =
`<div class="conteneur-flexbox">
    <div>Élément Flexbox</div>
    <div>Élément Flexbox</div>
    <div>Élément Flexbox</div>
    <div>Élément Flexbox</div>
</div>`;

code[2].css = 
`.conteneur-flexbox {
    display: flex;
    flex-direction: column;
    background-color: #CCC;
}

.conteneur-flexbox > div {
    margin: .2em;
    padding: .5em;
    color: #EEE;
    background-color: #333;
}`;

code.push({});
code[3].html =
`<div class="conteneur-flexbox">
    <div>Élément Flexbox assez long</div>
    <div>Élément Flexbox assez long</div>
    <div>Élément Flexbox assez long</div>
    <div>Élément Flexbox assez long</div>
    <div>Élément Flexbox assez long</div>
</div>`;

code[3].css = 
`.conteneur-flexbox {
    display: flex;
    flex-wrap: wrap;
    background-color: #CCC;
}

.conteneur-flexbox > div {
    margin: .2em;
    padding: .5em;
    color: #EEE;
    background-color: #333;
}`;

code.push({});
code[4].exHtml =
`justify-content: flex-start
<div class="conteneur-flexbox start">
    <div></div> <div></div> <div></div> <div></div> <div></div>
</div>

justify-content: flex-end
<div class="conteneur-flexbox end">
    <div></div> <div></div> <div></div> <div></div> <div></div>
</div>

justify-content: center
<div class="conteneur-flexbox center">
    <div></div> <div></div> <div></div> <div></div> <div></div>
</div>

justify-content: space-between
<div class="conteneur-flexbox between">
    <div></div> <div></div> <div></div> <div></div> <div></div>
</div>

justify-content: space-around
<div class="conteneur-flexbox around">
    <div></div> <div></div> <div></div> <div></div> <div></div>
</div>

justify-content: space-evenly
<div class="conteneur-flexbox even">
    <div></div> <div></div> <div></div> <div></div> <div></div>
</div>`;

code[4].exCss = 
`.conteneur-flexbox {
    display: flex;
    margin: .5em;
    background-color: #CCC;
}

.conteneur-flexbox > div {
    margin: .5em;
    width: 2em;
    height: 1.5em;
    color: #EEE;
    background-color: #333;
}

.start {
    justify-content: flex-start;
}

.end {
    justify-content: flex-end;
}

.center {
    justify-content: center;
}

.between {
    justify-content: space-between;
}

.around {
    justify-content: space-around;
}

.even {
    justify-content: space-evenly;
}`;

code.push({});
code[5].exHtml =
`align-items: flex-start
<div class="conteneur-flexbox start">
    <span></span> <span></span> <span></span> <span></span> <span></span>
    <div class="texte">axe</div> <div class="axe"></div> <div class="fleche"></div>
</div>

align-items: flex-end
<div class="conteneur-flexbox end">
    <span></span> <span></span> <span></span> <span></span> <span></span>
    <div class="texte">axe</div> <div class="axe"></div> <div class="fleche"></div>
</div>

align-items: center
<div class="conteneur-flexbox center">
    <span></span> <span></span> <span></span> <span></span> <span></span>
    <div class="texte">axe</div> <div class="axe"></div> <div class="fleche"></div>
</div>

align-items: baseline
<div class="conteneur-flexbox base">
    <span></span> <span></span> <span></span> <span></span> <span></span>
    <div class="texte">axe</div> <div class="axe"></div> <div class="fleche"></div>
</div>

align-items: stretch
<div class="conteneur-flexbox stretch">
    <span></span> <span></span> <span></span> <span></span> <span></span>
    <div class="texte">axe</div> <div class="axe"></div> <div class="fleche"></div>
</div>`;


code[5].exCss = 
`.conteneur-flexbox {
    display: flex;
    position: relative;
    margin: .5em;
    background-color: #CCC;
    height: 5em;
}

.conteneur-flexbox > span {
    margin: .5em;
    width: 2em;
    color: #EEE;
    background-color: #333;
}

.conteneur-flexbox > span:nth-child(1){ height: 1em; }
.conteneur-flexbox > span:nth-child(2){ height: 3em; }
.conteneur-flexbox > span:nth-child(3){ height: 2em; }
.conteneur-flexbox > span:nth-child(4){ height: 1.5em; }
.conteneur-flexbox > span:nth-child(5){ height: 2.5em; }

.start { align-items: flex-start; }
.end { align-items: flex-end; }
.center { align-items: center; }
.base { align-items: baseline; }
.stretch { align-items: stretch; }

.conteneur-flexbox.stretch > span {
    height: auto;
}

.axe {
    position: absolute;
    border-bottom: 2px solid #900;
    left: 0;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
}

.fleche {
    position: absolute;
    height: 0;
    border-left: .8em solid #900;
    border-top: .8em solid transparent;
    border-bottom: .8em solid transparent;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
}

.texte {
    position: absolute;
    color: #900;
    right: 1.5em;
    bottom: 50%;
}`;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Flexbox</h1>

            <section>
                <h2>Introduction</h2>
                <p>
                    Les Flexbox sont un moyen moderne d'afficher des éléments un à côté de l'autre dans votre page Web. Puisqu'ils sont relativement récent, nous utilisions par le passé
                    les affichages de type ${ inline('inline') }, ${ inline('block') } ou ${ inline('inline-block') } pour faire ce genre d'affichage. Ces techniques sont toutefois plus 
                    difficile à maintenir et à modifier. Les Flexbox sont donc un changement très apprécié des programmeurs Web.
                </p>
                <p>
                    Nous utiliserons les Flexbox pour toutes sortes de choses. Entre autres, pour afficher un menu horizontal dans le haut de notre page, pour afficher des cartes de façon 
                    réactive (responsive) sur plusieurs lignes, pour centrer un élément autant verticalement que horizontalement ou encore pour pousser un ${ inline('<footer>') } au bas 
                    de notre page Web.
                </p>
                <p>
                    Les Flexbox peuvent être un peu intimidants puisqu'ils nécessitent beaucoup de propriétés CSS pour se configurer. Si vous les maîtrisez, ils sont toutefois très 
                    facile à utiliser et facileront énormément votre travail.
                </p>
            </section>

            <section>
                <h2>Changement d'affichage</h2>
                <p>
                    Pour afficher des éléments à la manière Flexbox, nous utiliserons la propriété ${ inline('display: flex') }. Il faut comprendre que c'est le conteneur qui doit être 
                    spécifié en Flexbox. Essentiellement, si un conteneur est spécifié comme Flexbox, tous les éléments enfants directs qu'il contient seront affiché en Flexbox. Ceci est 
                    différent des types d'affichage conventionnel commme ${ inline('inline') } ou ${ inline('block') } qui affecte l'élément spécifié directement.
                </p>
                <code-example v-bind:code="code[0]"></code-example>
                <p>
                    Dans l'exemple ci-dessus, vous noterez que seulement le conteneur est marqué comme ${ inline('flex') }. L'affichage du conteneur ${ inline('flex') } est 
                    essentiellement un affichage en ${ inline('block') }. Vous remarquerez toutefois que même si les ${ inline('<div>') } dans ce conteneur sont affichés en 
                    ${ inline('block') }, ils s'affichent plutôt comme des éléments ${ inline('inline-block') }. Ce type d'affichage serait le même, même si nous utilisons une autre 
                    balise qu'un ${ inline('<div>') }
                </p>
            </section>

            <section>
                <h2>Axe d'affichage</h2>
                <p>
                    Si vous lisez un peu d'information sur les Flexbox, vous verrez qu'on le mentionne souvent comme un affichage unidimensionnel. Ceci est dû au fait que les éléments
                    à l'intérieur d'un conteneur Flexbox suivent un axe d'affichage. Par défaut, cet axe d'affichage est horizontal de gauche à droite.
                </p>
                <code-example v-bind:code="code[1]"></code-example>
                <p>
                    Il existe différentes propriétés CSS pour controller cet axe. Par exemple, il est possible de mettre l'axe de façon verticale où encore de le mettre de droite à 
                    gauche au lieu de gauche à droite. Pour ce genre de modification, nous utiliserons la propriété ${ inline('flex-direction') } sur le conteneur Flexbox.
                </p>
                <code-example v-bind:code="code[2]"></code-example>
                <p>
                    Pour d'autres exemples d'utilisation de cette propriété, vous pouvez aller vous renseigner sur 
                    <a href="https://www.w3schools.com/cssref/css3_pr_flex-direction.asp" target="_blank">w3school</a>.
                </p>
            </section>

            <section>
                <h2>Mettre l'axe sur plusiseurs lignes</h2>
                <p>
                    Si vous avez beaucoup d'éléments dans votre conteneur Flexbox, il est possible que vos éléments débordent de votre conteneur. Dans ce genre de cas, nous voulons 
                    souvent simplement mettre le contenu sur plusieurs ligne. Il est possible de forcer l'axe à continuer sur une prochaine ligne (ou colonne) à l'aide de la propriété
                    ${ inline('flex-wrap') }.
                </p>
                <code-example v-bind:code="code[3]"></code-example>
                <p>
                    Pour d'autres exemples d'utilisation de cette propriété, vous pouvez aller vous renseigner sur 
                    <a href="https://developer.mozilla.org/fr/docs/Web/CSS/flex-wrap" target="_blank">MDN</a>.
                </p>
            </section>

            <section>
                <h2>Espacements</h2>
                <p>
                    Si vous désirez placer des espacement entre les éléments dans le conteneur Flexbox, il est possible de faire plusieurs affichage intéressant avec la prorpriété
                    ${ inline('justify-content') }. Cette propriété CSS s'utilise sur le conteneur et contient de nombreuses valeurs très utile dépendant de ce que l'on veut faire.
                </p>
                <code-example v-bind:code="code[4]"></code-example>
                <p>
                    Pour d'autres exemples d'utilisation de cette propriété, vous pouvez aller vous renseigner sur 
                    <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/justify-content" target="_blank">MDN</a>.
                </p>
            </section>

            <section>
                <h2>Alignement sur l'axe</h2>
                <p>
                    Si les éléments dans le conteneur Flexbox n'ont pas tous la même taille verticalement (dans le cas d'en Flexbox en rangée), par défaut, ceux-ci seront forcé à
                    prendre toute la hauteur du conteneur. Nous pouvons toutefois changer ce comportement de base avec la propriété ${ inline('align-items') }.
                </p>
                <code-example v-bind:code="code[5]"></code-example>
                <p>
                    Pour d'autres exemples d'utilisation de cette propriété, vous pouvez aller vous renseigner sur 
                    <a href="https://css-tricks.com/almanac/properties/a/align-items/" target="_blank">CSS-TRICKS</a>.
                </p>
            </section>

            <section>
                <h2>Autres ressources</h2>
                <p>
                    Il existe de nombreuses autres fonctions des Flexbox. Il est en effet entre autre possible de changer l'ordre ou la taille des éléments dans un Flexbox sans même 
                    modifier le HTML. Je ne couvrirai pas ces situations ici, mais si cela vous intéresse, vous pourrez aller faire quelques recherches sur le Web. Autrement, si les 
                    Flexbox restent un mystère pour vous ou si vous désirez simplement approfondir ou solidifier vos connaissances, je vous suggère fortement les ressources suivantes:
                </p>
                <ul>
                    <li><span><a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/" target="_blank">
                        CSS-TRICKS - Guide des Flexbox
                    </a></span></li>
                    <li><span><a href="https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Concepts_de_base_flexbox" target="_blank">
                        MDN - Les concepts de base pour Flexbox
                    </a></span></li>
                </ul>
            </section>

            <section>
                <h2>Disposition en grille</h2>
                <p>
                    Si (comme moi) vous aimez les Flexbox, il existe une autre disposition qui peut être très utile. On l'appelle la disposition en grille. Elle est un peu plus 
                    complexe que les Flexbox. Ceci est dû au fait que cette disposition est multidimensionnelle (2 dimensions) contrairement au Flexbox qui est unidimentionnel 
                    (1 dimension / axe). Cette disposition vous permet toutefois beaucoup plus de flexibilité. Nous ne pourrons pas la couvrir dans le cours, mais vous pouvez en 
                    apprendre plus à son sujet sur <a href="https://css-tricks.com/snippets/css/complete-guide-grid/" target="_blank">CSS-TRICKS</a>.
                </p>
            </section>
        </div>`
};
