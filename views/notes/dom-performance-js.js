export default {
    template: `
        <div class="note" v-once>
            <h1>Performance du DOM</h1>

            <section>
                <h2>Ressources sur le DOM en Javascript</h2>
                <p>
                    Voici quelques ressources externes qui vous indiqueront différente façon de manipuler le DOM en Javascript.
                </p>
                <ol>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/API/WindowOrWorkerGlobalScope/setTimeout" target="_blank">
                            Utilisation du setTimeout - MDN
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://www.reddit.com/r/javascript/comments/9d4i8d/when_to_use_innerhtml_and_is_it_slower_than/" target="_blank">
                            appendChild ou inneHTML - Post reddit
                        </a>
                    </span></li>
                </ol>
            </section>

            <section>
                <h2>Capsule vidéo</h2>
                <div class="video">
                    <div>
                        <iframe
                            src="https://www.youtube.com/embed/Do4JfbqzX0M" 
                            frameborder="0" 
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </section>
        </div>`
};