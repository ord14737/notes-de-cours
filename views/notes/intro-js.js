export default {
    template: `
        <div class="note" v-once>
            <h1>Introduction au Javascript</h1>

            <section>
                <h2>Ressources sur le Javascript</h2>
                <p>
                    Voici quelques ressources externes qui vous permettront d'apprendre les bases du Javascript. Je vous suggère fortement des les consulter dans l'ordre de cette liste.
                </p>
                <ol>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Learn/JavaScript/First_steps/What_is_JavaScript#JavaScript_externe" target="_blank">
                            Ajouter des fichiers Javascript à voter page Web
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Learn/JavaScript/First_steps/Variables" target="_blank">
                            Les variables
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs#Incr%C3%A9mentation_et_d%C3%A9cr%C3%A9mentation" target="_blank">
                            Opérateurs
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Contr%C3%B4le_du_flux_Gestion_des_erreurs#Les_instructions_conditionnelles" target="_blank">
                            Booléens et Instructions conditionnelles
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Boucles_et_it%C3%A9ration" target="_blank">
                            Boucles
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://www.w3schools.com/js/js_string_methods.asp" target="_blank">
                            Chaîne de caractères (String)
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Math" target="_blank">
                            Fonctions mathématiques
                        </a>
                    </span></li>
                    <li><span>
                        <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Math/random" target="_blank">
                            Génération aléatoire
                        </a>
                    </span></li>
                </ol>
            </section>
        </div>`
};