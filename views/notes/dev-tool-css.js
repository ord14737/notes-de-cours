export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Outils de développement</h1>

                <section>
                    <h2>Navigateur et développement</h2>
                    <p>
                        Tous les grands navigateurs Web offre des outils de développement interne qui se spécialise au niveau du HTML, CSS, Javascript, du réseau et même de la mémoire 
                        utilisé par vos pages Web. Ces outils sont complexes, mais peuvent nous aider grandement si on sait comment les utiliser. Je vous suggère de vous familiariser 
                        avec les outils de développement d'un navigateur de votre choix. Dans ce document, je ne passerai pas en revu ces outils pour chaque navigateur, mais vous aurez 
                        des liens vers des tutoriels ou des vidéos pour apprendre à utiliser ces outils, en particulier avec le CSS.
                    </p>
                    <ul>
                        <li><a href="https://developers.google.com/web/tools/chrome-devtools/css" target="_blank">Chrome DevTools</a></li>
                        <li><a href="https://support.apple.com/fr-ca/guide/safari-developer/welcome/mac" target="_blank">Safari Developer Tools</a></li>
                        <li><a href="https://developer.mozilla.org/en-US/docs/Tools/Page_Inspector/How_to/Examine_and_edit_CSS" target="_blank">Firefox Developer Tools</a></li>
                        <li><a href="https://docs.microsoft.com/en-us/microsoft-edge/devtools-guide/elements/styles" target="_blank">EdgeHTML</a></li>
                    <ul>
                </section>
            </div>`
    }
})();