export default (function() {
let code1 =
`<!-- Une image venant d'un site Web externe -->
<p> Un dinosaur </p>
<img src="https://freesvg.org/img/Dinosaur5.png" alt="Dinosaur">

<!-- Une image venant de notre dossier /img -->
<p> Un weedle </p>
<img src="./img/weedle.webp" alt="Dinosaur" alt="Weedle">`;

let code1Css = 'img { width: 8em; }';

let code2 =
`<iframe 
    width="560" 
    height="315" 
    src="https://www.youtube-nocookie.com/embed/sdUUx5FdySs" 
    frameborder="0" 
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
    allowfullscreen>
</iframe>`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Media</h1>

                <section>
                    <h2>Images</h2>
                    <p>
                        Les images sont probablement l'élément média le plus vieux et le plus simple que l'on peut mettre dans une page Web. Pour débuter, vous devrez mettre l'image que 
                        vous désirez ajouter à votre site Web dans le dossier ${ inline('/img') } de votre projet Web. Par la suite, pour ajouter l'image à une pages Web, nous 
                        utiliserons simplement la balise ${ inline('<img>') }. Cette balise est l'une des rares balise à ne pas avoir de balise fermante. Pour spécifier l'image à afficher,
                        nous devons spécifier un attribut ${ inline('src') } qui correspondra à l'URL de l'image. Bien entendu, cet URL peut être relatif pour les images dans le dossier
                        ${ inline('/img') }.
                    </p>
                    ${ addCodeExample(code1, [code1Css]) }
                    <p>
                        Vous pouvez constater que les images ont tous un attribut ${ inline('alt') }. Cet attribut indique simplement ce que l'image affiche. Il est surtout utilisé par les
                        lecteurs de pages Web pour les personnes ayant un handicap visuel ou encore pour afficher du texte si l'image n'est pas en mesure de se charger. Vous devez toujours
                        mettre un attribut ${ inline('alt') } sur une balise ${ inline('<img>') }.
                    </p>
                </section>

                <section>
                    <h2>Vidéo externe à la page Web</h2>
                    <p>
                        Une des façon les plus facile d'intégrer des vidéos dans notre site Web est de simplement héberger notre vidéo sur un autre site Web (Youtube, Vimeo, DailyMotion)
                        et ensuite de lier la vidéo à notre site Web. Cette façon de faire permet entres autres d'économiser de la mémoire pour la taille de notre site Web. Chaque site 
                        Web d'hébergement de vidéo utilise son propre code pour partager les vidéo. Il faut donc aller lire un peu sur le sujet si vous désirez le faire. Dans le cas de 
                        Youtube, pour ajouter une vidéo à notre site Web, nous n'avons qu'à nous rendre sur la page Web de la vidéo Youtube, cliquer sur le bouton "Partager", suivit de
                        "Intégrer". Vous n'avez plus qu'à copier le code affiché dans votre site Web.
                    </p>
                    ${ addCodeExample(code2) }
                    <p>
                        Dépendant de la plate-forme contenant votre vidéo, il est possible de voir des balises comme ${ inline('<embed>') }, ${ inline('<object>') }, 
                        ${ inline('<iframe>') } ou encore ${ inline('<video>') }.
                    </p>
                </section>

                <section>
                    <h2>Vidéo et son interne à la page Web</h2>
                    <p>
                        Il est possible d'utiliser les balises ${ inline('<video>') } ou ${ inline('<audio>') } pour insérer respectivement des fichiers vidéos ou des fichiers audios 
                        dans une page Web. Je ne mettrai pas la documentation ici, mais vous pouvez trouvez des exemples d'utilisation sur w3schools aux liens suivant:
                    </p>
                    <ul>
                        <li><span><a href="https://www.w3schools.com/html/html5_video.asp" target="_target">Vidéo</a></span></li>
                        <li><span><a href="https://www.w3schools.com/html/html5_audio.asp" target="_target">Audio</a></span></li>
                    </ul>
                </section>

                <section>
                    <h2>Type de fichiers</h2>
                    <p>
                        Voici une liste non exhaustives des types de fichiers multimédias généralement utilisé dans un site Web:
                    </p>
                    <table>
                        <thead>
                            <tr>
                                <td>Fichier d'image</td>
                                <td>Fichier audio</td>
                                <td>Fichier vidéo</td>
                            </tr>
                        </thead>
                        <tr><td>.jpg</td><td>.midi</td><td>.mpeg</td></tr>
                        <tr><td>.png</td><td>.wma</td><td>.avi</td></tr>
                        <tr><td>.gif</td><td>.wav</td><td>.wmv</td></tr>
                        <tr><td>.webp</td><td>.ogg</td><td>.mov</td></tr>
                        <tr><td>.svg</td><td>.mp3</td><td>.ogg</td></tr>
                        <tr><td></td><td>.mp4</td><td>.webm</td></tr>
                        <tr><td></td><td></td><td>.mp4</td></tr>
                    </table>
                </section>
            </div>`
    }
})();
