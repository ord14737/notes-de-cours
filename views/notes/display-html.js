export default (function(){
    return {
        template: `
            <div class="note" v-once>
                <h1>Affichage de bloc ou en ligne</h1>

                <section>
                    <h2>Type d'affichage d'une balise</h2>
                    <p>
                        Sauf certaines exceptions, il existe essentiellement 2 types d'affichage pour chaque balise:
                    </p>
                    <ul>
                        <li><span>Affichage en bloc (block)</span></li>
                        <li><span>Affichage en ligne (inline)</span></li>
                    </ul>
                    <p>
                        Chaque balise a un type d'affichage par défaut et cela affecte sa façon d'apparaître à l'écran.
                    </p>
                </section>

                <section>
                    <h2>Affichage en bloc (block)</h2>
                    <p>
                        L'affichage en bloc est un affichage qui force son élément à prendre toute l'espace possible sur la largeur et à ne prendre que l'espace nécesssaire en hauteur.
                        Si vous voulez mettre des éléments un en dessous de l'autre, c'est ce type d'affichage que vous rechercherez. Voici une liste des balises vu jusqu'à maintenant qui
                        utilise l'affichage en bloc.
                    </p>
                    <ul>
                        <li><span>${ inline('<p>') }</span></li>
                        <li><span>${ inline('<h?>') }</span></li>
                        <li><span>${ inline('<pre>') }</span></li>
                        <li><span>${ inline('<ul>') } / ${ inline('<ol>') } / ${ inline('<dl>') }</span></li>
                        <li><span>${ inline('<li>') } / ${ inline('<dt>') } / ${ inline('<dd>') }</span></li>
                        <li><span>${ inline('<header>') }</span></li>
                        <li><span>${ inline('<footer>') }</span></li>
                        <li><span>${ inline('<main>') }</span></li>
                        <li><span>${ inline('<aside>') }</span></li>
                        <li><span>${ inline('<nav>') }</span></li>
                        <li><span>${ inline('<section>') }</span></li>
                    </ul>
                </section>

                <section>
                    <h2>Affichage en ligne (inline)</h2>
                    <p>
                        L'affichage en ligne est un affichage qui force son élément à suivre le texte dans un site Web. Les balises pouvant manipuler du texte ou s'insérer facilement dans
                        le texte ont généralement ce type d'affichage. Si vous voulez mettre des éléments un à côté de l'autre, vous utiliserez généralement cette balise (pour le moment).
                        Voici une liste des balises vu jusqu'à maintenant qui utilise l'affichage en ligne.
                    </p>
                    <ul>
                        <li><span>Le texte</span></li>
                        <li><span>${ inline('<strong>') }</span></li>
                        <li><span>${ inline('<a>') }</span></li>
                        <li><span>${ inline('<img>') }</span></li>
                        <li><span>${ inline('<iframe>') }</span></li>
                        <li><span>${ inline('<video>') }</span></li>
                    </ul>
                </section>

                <section>
                    <h2>Autres types d'affichage</h2>
                    <p>
                        Il existe de nombreux autres types d'affichage. Par exemple, les tableaux, les rangées de tableaux ainsi que les cellules de tableau ont tous leur propre affichage.
                        Il en existe toutefois beaucoup d'autre. Au total, il y a plus de 10 types d'affichage différent. Nous utiliserons toutefois ces types d'affichage avec du CSS
                        puisque c'est la seule façon de changer le type d'affichage d'une balise et d'utiliser certains de ces types d'affichages.
                    </p>
                </section>
            </div>`
    }
})();
