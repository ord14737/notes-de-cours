let code = [];

code.push({});
code[0].html =
`<div class="conteneur">
    <div>Élément à centrer</div>
</div>`;

code[0].css = 
`.conteneur {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 10em;
    background-color: #CCC;
}

.conteneur > div {
    padding: 1em;
    color: #EEE;
    background-color: #333;
}`;

code.push({});
code[1].html =
`<div class="page-wrapper">
    <header> Entête </header>
    <main>Contenu trop court de la page</main>
    <footer> Pied-de-page </footer>
</div>`;

code[1].css = 
`.page-wrapper {
    display: flex;
    flex-direction: column;
    min-height: 100vh;
}

header, footer {
    background-color: #CCC;
} 

main {
    flex: 1;
}`;

code[1].exCss = 
`body {
    margin: 0;
    height: 10em;
}

.page-wrapper {
    display: flex;
    flex-direction: column;
    min-height: 100vh;
}

.page-wrapper > * {
    padding: .5em;
}

header, footer {
    background-color: #CCC;
} 

main {
    flex: 1;
}`;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Trucs avec Flexbox</h1>

            <section>
                <h2>Introduction</h2>
                <p>
                    Cette page est dédié à tous ces trucs pratiques que l'on peut faire avec les Flexbox. Je suis certain que vous trouverez quelques trucs intéressants qui vous pourrez
                    utiliser dans vos sites Web. Si vous utiliser certains trucs pratiques avec Flexbox qui ne se retrouvent pas sur la page, n'hésitez pas à me les mentionner pour que
                    je puisse les ajouter.
                </p>
            </section>

            <section>
                <h2>Centrer un élément horizontalement et verticalement</h2>
                <code-example v-bind:code="code[0]"></code-example>
            </section>

            <section>
                <h2>Forcer un ${ inline('<footer>') } en bas de la page</h2>
                <code-example v-bind:code="code[1]"></code-example>
            </section>
        </div>`
};
