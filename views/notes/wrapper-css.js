export default (function() {
let code1Html =
`<div class="wrapper">
    <h1>Titre de ma page</h1>
    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, dolore sunt? Ut quos fugiat repellendus distinctio 
        atque quis? Perspiciatis suscipit maxime temporibus aspernatur ipsa atque qui porro itaque iusto laboriosam?
    </p>
</div>`;

let code1Css =
`.wrapper {
    max-width: 600px;
    margin: 0 auto;
}`;

    return {
        template: `
            <div class="note" v-once>
                <h1>Wrapper</h1>

                <section>
                    <h2>Définition</h2>
                    <p>
                        Le wrapper est une technique CSS permettant de s'assurer que le contenu de votre page Web ne s'étire pas trop. Cette technique est surout utile lorsque votre site 
                        Web peut être vu sur des grands écrans. Elle permet de densifier le contenu de votre page Web, ce qui est souvent plus facile à lire et à regarder que si le site 
                        Web s'étire sur le long de l'écran au complet.
                    </p>
                </section>

                <section>
                    <h2>Application</h2>
                    <p>
                        Pour créer un wrapper, nous utliserons généralement un ${ inline('<div>') } qui englobera les élément que nous voulons mettre dans le wrapper. Sur ce 
                        ${ inline('<div>') }, nous utiliserons une combinaison de la propriété CSS ${ inline('max-width') } et de la technique pour centrer les éléments de type 
                        ${ inline('block') } avec la propriété ${ inline('margin') }
                    </p>
                    ${ addCodeExample(code1Html, code1Css) }
                    <p>
                        Si vous redimensionnez votre navigateur, vous verrez que quand la fênetre est de grande taille, des marges à gauche et à droite forceront le contenu du wrapper à
                        se densifier. Si vous diminuez la taille de la fenêtre, les marges finiront simplement par disparaître. Ce genre de technique est très pratique pour le 
                        développemnent de site Web "responsive".
                    </p>
                    <p>
                        Vous pouvez trouver un peu plus d'informations sur le wrapper sur <a href="https://css-tricks.com/best-way-implement-wrapper-css/" target="_blank">CSS-TRICKS</a>
                    </p>
                </section>
            </div>`
    }
})();
