export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Unité de mesures</h1>

                <section>
                    <h2>Type d'unités</h2>
                    <p>
                        Il existe 2 sortent d'unités de mesures en CSS, soit les unités de mesures absolues et les unités de mesures relative. Nous utiliserons ces unités de mesures 
                        majoritairement dans les propriétés CSS permettant de modifier la taille ou l'espacement des éléments HTML. Vous pouvez trouver plus d'information sur les unités
                        de mesure sur <a href="https://www.w3schools.com/cssref/css_units.asp" target="_blank">w3school</a>.
                    </p>
                </section>

                <section>
                    <h2>Unités absolues</h2>
                    <p>
                        Une unité de mesures absolue ne dépend de rien. Elle spécifie une taille très spécifique à l'écran. Bien que nous les utilisions de temps en temps, il n'est pas 
                        vraiment recommander de les utiliser puisque aujourd'hui, les tailles d'écrans varient beaucoup. En effet, si vous utilisez une taille absolue sur un écran 
                        d'ordinateur ou sur un téléphone cellulaire, une marge de 30 pixels peut être très différente. Voici tout de même quelques unités de mesures absolues:
                    </p>
                    <table>
                        <thead>
                            <tr>
                                <th>Unité</th><th>Définition</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>cm</td><td>Mesure en centimètres</td>
                            </tr>
                            <tr>
                                <td>mm</td><td>Mesure en millimètre</td>
                            </tr>
                            <tr>
                                <td>px</td><td>Mesure en pixels</td>
                            </tr>
                            <tr>
                                <td>in</td><td>Mesure en pouces (1in = 2.54cm = 96px)</td>
                            </tr>
                            <tr>
                                <td>pt</td><td>Mesure en points (1pt = 0.035cm = 1.33px)</td>
                            </tr>
                        </tbody>
                    </table>
                </section>

                <section>
                    <h2>Unités relatives</h2>
                    <p>
                        Une unité de mesure relative est toujours dépendante d'une autre mesure. C'est généralement le type d'unités de mesures que nous utiliserons dans nos pages Web.
                        Il existe de nombreuses unitées de mesures relatives et elles ne sont pas toutes relatives aux mêmes choses. Voici donc une liste de ces unités de mesure ainsi
                        que de ce qu'elle dépende.
                    </p>
                    <table>
                        <thead>
                            <tr>
                                <th>Unité</th><th>Définition</th><th>Utilisation</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>em</td>
                                <td>Mesure relative en fonction de la taille courante de la police de caractères (2em = 2 fois la taille courante des caractères)</td>
                                <td>margin, padding, border, font-size</td>
                            </tr>
                            <tr>
                                <td>rem</td>
                                <td>Mesure relative en fonction de la taille de base de la police de caractères (en général, la taille de base est 16px)</td>
                                <td>margin, padding, border, font-size</td>
                            </tr>
                            <tr>
                                <td>%</td>
                                <td>Mesure relative en fonction de la valeur du parent (50% = la moitié de la valeur de cette propriété du parent)</td>
                                <td>width, height</td>
                            </tr>
                            <tr>
                                <td>vw</td>
                                <td>Mesure relative en fonction de la largeur du navigateur (1vw = 1% de la largeur du navigateur)</td>
                                <td>width, height</td>
                            </tr>
                            <tr>
                                <td>vh</td>
                                <td>Mesure relative en fonction de la hauteur du navigateur (1vh = 1% de la hauteur du navigateur)</td>
                                <td>width, height</td>
                            </tr>
                        </tbody>
                    </table>
                </section>
            </div>`
    }
})();
