export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Laboratoire 9: Reproduction du style</h1>
                <p>Pour compléter ce laboratoire, vous devez:</p>
                <ol>
                    <li><span>
                        Créez une page Web comme celui de la vidéo ci-dessous à partir du HTML. Le fichier HTML et la structure de base du laboratoire vous est déjà fourni. Vous pouvez 
                        les trouver sur <a href="https://gitlab.com/ord14737/laboratoire-9" target="_blank">GitLab</a>. Vous n'avez qu'à faire un clone du projet dans votre répertoire 
                        de travail ou à télécharger le fichier ${ inline('.zip') } du projet.
                    </span></li>
                    <li><span>
                        Ajoutez le CSS de cette page Web dans le fichier ${ inline('style.css') }. Vous ne devez pas modifier le HTML du projet. Les media queries pour les cassures de 
                        la page sont déjà bons.
                    </span></li>
                    <li><span>
                        Pour ce laboratoire, il vous est interdit d'utiliser les propriétés ${ inline('display: inline') } et ${ inline('display: inline-block') }. Cela vous obligera
                        à utiliser les Flexbox.
                    </span></li>
                    <li>
                        <span>Les couleurs utilisées sont:</span>
                        <ul>
                            <li><span><span class="color-box" style="background-color: #3f5aa3"></span> ${ inline('#3f5aa3') }</span></li>
                            <li><span><span class="color-box" style="background-color: #5394d1"></span> ${ inline('#5394d1') }</span></li>
                            <li><span><span class="color-box" style="background-color: #292955"></span> ${ inline('#292955') }</span></li>
                        </ul>
                    </li>
                    <li>
                        <span>
                            Les polices de caratères utilisées sont déjà intégré au HTML de votre page. Vous pouvez tout de même les trouver sur 
                            <a href="https://fonts.google.com/" target="_blank">Google Font</a> au besoin. Vous devez utiliser les polices de la façon suivante:
                        </span>
                        <ul>
                            <li><span><a href="https://fonts.google.com/specimen/Noto+Sans+TC" target="_blank">Noto Sans TC</a> pour le texte normal de votre page</span></li>
                            <li><span><a href="https://fonts.google.com/specimen/Noto+Sans+JP" target="_blank">Noto Sans JP</a> pour les titres (h1, h2, etc.) de votre page</span></li>
                        </ul>
                    </li>
                    <li>
                        <span>Certains éléments de la page Web ont des tailles spécifiques. Voici donc leurs spécifications:</span>
                        <ul>
                            <li><span>${ inline('<header>') } a une hauteur fixe de ${ inline('25em') }</span></li>
                            <li><span>${ inline('<footer>') } a une hauteur fixe de ${ inline('6em') }</span></li>
                            <li><span>${ inline('<aside>') } a une largeur fixe de ${ inline('20em') } lorsqu'il est ouvert</span></li>
                            <li><span>${ inline('<img>') } dans la galerie ont une largeur et hauteur fixe de ${ inline('10em') } lorsqu'il est ouvert</span></li>
                            <li><span>Les autres mesures que vous utilisez peuvent être approximatives.</span></li>
                        </ul>
                    </li>
                    <li>
                        <span>Autres trucs utiles pour la reproduction du style</span>
                        <ul>
                            <li><span>
                                Pour mettre les icônes en blanc, vous pouvez utiliser ${ inline('filter: invert(1)') }. Pour en apprendre plus sur les filtres et la manipulation
                                d'images, vous pouvez visiter <a href="https://css-tricks.com/almanac/properties/f/filter/" target="_blank">CSS-TRICKS</a>
                            </span></li>
                            <li><span>Pour mettre une image comme fond à un élément HTML, vous pouvez utiliser la propriété ${ inline('background-image') }.</span></li>
                            <li><span>
                                Pour empêcher à une image de s'étirer vous pouvez utiliser les propriétés ${ inline('background-size: cover') } pour les images en fond d'élément HTML
                                et ${ inline('object-fit: cover') } pour les ${ inline('<img>') }.
                            </span></li>
                            <li><span>
                                Pour que le texte soit facilement lisible sur votre image dans le ${ inline('<header>') }, vous pouvez mettre plusieurs ${ inline('text-shadow') } 
                                sur le texte. Une recherche sur Google vous montrera comment faire.
                            </span></li>
                        </ul>
                    </li>
                </ol>
                <div class="video">
                    <div>
                        <iframe 
                            src="https://www.youtube.com/embed/mdncbCK84QE" 
                            frameborder="0" 
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>`
    }
})();