export default {
    template: `
        <div class="note" v-once>
            <h1>Laboratoire 13: Programmation en Javascript</h1>
            <p>Pour compléter ce laboratoire, vous devez:</p>
            <ol>
                <li><span>
                    Compléter l'application Web de gestion de livre. La structure de base du laboratoire vous est déjà fourni. Vous pouvez les trouver sur 
                    <a href="https://gitlab.com/ord14737/laboratoire-13" target="_blank">GitLab</a>. Vous n'avez qu'à faire un clone du projet dans votre répertoire de travail ou à 
                    télécharger le fichier ${ inline('.zip') } du projet.
                </span></li>
                <li><span>
                    Programmer un évènement sur le clic du bouton "Ajouter" pour permettre d'ajouter un livre dans la liste de livres.
                </span></li>
                <li><span>
                    Programmer un évènement sur le clic du bouton "Modifier" pour permettre de modifier le livre sélectionné dans la liste de livres.
                </span></li>
                <li><span>
                    Programmer un évènement sur le clic du bouton "Supprimer" pour permettre de supprimer le livre sélectionné dans la liste de livres.
                </span></li>
            </ol>
            <div class="video">
                <div>
                    <iframe 
                        src="https://www.youtube.com/embed/qnDFp_ODTYc" 
                        frameborder="0" 
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                        allowfullscreen>
                    </iframe>
                </div>
            </div>
        </div>`
};