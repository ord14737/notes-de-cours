export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Laboratoire 1: Reproduction d'une structure HTML</h1>
                <p>Pour compléter ce laboratoire, vous devez:</p>
                <ol>
                    <li><span>Créer la structure de base d'un projet Web</span></li>
                    <li><span>Créer une page index.html de base suivant le gabarit du cours</span></li>
                    <li><span>Ajouter les balises et le contenu nécessaire pour que votre page Web apparaîsse comme spécifié ci-dessous</span></li>
                </ol>
                <a href="./img/reproduction-html-1.jpg"><img src="./img/reproduction-html-1.jpg" alt="Laboratoire de reproduction d'une structure HTML 1"></a>
            </div>`
    }
})();
    