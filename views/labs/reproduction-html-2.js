export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Laboratoire 2: Reproduction d'une structure HTML</h1>
                <p>Pour compléter ce laboratoire, vous devez:</p>
                <ol>
                    <li><span>Créer la structure de base d'un projet Web</span></li>
                    <li><span>Créer une page index.html de base suivant le gabarit du cours</span></li>
                    <li>
                        <span>Ajouter les balises et le contenu nécessaire pour que votre page Web soit similaire à l'exemple ci-dessous</span>
                        <ol>
                            <li><span>Vous pouvez choisir 6 pokémon différents</span></li>
                            <li><span>Vous pouvez choisir un Lorem Ipsum différent</span></li>
                            <li>
                                <span>
                                    Pour les images, statistique et les liens vous pouvez aller sur 
                                    <a href="https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_National_Pok%C3%A9dex_number">Bulbapedia</a>
                                </span>
                            </li>
                        </ol>
                    </li>
                    <li><span>Les liens du menu en haut doivent déplacer la vue du navigateur pour afficher le bon Pokémon ou les statistiques</span></li>
                    <li><span>Les liens "retour en haut" doivent déplacer la vue du navigateur pour afficher le haut de la page</span></li>
                    <li><span>Assurez-vous de bien utiliser les balises sémantiques</span></li>
                </ol>
                <a href="./img/reproduction-html-2.jpg"><img src="./img/reproduction-html-2.jpg" alt="Laboratoire de reproduction d'une structure HTML 2"></a>
            </div>`
    }
})();