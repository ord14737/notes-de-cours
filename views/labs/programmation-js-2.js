export default {
    template: `
        <div class="note" v-once>
            <h1>Laboratoire 11: Programmation en Javascript</h1>
            <p>Pour compléter ce laboratoire, vous devez:</p>
            <ol>
                <li><span>
                    Compléter le créateur de cartes d'affaires. La structure de base du laboratoire vous est déjà fourni. Vous pouvez les trouver sur 
                    <a href="https://gitlab.com/ord14737/laboratoire-11" target="_blank">GitLab</a>. Vous n'avez qu'à faire un clone du projet dans votre répertoire de travail ou à 
                    télécharger le fichier ${ inline('.zip') } du projet.
                </span></li>
                <li><span>
                    Ajouter le code nécessaire pour que la fonction ${ inline('creerCarte') } du module ${ inline('createurCarte') } soit exécuté lorsque l'utilisateur clique sur le 
                    bouton "Créer" dans l'interface graphique.
                </span></li>
                <li><span>
                    Dans le fichier ${ inline('main.js') }, programmez une fonction ${ inline('validerCourriel') } qui doit retourner vrai ou faux dépendant de si l'adresse courriel est
                    valide ou non. Si l'adresse courriel n'est pas valide, vous devriez changer la couleur de la bordure du champ en rouge.
                </span></li>
                <li><span>
                    Dans le fichier ${ inline('main.js') }, programmez une fonction ${ inline('validerTelephone') } qui doit retourner vrai ou faux dépendant de si l'adresse courriel 
                    est valide ou non. Si l'adresse courriel n'est pas valide, vous devriez changer la couleur de la bordure du champ en rouge.
                </span></li>
                <li><span>
                    Dans le fichier ${ inline('main.js') }, complétez la fonction ${ inline('modifierCarte') } qui doit modifier la carte d'affaires dans le HTML en fonction des valeurs
                    entrées par l'utilisateur dans le formulaire. 
                </span></li>
                <div class="video">
                    <div>
                        <iframe 
                            src="https://www.youtube.com/embed/GFbMNtI0Ay8" 
                            frameborder="0" 
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </ol>
        </div>`
};