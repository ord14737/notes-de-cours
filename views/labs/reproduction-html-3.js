export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Laboratoire 4: Reproduction d'une structure HTML</h1>
                <p>Pour compléter ce laboratoire, vous devez:</p>
                <ol>
                    <li><span>Créer la structure de base d'un projet Web</span></li>
                    <li><span>Créer une page index.html de base suivant le gabarit du cours</span></li>
                    <li><span>Ajouter les balises et le contenu nécessaire pour que votre page Web soit similaire à l'exemple ci-dessous</span></li>
                    <li>
                        <span>Respecter les critères ci-dessous: </span>
                        <ul>
                            <li><span>Le focus par défaut doit être sur le selecteur de textures</span></li>
                            <li><span>Les radiobuttons du même groupe sont bien regroupés</span></li>
                            <li><span>Plusieurs garnitures peuvent être sélectionnées</span></li>
                            <li><span>Vous pouvez prendre n'importe quelle image de ramen</span></li>
                            <li><span>Assurez-vous qu'en cliquant sur le nom du champ, le focus soit mis dans le champ</span></li>
                            <li><span>Les valeurs valides pour l'ail sont: "Aucun", "Un peu", "Normal", "Beaucoup" et "Énormément"</span></li>
                            <li><span>Les valeurs valides pour l'oignon vert sont: "Aucun", "Oignon vert partiel (blanc seulement)" et "Oignon vert complet"</span></li>
                        </ul>
                    </li>
                </ol>
                <a href="./img/reproduction-html-3.jpg"><img src="./img/reproduction-html-3.jpg" alt="Laboratoire de reproduction d'une structure HTML 3"></a>
            </div>`
    }
})();
    