export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Laboratoire optionnel: Installation des logiciels</h1>
                <p>Pour compléter ce laboratoire, vous devez:</p>
                <ol>
                    <li><span>Installez <a href="https://code.visualstudio.com/" target="_blank">Visual Studio Code</a></span></li>
                    <li>
                        <span>Installez <a href="https://git-scm.com/downloads" target="_blank">Git</a></span>
                        <ul>
                            <li><span>La plupart des options de base sont correcte pour l'installation</span></li>
                            <li><span>Spécifiez Visual Studio Code comme éditeur pour Git lorsque demandé</span></li>
                            <li><span>Assurez-vous d'avoir le &quot;Checkout Windows-style, Commit Unix-style line endings&quot; si vous êtes sur Windows</span></li>
                        </ul>
                    </li>
                    <li><span>Installez <a href="https://desktop.github.com/" target="_blank">GitHub Desktop</a> (optionnel)</span></li>
                    <li>
                        <span>Créez-vous un compte sur <a href="https://gitlab.com/users/sign_in" target="_blank">GitLab</a></span>
                        <ul>
                            <li><span>Assurez-vous d'utiliser votre adresse courriel du collège</span></li>
                            <li><span>Assurez-vous d'utiliser votre nom réel</span></li>
                        </ul>
                    </li>
                </ol>
            </div>`
    }
})();
