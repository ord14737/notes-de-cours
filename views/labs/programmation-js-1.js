let code = [];

code.push({});
code[0].js =
`/* Affiche une chaîne de caractères dans la console */
page.console.print( "texte" );

/* Affiche une chaîne de caractères dans la console suivie d'un retour de ligne */
page.console.println( "texte" );

/* Vide la console */
page.console.clear();

/* Demande à l'utilisateur d'entrer une valeur  */
let valeur = page.console.input( "Entrez une valeur: " );`;

export default {
    data: function(){
        return {
            code: code
        };
    },

    template: `
        <div class="note" v-once>
            <h1>Laboratoire 10: Programmation en Javascript</h1>
            <p>Pour compléter ce laboratoire, vous devez:</p>
            <ol>
                <li><span>
                    Programmer en Javascript une solution aux 3 exercices suivants. Le fichier HTML et la structure de base du laboratoire vous est déjà fourni. Vous pouvez les 
                    trouver sur <a href="https://gitlab.com/ord14737/laboratoire-10" target="_blank">GitLab</a>. Vous n'avez qu'à faire un clone du projet dans votre répertoire de 
                    travail ou à télécharger le fichier ${ inline('.zip') } du projet.
                    <ol>
                        <li><span>
                            Créer un programme demandant à l'utilisateur d'entrer un nombre de zéros consécutifs à rencontrer avant d'arrêter le programme. Générez et affichez 
                            ensuite un entier entre 0 et 1 à répétition. Arrêtez uniquement lorsque le nombre de zéros consécutifs est atteint. À la fin du programme, affichez le
                            nombre d'entiers générés avant d'atteindre le nombre de zéros consécutifs spécifié par l'utilisateur.
                        </span></li>
                        <li><span>
                            Demander à l'utilisateur d'entrer une chaîne de caractères. Indiquez à la console si cette chaîne est un palindrome ou non.
                        </span></li>
                        <li><span>
                            Demander à l'utilisateur d'entrer les 3 paramètres d'une équation quadratique (A, B et C). Solutionnez l'équation quadratique et affichez à la console 
                            le ou les zéros de la fonction s'il y a lieu. Vous pouvez utiliser la formule quadratique disponible 
                            <a href="http://www.alloprof.qc.ca/BV/pages/m1397.aspx" target="_blank">ici</a> pour solutionner l'équation.
                        </span></li>
                    </ol>
                </span></li>
                <li><span>
                    Vous devez écrire votre code dans les fichiers ${ inline('exercice1.js') }, ${ inline('exercice2.js') } et ${ inline('exercice3.js') }. Vous devez aussi insérer ces
                    scripts dans leur page HTML respective.
                </span></li>
                <li><span>
                    Utilisez la console Javascript fournie dans le fichier pour résoudre ces exercices. La console Javascript vous offres les fonctions suivantes:
                    <styled-code v-bind:code="code[0].js" lang="Javascript"></styled-code>
                </span></li>
            </ol>
        </div>`
};