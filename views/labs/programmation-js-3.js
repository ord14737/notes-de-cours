export default {
    template: `
        <div class="note" v-once>
            <h1>Laboratoire 12: Programmation en Javascript</h1>
            <p>Pour compléter ce laboratoire, vous devez:</p>
            <ol>
                <li><span>
                    Compléter le jeu du tic-tac-toe. La structure de base du laboratoire vous est déjà fourni. Vous pouvez les trouver sur 
                    <a href="https://gitlab.com/ord14737/laboratoire-12" target="_blank">GitLab</a>. Vous n'avez qu'à faire un clone du projet dans votre répertoire de travail ou à 
                    télécharger le fichier ${ inline('.zip') } du projet.
                </span></li>
                <li><span>
                    Vous devez programmer la fonction ${ inline('checkWinner()') } qui retourne vrai si un joueur est gagnant dans la grille de tic-tac-toe ou faux sinon. De plus, si un
                    des joueurs est gagnant, vous devez ajouter la classe ${ inline('winner') } aux cellules gagnantes de la grille (bref, aux cellules qui font la ligne gagnante).
                </span></li>
                <li><span>
                    Ajoutez du code sur le bouton "Recommencer" pour qu'il remettre le jeu à neuf. Assurez-vous de bien réinitialiser toutes les variables à leur valeur par défaut et de 
                    réinitialiser l'interface graphique.
                </span></li>
            </ol>
            <div class="video">
                <div>
                    <iframe 
                        src="https://www.youtube.com/embed/WJQm7uxB5LY" 
                        frameborder="0" 
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                        allowfullscreen>
                    </iframe>
                </div>
            </div>
        </div>`
};