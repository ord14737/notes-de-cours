export default {
    template: `
        <div class="note" v-once>
            <h1>Laboratoire 14: Programmation en Javascript</h1>
            <p>Pour compléter ce laboratoire, vous devez:</p>
            <p>
                Compléter l'application d'affichage de cartes. Par défaut, l'application affiche uniquement 8 cartes dont le contenu est déjà spécifié. Vous devez modifier le fichier 
                ${ inline('index.html') } et ${ inline('main.js') } pour permettre à l'application Web de générer entre 10 et 200 cartes avec du contenu aléatoire. Je vous suggère 
                fortement d'utiliser les ${ inline('<template>') } ainsi que les fonctions de génération aléatoire déjà fournie dans le fichier ${ inline('main.js') }. La structure 
                de base du laboratoire vous est déjà fourni. Vous pouvez la trouver sur <a href="https://gitlab.com/ord14737/laboratoire-14" target="_blank">GitLab</a>. Vous n'avez 
                qu'à faire un clone du projet dans votre répertoire de travail ou à télécharger le fichier ${ inline('.zip') } du projet.
            </p>
            <div class="video">
                <div>
                    <iframe 
                        src="https://www.youtube.com/embed/QnosdxqfVdE" 
                        frameborder="0" 
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                        allowfullscreen>
                    </iframe>
                </div>
            </div>
        </div>`
};