export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Laboratoire 8: Concours de HTML et CSS</h1>
                <section>
                    <h2>À compléter</h2>
                    <p>
                        Pour compléter ce laboratoire, vous devez créer et soumettre un site Web valide pour le concours de HTML et CSS. Assurez-vous de bien respecter les règles pour
                        ne pas soumettre une entrée invalide.
                    </p>
                </section>

                <section>
                    <h2>Règlement du concours</h2>
                    Seul ou en équipe de 2, vous devez créez un site Web pour promouvoir un évènement (fictif ou non) de votre choix. Je vous suggère fortement de vous mettre en équipe
                    de 2 pour pouvoir faire un "brainstorm" en équipe. Votre site Web doit respecter les élément ci-dessous.
                    <ol>
                        <li><span>Votre site Web doit avoir exactement 2 pages.</span></li>
                        <li>
                            <span>Votre site Web doit contenir de l'information sur l'évènement. Par exemple:</span>
                            <ul>
                                <li><span>Nom de l'évènement</span></li>
                                <li><span>Date de l'évènement</span></li>
                                <li><span>Description de l'évènement</span></li>
                                <li><span>Emplacement de l'évènement</span></li>
                            </ul>
                        </li>
                        <li>
                            <span>Votre site Web doit contenir au moins 1 formulaire. Par exemple:</span>
                            <ul>
                                <li><span>Acheter des billets</span></li>
                                <li><span>Réservations</span></li>
                                <li><span>Inscription pour bénévolat</span></li>
                                <li><span>Abonnement aux courriels</span></li>
                            </ul>
                        </li>
                        <li><span>Votre site Web doit avoir au moins 10 images.</span></li>
                        <li><span>Votre site Web doit être fait uniquement avec du HTML et CSS. Tout site Web contenant du Javascript, PHP ou autre langage sera disqualifié.</span></li>
                        <li>
                            <span>
                                Votre site Web ne peut pas être basé sur un gabarit existant ou sur une copie d'un autre site Web. Si un tel site Web est soumis, il sera 
                                automatiquement disqualifié.
                            </span>
                        </li>
                        <li>
                            <span>
                                Vous ne pouvez pas utiliser de librairies HTML ou CSS. Vous ne pouvez donc pas utiliser <a href="https://getbootstrap.com/" target="_blank">Bootstrap</a>,
                                <a href="https://bulma.io/" target="_blank">Bulma</a> ou <a href="https://purecss.io/" target="_blank">PureCSS</a>.
                            </span>
                        </li>
                        <li><span>Vous devez absolument voter pour que votre entrée au concours soit valide.</span></li>
                        <li><span>Vous ne pouvez pas voter pour vous-même. Un étudiant qui voterais pour son propre travail se verrait disqualifié.</span></li>
                    </ol>
                </section>

                <section>
                    <h2>Soumission</h2>
                    <p>
                        La date de soumission du laboratoire est indiqué sur eCité.
                    </p>
                    <p>
                        Vous devez soumettre votre laboratoire à l'aide de GitLab. Après la date de soumission, le dernier commit de votre site Web fait sur GitLab sera considéré comme
                        votre soumission. Les différentes soumissions seront compilées dans un seul site Web pour les votes. Aucun retard ne sera accepté puisque ceci est une compétition. 
                        Assurez-vous d'ajouter mon utilisateur GitLab (@jwilki) comme "invité" sur votre projet pour que je puisse télécharger votre site Web.
                    </p>
                    <p>
                        Nous verrons en classe comment créer un projet, ajouter des utilisateurs et comment faire les commits. Si vous n'êtes pas en classe, vous pouvez faire des 
                        recherches sur le Web ou demander à vos collègues de vous aider.
                    </p>
                </section>
                
                <section>
                    <h2>Votes</h2>
                    Après la date de remise des entrées pour le concours, chaque étudiant devra voter pour le site Web qu'il préfère parmi les autres soumissions. Un étudiant ne peut 
                    donc pas voter pour lui-même. Un comité de professeurs votera aussi au préalable sur les sites Web qu'ils préfèrent. Le vote d'un professeur compte pour 2 votes. Les 
                    votes doivent prendre en considération uniquement l'aspect visuel et le contenu du site Web. Bref, ne votez pas uniquement pour vos amis. Les gagnants seront 
                    déterminé par la quantité de votes pour chaque soumission.
                </section>
                
                <section>
                    <h2>Prix</h2>
                    Les 3 équipes ayant le plus de votes recevront les prix suivant:
                    <div class="podium">
                        <div class="second">
                            <div class="prize"><div class="big">+4% bonus</div><div class="small">au prochain examen</div></div> 
                            <div class="number">2<sup>ème</sup></div>
                        </div>
                        <div class="first">
                            <div class="prize"><div class="big">+5% bonus</div><div class="small">au prochain examen</div></div> 
                            <div class="number">1<sup>er</sup></div>
                        </div>
                        <div class="third">
                            <div class="prize"><div class="big">+3% bonus</div><div class="small">au prochain examen</div></div> 
                            <div class="number">3<sup>ème</sup></div>
                        </div>
                    </div>
                    <p class="ultra-small">
                        * J'ai passé vraiment trop de temps à faire ce petit podium en HTML et CSS...
                    </p>
                </section>
                
                <section>
                    <h2>Liens et outils</h2>
                    <p>
                        Si vous avez besoin d'images ou d'icônes, vous pouvez utilisez celles se trouvant aux liens ci-dessous. Assurez-vous de toujours utiliser des images dont vous
                        avez les droits lorsque vous produisez un site Web. Assurez-vous de lire les licences d'utilisation.
                    </p>
                    <ul>
                        <li><span><a href="https://unsplash.com/" target="_blank">Unsplash</a></span></li>
                        <li><span><a href="https://www.pexels.com/" target="_blank">Pexels</a></span></li>
                        <li><span><a href="https://pixabay.com/" target="_blank">Pixabay</a></span></li>
                        <li><span><a href="https://iconmonstr.com/" target="_blank">Iconmonstr</a></span></li>
                    </ul>
                </section>

                <section>
                    <h2>Encouragements</h2>
                    <p>
                        Si les points bonus ne sont pas un assez bon encouragement, en voici d'autres:
                    </p>
                    <ul>
                        <li><span>Let's go gang!</span></li>
                        <li><span>Vous êtes capable!</span></li>
                        <li><span>Impressionnez-moi!</span></li>
                        <li><span>Ayez du plaisir!</span></li>
                    </ul>
                </section>
            </div>`
    }
})();