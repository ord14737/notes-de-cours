export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Laboratoire 6: Reproduction du style</h1>
                <p>Pour compléter ce laboratoire, vous devez:</p>
                <ol>
                    <li><span>Créez un site Web contenant 3 pages, soit ${ inline('index.html') }, ${ inline('histoire.html') } et  ${ inline('terrrain.html') }.</span></li>
                    <li><span>Créez le HTML de ces pages et assurez-vous que les liens entre les pages fonctionnent correctement.</span></li>
                    <li><span>Styliser le HTML des 3 pages en ajoutant un fichier CSS à vos pages HTML.</span></li>
                    <li><span>Les mesures que vous utilisez peuvent être approximatives.</span></li>
                    <li>
                        <span>Les couleurs utilisées sont:</span>
                        <ul>
                            <li><span><span class="color-box" style="background-color: #000000"></span> ${ inline('#000000') }</span></li>
                            <li><span><span class="color-box" style="background-color: #2b4a2d"></span> ${ inline('#2b4a2d') }</span></li>
                            <li><span><span class="color-box" style="background-color: #ffffff"></span> ${ inline('#ffffff') }</span></li>
                            <li><span><span class="color-box" style="background-color: #29302d"></span> ${ inline('#29302d') }</span></li>
                            <li><span><span class="color-box" style="background-color: #698062"></span> ${ inline('#698062') }</span></li>
                        </ul>
                    </li>
                    <li>
                        <span>Les images utilisées sont:</span>
                        <ul>
                            <li><span><a href="./img/intro.jpg">intro.jpg</a></span></li>
                            <li><span><a href="./img/histoire-1.jpg">histoire-1.jpg</a></span></li>
                            <li><span><a href="./img/histoire-2.jpg">histoire-2.jpg</a></span></li>
                        </ul>
                    </li>
                </ol>
                <a href="./img/reproduction-css-2-1.jpg"><img src="./img/reproduction-css-2-1.jpg" alt="Reproduction du style 1"></a>
                <a href="./img/reproduction-css-2-2.jpg"><img src="./img/reproduction-css-2-2.jpg" alt="Reproduction du style 1"></a>
                <a href="./img/reproduction-css-2-3.jpg"><img src="./img/reproduction-css-2-3.jpg" alt="Reproduction du style 1"></a>
            </div>`
    }
})();