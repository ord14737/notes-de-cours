export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Laboratoire 7: Reproduction du style</h1>
                <p>Pour compléter ce laboratoire, vous devez:</p>
                <ol>
                    <li><span>
                        Créez une page Web comme celui de la vidéo ci-dessous à partir du HTML. Le fichier HTML et la structure de base du laboratoire vous est déjà fourni. Vous pouvez 
                        les trouver sur <a href="https://gitlab.com/ord14737/laboratoire-7" target="_blank">GitLab</a>. Vous n'avez qu'à faire un clone du projet dans votre répertoire 
                        de travail ou à télécharger le fichier ${ inline('.zip') } du projet.
                    </span></li>
                    <li><span>Créez le CSS de cette page Web dans le fichier ${ inline('style.css') }. Vous avez le droit de modifier le HTML au besoin.</span></li>
                    <li><span>Les mesures que vous utilisez peuvent être approximatives.</span></li>
                    <li>
                        <span>Les couleurs utilisées sont:</span>
                        <ul>
                            <li><span><span class="color-box" style="background-color: #eddfca"></span> ${ inline('#eddfca') }</span></li>
                            <li><span><span class="color-box" style="background-color: #301c15"></span> ${ inline('#301c15') }</span></li>
                            <li><span><span class="color-box" style="background-color: #4f3126"></span> ${ inline('#4f3126') }</span></li>
                            <li><span><span class="color-box" style="background-color: #6b493d"></span> ${ inline('#6b493d') }</span></li>
                            <li><span><span class="color-box" style="background-color: #fff"></span> ${ inline('#fff') }</span></li>
                        </ul>
                    </li>
                    <li>
                        <span>
                            Les polices de caratères peuvent être trouvé sur <a href="https://fonts.google.com/" target="_blank">Google Font</a>. Dans ce laboratoire, nous utilisons les 
                            polices ci-dessous. Assurez-vous de sélectionner leur version régulière et en gras.
                        </span>
                        <ul>
                            <li><span><a href="https://fonts.google.com/specimen/Neuton" target="_blank">Neuton</a></span></li>
                            <li><span><a href="https://fonts.google.com/specimen/Lato" target="_blank">Lato</a></span></li>
                        </ul>
                    </li>
                </ol>
                <div class="video">
                    <div>
                        <iframe
                            src="https://www.youtube.com/embed/J-eDRWesbxQ" 
                            frameborder="0" 
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>`
    }
})();