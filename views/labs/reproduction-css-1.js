export default (function() {
    return {
        template: `
            <div class="note" v-once>
                <h1>Laboratoire 5: Reproduction du style</h1>
                <p>Pour compléter ce laboratoire, vous devez:</p>
                <ol>
                    <li><span>Créer un tableau de 6 rangées et 4 colonnes en HTML similaire à celui ci-dessous.</span></li>
                    <li><span>Fusionner les bonnes cellules dans le tableau.</span></li>
                    <li><span>Styliser la page Web et la tableau pour qu'il ressemble à l'image ci-dessous.</span></li>
                    <li>
                        <span>Les couleurs utilisées sont:</span>
                        <ul>
                            <li><span><span class="color-box" style="background-color: #29302d"></span> ${ inline('#29302d') }</span></li>
                            <li><span><span class="color-box" style="background-color: #ffffff"></span> ${ inline('#ffffff') }</span></li>
                            <li><span><span class="color-box" style="background-color: #698062"></span> ${ inline('#698062') }</span></li>
                        </ul>
                    </li>
                </ol>
                <a href="./img/reproduction-css-1.jpg"><img src="./img/reproduction-css-1.jpg" alt="Reproduction du style 1"></a>
            </div>`
    }
})();