export default {
    template: `
        <div class="week">
            <div class="week-header">
                <div class="bubble">
                    <span class="bubble-week">Semaine</span>
                    <span class="bubble-number">{{ $root.getWeekNumber($route.query.id) }}</span>
                </div>
                <h1>
                    {{ $root.weekData[$route.query.id].title }}
                </h1>
            </div>

            <section class="week-lists">
                <h2>Note de cours</h2>
                <ul>
                    <li v-for="note in $root.weekData[$route.query.id].notes">
                        <router-link :to="{ path: '/notes/' + note.path }">{{ note.title }}</router-link>
                    </li>
                </ul>
            </section>

            <section class="week-lists" v-if="$root.weekData[$route.query.id].labs.length > 0">
                <h2>Laboratoire</h2>
                <ul>
                    <li v-for="labs in $root.weekData[$route.query.id].labs">
                        <router-link :to="{ path: '/labs/' + labs.path }">{{ labs.title }}</router-link>
                    </li>
                </ul>
            </section>
        </div>`
};
