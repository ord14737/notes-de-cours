export default {
    template: `
        <div class="home">
            <h1>Bienvenue!</h1>
            <p>
            Pokem ipsum dolor sit amet Ferrothorn they're comfy and easy to wear Breloom Beautifly Exeggcute Gliscor. Fog Badge Ambipom Volcarona Jellicent Ariados Simisear Relicanth. 
            Sonic Boom ut enim ad minim veniam Stantler Oshawott Ditto anim id est laborum Nidoking. Fire I'm on the road to Viridian City Shuppet Rainbow Badge Scyther Minun Flaaffy. 
            Ghost Raichu Crawdaunt Sandile Pinsir Primeape Quagsire.
            </p>
            <ul class="week-list">
                <li class="week-info" v-for="(week, index) in $root.weekData" v-bind:class="{ disabled: week.disabled }">
                    <div class="bubble">
                        <span class="bubble-week">Semaine</span>
                        <span class="bubble-number">{{ $root.getWeekNumber(index) }}</span>
                    </div>
                    <div class="description">
                        <router-link :to="{ path: '/week', query: { id: index } }" v-bind:tabIndex="week.disabled ? -1 : null"><h2>{{ week.title }}</h2></router-link>
                        <p>
                            {{ week.description }}
                        </p>
                    </div>
                </li>
            </ul>
        </div>`
};
