hljs.configure({
    languages: ['Javascript', 'HTML', 'CSS', 'Bash']
});

let inline = function(code){
    return '<span class="inline-code">' + 
        code.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/-/g, '&#8209;') + 
        '</span>';
};

/**
 * Return an HTML string to display multiple box of codes and an example using all the code in the boxes.
 * 
 * If the parameters are arrays of 2 elements, the first element of the array is used for the box display and the second one is used for the example. This way, you can change your code in
 * your example without changing the box display.
 * 
 * If the parameters are arrays of 1 element, the first element of the array is used for the example, but is not showed in the box display.
 * 
 * @param {string|array<string>} html The HTML to display in a box and to show as an example.
 * @param {string|array<string>} css The CSS to display in a box and to show as an example.
 * @param {string|array<string>} js The Javascript to display in a box and to show as an example.
 */
let addCodeExample = function(html, css, js){
    let template = '<div class="code-example">'
    let example = '';

    let addLanguage = function(code, name, tag){
        let addTag = function(code, tag){
            return tag ? `<${ tag }>${ code }</${ tag }>` : code
        }

        if(code){
            if(Array.isArray(code) && code.length > 1){
                template += addCode(code[0], name);
                example += addTag(code[1], tag);
            }
            else if(Array.isArray(code) && code.length == 1){
                example += addTag(code[0], tag);
            }
            else{
                template += addCode(code, name);
                example += addTag(code, tag);
            }
        }
    }

    addLanguage(css, 'CSS', 'style');
    addLanguage(js, 'Javascript', 'script');
    addLanguage(html, 'HTML');

    // Add Example
    template += addExample(example)
    template += '</div>'

    return template;
}

let addCode = function(code, language){
    let highlight = (language ? hljs.highlight(language, code, true) : hljs.highlightAuto(code));
    let highlightedCode = highlight.value;
    language = language || highlight.language;
    return `<div class="code-wrapper">
                <div class="code-tag">${ language }</div>
                <pre><code class="hljs ${ language }">${ highlightedCode }</code></pre>
            </div>`
};

let addExample = function(code){
    return `<iframe class="example" height="0" onload="addExampleContent(this, &quot;${ encodeURI(code) }&quot;)"></iframe>`;
};

let addExampleContent = function(iframe, code){
    iframe.contentWindow.document.open();
    iframe.contentWindow.document.write(decodeURI(code));
    iframe.contentWindow.document.close();
    for(let img of iframe.contentWindow.document.images){
        img.onload = function(){
            iframe.height = iframe.contentWindow.document.body.scrollHeight + 2;
        } 
    }

    iframe.height = iframe.contentWindow.document.body.scrollHeight + 2;
};
