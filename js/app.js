// Constant representing the page body
const body = document.getElementsByTagName('body')[0];

// Vue.js Application
const app = new Vue({ 
    router: router,

    el: '#app',

    components: {
        'aside-week': asideWeek
    },

    data: {
        // Week data to use in the HTML
        weekData: weekData,
    },

    methods: {
        // Toggle aside menu
        toggleAside: function(){
            body.classList.toggle('aside-open');
        },

        // Toggle menu
        toggleMenu: function(){
            body.classList.toggle('menu-open')
        },

        // Close menu when clicking outside menu
        closeMenu: function () {
            const header = document.getElementsByTagName('header')[0];
            if (!header.contains(event.target)) {
                body.classList.remove('menu-open');
            }
        },

        // Switch color to normal or dark
        switchColor: function() {
            const lightCSS = document.getElementById('style-code-light');
            const darkCSS = document.getElementById('style-code-dark');
            body.classList.toggle('dark');
            lightCSS.disabled = !lightCSS.disabled;
            darkCSS.disabled = !darkCSS.disabled;

            localStorage.setItem('color-theme', body.classList.contains('dark') ? 'dark' : 'light');
        },

        // Get the week name
        getWeekNumber: function(index){
            return ('0' + (parseInt(index) + 1)).slice(-2);
        },
    },
    
    mounted: function(){
        if(localStorage.getItem('color-theme') === 'dark'){
            this.switchColor();
        }

        body.classList.remove('hidden')
    }
});
