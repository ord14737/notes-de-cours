let createRoutes = function(){
    // Define basic routes
    let routes = [
        { path: '/', component: () => import('../views/home.js') },
        { path: '/week', component: () => import('../views/week.js') }
    ];

    // Function to get component from path
    let capitalize = (string) => string.charAt(0).toUpperCase() + string.slice(1);
    let lowertalize = (string) => string.charAt(0).toLowerCase() + string.slice(1);
    let getComponentFromPath = function(path){
        let componentName = lowertalize(path.split('-').map(capitalize).join(''));
        return pageComponents[componentName];
    };

    // Define notes and labs routes
    for(let week of weekData){
        for(let note of week.notes){
            routes.push({ path: '/notes/' + note.path, component: () => import('../views/notes/' + note.path + '.js') });
        }
    
        for(let lab of week.labs){
            routes.push({ path: '/labs/' + lab.path, component: () => import('../views/labs/' + lab.path + '.js') });
        }
    }

    // Add a catch all route to the main page
    routes.push({ path: '*', redirect: '/' });

    // Return the routes
    return routes;
};

const router = new VueRouter({
    routes: createRoutes()
});
