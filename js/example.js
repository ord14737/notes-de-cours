Vue.component('example', {
    props: {
        code: String
    },

    methods: {
        addContent: function(){
            // Write to iframe
            this.$refs.iframe.contentWindow.document.open();
            this.$refs.iframe.contentWindow.document.write(this.code);
            this.$refs.iframe.contentWindow.document.close();

            // Set iframe height
            for(let img of this.$refs.iframe.contentWindow.document.images){
                img.onload = function(){
                    this.$refs.iframe.height = this.$refs.iframe.contentWindow.document.body.scrollHeight + 2;
                } 
            }
        
            this.$refs.iframe.height = this.$refs.iframe.contentWindow.document.body.scrollHeight + 2;
        }
    },

    template: 
        `<iframe 
            v-once
            ref="iframe"
            class="example" 
            height="0" 
            v-on:load="addContent()">
        </iframe>`
});
