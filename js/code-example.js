Vue.component('code-example', {
    props: {
        code: Object
    },

    created: function(){
        if(this.code.css || this.code.exCss){
            this.exampleCode += '<style>' + (this.code.exCss || this.code.css) + '</style>';
        }

        this.exampleCode += (this.code.exHtml || this.code.html);

        if(this.code.js || this.code.exJs){
            this.exampleCode += '<script>' + (this.code.exJs || this.code.js) + '</script>';
        }
    },

    data: function(){
        return {
            exampleCode: ''
        }
    },

    template: 
        `<div class="code-example" v-once>
            <styled-code v-if="code.css" lang="CSS" v-bind:code="code.css"></styled-code>
            <styled-code v-if="code.js" lang="Javascript" v-bind:code="code.js"></styled-code>
            <styled-code v-if="code.html" lang="HTML" v-bind:code="code.html"></styled-code>
            <example v-bind:code="exampleCode"></example>
        </div>`
});