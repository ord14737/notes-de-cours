Vue.component('styled-code', {
    props: {
        lang: String,
        code: String,
    },

    created: function(){
        let highlight;
        if(this.lang) {
            highlight = hljs.highlight(this.lang, this.code, true);
            this.langUsed = this.lang;
        }
        else{
            highlight = hljs.highlightAuto(this.code);
            this.langUsed = highlight.language;
        }

        this.codeUsed = highlight.value;
    },

    data: function(){
        return {
            langUsed: '',
            codeUsed: ''
        }
    },

    template: 
        `<div class="code-wrapper" v-once>
            <div class="code-tag" v-if="langUsed != 'plaintext'">{{ langUsed }}</div>
            <pre><code class="hljs" v-bind:class="langUsed" v-html="codeUsed"></code></pre>
        </div>`
});
