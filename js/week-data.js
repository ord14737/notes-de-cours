// Define the object reprenting all weeks and their pages
const weekData = [
    {
        title: 'Introduction au HTML et aux outils',
        description: 'Une introduction au développement Web, en commençant par le HTML. De plus, nous installerons et commenceront à utiliser les logiciels d\'édition de code et de versioning. Finalement, une présentation des ressources en ligne pour vous aider.',
        disabled: false,
        notes: [
            { title: 'Introduction au HTML', path: 'intro-html' },
            { title: 'Création d\'un projet', path: 'intro-projet' },
            { title: 'Balise et concept de base', path: 'balise-1' },
            { title: 'Caractère spéciaux', path: 'special-char' },
            { title: 'Visual Studio Code', path: 'visual-studio-code' },
            { title: 'Introduction à Git', path: 'intro-git' }
        ],
        labs: [
            { title: 'Laboratoire 1: Reproduction d\'une structure HTML', path: 'reproduction-html-1' },
            { title: 'Laboratoire optionnel: Installation des logiciels', path: 'installations' }
        ]
    },
    {
        title: 'Balises',
        description: 'Une vue rapide des différentes balises HTML plus complexes. Nous voyons comment ajouter des liens, des tableaux et des médias à nos pages Web. Finalement, nous passerons un court instant sur les balises sémantiques et la validation du HTML.',
        disabled: false,
        notes: [
            { title: 'Tableau', path: 'balise-2' },
            { title: 'Lien', path: 'balise-3' },
            { title: 'Media', path: 'balise-4' },
            { title: 'Balises sémantiques', path: 'balise-5' },
            { title: 'Affichage de bloc ou en ligne', path: 'display-html' },
            { title: 'Validation du HTML', path: 'validation-html' }
        ],
        labs: [
            { title: 'Laboratoire 2: Reproduction d\'une structure HTML', path: 'reproduction-html-2' },
            { title: 'Laboratoire 3: Valider du HTML', path: 'reparer-validation-html' }
        ]
    },
    {
        title: 'Formulaires',
        description: 'Nous voyons comment créer des formulaires et comment utiliser leurs différents composants pour éventuellement avoir des pages intéractives.',
        disabled: false,
        notes: [
            { title: 'Balises d\'entrée', path: 'balise-6' },
            { title: 'Zone de textes et sélections', path: 'balise-7' },
            { title: 'Formulaire, nom de champs et attributs', path: 'balise-8' }
        ],
        labs: [
            { title: 'Laboratoire 4: Reproduction d\'une structure HTML', path: 'reproduction-html-3' }
        ]
    },
    {
        title: 'Introduction au CSS',
        description: 'Une introduction au design de page Web avec CSS. Nous voyons comment changer la taille, la couleur et l\'espacement des éléments définis dans le HTML.',
        disabled: false,
        notes: [
            { title: 'Introduction au CSS', path: 'intro-css' },
            { title: 'Sélecteurs CSS de base', path: 'selector-1' },
            { title: 'Couleurs', path: 'colors-css' },
            { title: 'Taille, marge et espacement', path: 'spacing-css' },
            { title: 'Bordures', path: 'border-css' },
            { title: 'Changement d\'affichage', path: 'display-css' },
            { title: 'Balise non sémantique', path: 'balise-9' },
        ],
        labs: [
            { title: 'Laboratoire 5: Reproduction du style', path: 'reproduction-css-1' },
            { title: 'Laboratoire 6: Reproduction du style', path: 'reproduction-css-2' },
        ]
    },
    {
        title: 'Propriété CSS, outils et conventions',
        description: 'Une présentation des sélecteurs CSS de pseudo-classe et des différentes conventions du CSS, tant par son écriture que par son design. Nous verrons entres autres les propriétés typographiques, la stylisation de listes, de tableaux et les différentes unités de mesure utilisé en CSS.',
        disabled: false,
        notes: [
            { title: 'Unité de mesures', path: 'units-css' },
            { title: 'Style de base', path: 'base-style-css' },
            { title: 'Listes et tableaux', path: 'list-table-css' },
            { title: 'Typographie et polices', path: 'text-font-css' },
            { title: 'Wrapper', path: 'wrapper-css' },
            { title: 'Sélecteurs CSS de pseudo-classe', path: 'selector-2' },
            { title: 'Validation du CSS', path: 'validation-css' },
            { title: 'Outils de développement CSS', path: 'dev-tool-css' },
        ],
        labs: [
            { title: 'Laboratoire 7: Reproduction du style', path: 'reproduction-css-3' }
        ]
    },
    {
        title: 'Affichage complexe',
        description: 'Une vue des différents types d\'affichage et de positionnements des éléments HTML. Nous voyons aussi comment utiliser des sélecteurs CSS plus complexe.',
        disabled: false,
        notes: [
            { title: 'Positionnement', path: 'position-css' },
            { title: 'Sélecteur CSS d\'attributs et de voisins', path: 'selector-3' },
        ],
        labs: [
            { title: 'Laboratoire 8: Concours de HTML et CSS', path: 'contest-html-css' }
        ]
    },
    {
        title: 'Design réactif',
        description: 'Une introduction au design réactif (responsive) à l\'aide des media queries et des Flexbox. Cette introduction nous plonge dans le développement de sites Web qui fonctionnent autant sur un appareil mobile que sur un ordinateur.',
        disabled: false,
        notes: [
            { title: 'Approche "mobile-first" et media queries', path: 'media-queries-css' },
            { title: 'Flexbox', path: 'flexbox-css' },
            { title: 'Trucs avec Flexbox', path: 'flexbox-tricks-css' }
        ],
        labs: [
            { title: 'Laboratoire 9: Reproduction du style', path: 'reproduction-css-4' }
        ]
    },
    {
        title: 'Transition et transformation',
        description: 'Nous voyons comment ajouter un peu de vie à page en ajoutant des transitions à différents éléments de nos pages Web. Nous utiliserons entres autres les transformations CSS pour animer nos transitions.',
        disabled: false,
        notes: [
            { title: 'Transition', path: 'transition-css' },
            { title: 'Transformation', path: 'transform-css' }
        ],
        labs: []
    },
    {
        title: 'Introduction au Javascript',
        description: 'Introduction au langage de programmation Javascript dans un context général. Nous voyons les différents éléments de base d\'un langage de programmation, comme les variables, les conditions et les boucles dans ce langage. Finalement, nous introduisons aussi les outils de développement, comme les débogueurs pour Javascript.',
        disabled: false,
        notes: [
            { title: 'Introduction au Javascript', path: 'intro-js' }
        ],
        labs: [
            { title: 'Laboratoire 10: Programmation en Javascript', path: 'programmation-js-1' }
        ]
    },
    {
        title: 'Introduction au DOM et aux fonctions',
        description: 'Une vue rapide du modèle objet de document (DOM) pour y ajouter des évènements et par conséquent, lier le HTML, le Javascript et le CSS. Nous voyons aussi comment utiliser les fonctions en Javascript, un concept beaucoup plus complexe qu\'en programmation orienté objet traditionnel.',
        disabled: false,
        notes: [
            { title: 'Fonctions', path: 'fonction-js' },
            { title: 'Introduction au DOM', path: 'dom-js' },
            { title: 'Travailler en équipe avec Git', path: 'team-git' }
        ],
        labs: [
            { title: 'Laboratoire 11: Programmation en Javascript', path: 'programmation-js-2' }
        ]
    },
    {
        title: 'Structure de données',
        description: 'Une présentation des différentes structures de données de base en Javascript, comme les tableaux, les objets et les classes. Nous voyons aussi les différentes fonctions permmetant leur manipulation.',
        disabled: false,
        notes: [
            { title: 'Tableau', path: 'array-js' },
            { title: 'Objet', path: 'object-js' },
            { title: 'Classe', path: 'class-js' },
            { title: 'Publier en ligne sur GitLab', path: 'gitlab-pages-git' }
        ],
        labs: [
            { title: 'Laboratoire 12: Programmation en Javascript', path: 'programmation-js-3' },
            { title: 'Laboratoire 13: Programmation en Javascript', path: 'programmation-js-4' }
        ]
    },
    {
        title: 'Modification du DOM',
        description: 'Une démonstration de comment on peut manipuler le DOM avec javascript afin d\'insérer, modifier ou supprimer des éléments, des propriétés ou des évènements dans le HTML de vos projets Web. Nous voyons aussi les impacts de ces manipulations sur la performance de vos pages Web.',
        disabled: false,
        notes: [
            { title: 'Manipulation du DOM', path: 'dom-modify-js' },
            { title: 'Problèmes de performance du DOM', path: 'dom-performance-js' }
        ],
        labs: [
            { title: 'Laboratoire 14: Programmation en Javascript', path: 'programmation-js-5' }
        ]
    }
];
