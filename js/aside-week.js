let asideWeek = {
    methods: {
        getTabIndex: function(week){
            return week.disabled ? -1 : null;
        }
    },

    template: `
        <aside>
            <nav>
                <ul>
                    <li v-for="(week, index) in $root.weekData">
                        <router-link 
                            :to="{ path: '/week', 
                            query: { id: index } }"
                            v-bind:class="{ disabled: week.disabled }" 
                            v-bind:tabIndex="getTabIndex(week)">
                            <span v-on:click="$root.toggleAside" >Semaine {{ $root.getWeekNumber(index) }}</span>
                        </router-link>
                    </li>
                </ul>
            </nav>
        </aside>`
};